# -*- mode: python -*-
# -*- coding: utf-8 -*-

import os
import secrets
from PyInstaller.building.api import PYZ, EXE, COLLECT
from PyInstaller.building.build_main import Analysis
from PyInstaller.utils.hooks import is_module_satisfies
from PyInstaller.archive.pyz_crypto import PyiBlockCipher

# Constants
DEBUG = os.environ.get("CEFPYTHON_PYINSTALLER_DEBUG", False)
PYCRYPTO_MIN_VERSION = "2.6.1"

cipher_obj = PyiBlockCipher(key=secrets.token_hex(16))

a = Analysis(
    ["./executar_servidor.py"],
    binaries=[
        ('./icon.ico','.'),
        ('./QtUtilsServer/bandeja/icones/*','./qtutilsserver/bandeja/icones'),
    ],
    hiddenimports=[m for m in os.getenv('QTUTILS_MODULOS', '').split(',')],
    hookspath=None,
    cipher=cipher_obj,
    win_private_assemblies=True,
    win_no_prefer_redirects=True,
)

pyz = PYZ(a.pure,
          a.zipped_data,
          cipher=cipher_obj)

exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name=os.getenv("APP_NAME"),
          debug=DEBUG,
          strip=False,
          upx=False,
          console=DEBUG,
          icon="./icon.ico",
          )

COLLECT(exe,
        a.binaries,
        a.zipfiles,
        a.datas,
        strip=False,
        upx=False,
        name="pyinstaller")
