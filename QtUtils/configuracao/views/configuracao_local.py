# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'configuracao_local.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(378, 142)
        Dialog.setMinimumSize(QSize(378, 142))
        Dialog.setMaximumSize(QSize(378, 142))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.fechar = QPushButton(self.frame)
        self.fechar.setObjectName(u"fechar")

        self.horizontalLayout.addWidget(self.fechar)

        self.salvar = QPushButton(self.frame)
        self.salvar.setObjectName(u"salvar")

        self.horizontalLayout.addWidget(self.salvar)


        self.gridLayout.addWidget(self.frame, 3, 0, 1, 1)

        self.widget_3 = QWidget(Dialog)
        self.widget_3.setObjectName(u"widget_3")
        self.gridLayout1 = QGridLayout(self.widget_3)
        self.gridLayout1.setObjectName(u"gridLayout1")
        self.gridLayout1.setContentsMargins(0, 0, 0, 0)
        self.tabWidget = QTabWidget(self.widget_3)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.gridLayout_3 = QGridLayout(self.tab_2)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.bkp_dir = QPushButton(self.tab_2)
        self.bkp_dir.setObjectName(u"bkp_dir")

        self.gridLayout_3.addWidget(self.bkp_dir, 1, 1, 1, 1)

        self.bkp_pasta_padrao = QLineEdit(self.tab_2)
        self.bkp_pasta_padrao.setObjectName(u"bkp_pasta_padrao")
        self.bkp_pasta_padrao.setEnabled(True)
        self.bkp_pasta_padrao.setReadOnly(True)

        self.gridLayout_3.addWidget(self.bkp_pasta_padrao, 1, 0, 1, 1)

        self.label_8 = QLabel(self.tab_2)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_3.addWidget(self.label_8, 0, 0, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_3.addItem(self.verticalSpacer_2, 2, 0, 1, 1)

        self.tabWidget.addTab(self.tab_2, "")

        self.gridLayout1.addWidget(self.tabWidget, 0, 0, 1, 1)


        self.gridLayout.addWidget(self.widget_3, 0, 0, 1, 1)

        QWidget.setTabOrder(self.tabWidget, self.bkp_pasta_padrao)
        QWidget.setTabOrder(self.bkp_pasta_padrao, self.bkp_dir)
        QWidget.setTabOrder(self.bkp_dir, self.fechar)
        QWidget.setTabOrder(self.fechar, self.salvar)

        self.retranslateUi(Dialog)
        self.salvar.released.connect(Dialog.accept)
        self.fechar.released.connect(Dialog.reject)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Configura\u00e7\u00f5es", None))
        self.fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.bkp_dir.setText(QCoreApplication.translate("Dialog", u"Selecionar", None))
        self.bkp_pasta_padrao.setPlaceholderText(QCoreApplication.translate("Dialog", u"C:\\", None))
        self.label_8.setText(QCoreApplication.translate("Dialog", u"Pasta padr\u00e3o:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Dialog", u"Backup", None))
    # retranslateUi

