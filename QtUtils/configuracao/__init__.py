import os
from typing import Optional

from loguru import logger

from QtUtils import Application, QtUtils
from QtUtils.modulos import ImportarClasse
from QtUtils.colecoes.dialogos import Alerta
from . import base


class Configuracao(base.Configuration):

    def __init__(self, modelo: str) -> None:
        super().__init__(node=ImportarClasse(modelo).classe())
    
    def __alerta(self, texto: str, informacao: Optional[str]=None) -> None:
        if informacao:
            logger.debug(f"{texto}: {informacao}.")
        else:
            logger.debug(texto)
        if Application.instance():
            alerta = Alerta(parent=QtUtils.instancia_janela)
            alerta.setText(texto)
            if informacao:
                alerta.setInformativeText(informacao)
            alerta.exec_()
    
    def __janela_configuracao(self):
        if self.root.janela().exec_():
            self.carregar()
        else:
            QtUtils.encerrar()
    
    def carregar(self) -> None:
        if os.path.exists(QtUtils.definicoes.arquivo_configuracao):
            try:
                self.load(QtUtils.definicoes.arquivo_configuracao)
            except Exception as erro:
                self.__alerta(
                    informacao="Uma reconfiguração será necessária.\nErro: {erro}.",
                    texto="Erro ao carregar as configurações.",
                )
                self.__janela_configuracao()
        else:
            self.__alerta(
                informacao=(
                    "Trata-se de uma nova instalação,\n"
                    "ou o arquivo foi excluído ou movido."
                ),
                texto="Arquivo de configurações inexistente.",
            )
            self.__janela_configuracao()