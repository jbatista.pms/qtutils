from QtUtils import QtUtils

from .base import *
from .controllers import ConfiguracaoJanelaLocal, ConfiguracaoJanelaRede


class ConfiguracaoModeloLocal(Node):
    diretorio_backup = StringField(default=QtUtils.definicoes.diretorio_backup)
    diretorio_versionamento = StringField(default='')
    janela = ConfiguracaoJanelaLocal


class DadosRede(Node):
    servidor = StringField(null=True)
    porta = IntegerField(null=True)


class ConfiguracaoModeloRede(Node):
    diretorio_backup = StringField(default=QtUtils.definicoes.diretorio_backup)
    diretorio_versionamento = StringField(default='')
    rede = ExtendedField(DadosRede)
    janela = ConfiguracaoJanelaRede