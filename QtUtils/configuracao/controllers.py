import os
from PySide2 import QtWidgets

from QtUtils import QtUtils, subwindowsbase
from QtUtils.rede import porta_disponivel
from QtUtils.colecoes import dialogos

from .views import configuracao_local, configuracao_rede


class ConfiguracaoJanelaLocal(subwindowsbase.Formulario):
    classe_ui = configuracao_local.Ui_Dialog

    def accept(self):
        QtUtils.config.diretorio_backup = self.ui.bkp_pasta_padrao.text()
        QtUtils.salvar_configuracoes()
        return super().accept()

    def inicializar(self, *args, **kwargs):
        self.ui.bkp_pasta_padrao.setText(QtUtils.config.diretorio_backup)
    
    def selecionar_diretorio_bkp(self):
        diretorio = QtWidgets.QFileDialog.getExistingDirectory(
            parent=self,
            caption='Selecionar diretório',
            dir=os.environ['HOMEPATH'],
        )
        if diretorio:
            self.ui.bkp_pasta_padrao.setText(diretorio)


class ConfiguracaoJanelaRede(ConfiguracaoJanelaLocal):
    classe_ui = configuracao_rede.Ui_Dialog

    def accept(self):
        if self.ui.cmp_servidor.text():
            QtUtils.config.rede.servidor = self.ui.cmp_servidor.text()
            QtUtils.config.rede.porta = self.ui.cmp_porta.value()
            return super().accept()
        else:
            alerta = dialogos.Alerta(parent=self)
            alerta.setText("Os campos 'Servidor' e 'Senha' devem ser preenchidos.")
            alerta.exec_()

    def inicializar(self, *args, **kwargs):
        self.ui.bkp_pasta_padrao.setText(QtUtils.config.diretorio_backup)
        self.ui.cmp_porta.setValue(QtUtils.config.rede.porta or porta_disponivel())
        self.ui.cmp_servidor.setText(QtUtils.config.rede.servidor)
    
    def selecionar_diretorio_bkp(self):
        diretorio = QtWidgets.QFileDialog.getExistingDirectory(
            parent=self,
            caption='Selecionar diretório',
            dir=os.environ['HOMEPATH'],
        )
        if diretorio:
            self.ui.bkp_pasta_padrao.setText(diretorio)
    