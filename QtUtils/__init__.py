import argparse
import os
from pathlib import WindowsPath
import shutil
import sys
from collections import OrderedDict

import pytz
from cefpython3 import cefpython
from dotenv import load_dotenv
from loguru import logger
from PySide2 import QtCore, QtWidgets
from typing_extensions import Final

load_dotenv()

from .modulos import ImportarClasse


class Application(QtWidgets.QApplication):
    timer = None

    def __init__(self, args):
        super(Application, self).__init__(args)
        self.timer = self.createTimer()

    def createTimer(self):
        timer = QtCore.QTimer()
        timer.timeout.connect(self.onTimer)
        timer.start(10)
        return timer

    def onTimer(self):
        cefpython.MessageLoopWork()

    def stopTimer(self):
        # Stop the timer after Qt message loop ended, calls to MessageLoopWork()
        # should not happen anymore.
        self.timer.stop()


class Definicoes(object):
    MODO_LOCAL: Final = 'MODO_LOCAL'
    MODO_REDE: Final = 'MODO_REDE'
    ambiente_desenvolvimento: Final = os.getenv("QTUTILS_DESENVOLVIMENTO", None) == '1'

    def __init__(self, **definicoes) -> None:
        self.__definicoes = definicoes.copy()
        self.abreviatura_nome: Final = definicoes.get('abreviatura_nome')
        self.classe_janela_principal: Final = definicoes.get('classe_janela_principal')
        self.controladores: Final = definicoes.get('controladores', [])
        self.inicializar_cef: Final = definicoes.get('inicializar_cef')
        self.modelos: Final = definicoes.get('modelos', [])
        self.nome_aplicacao: Final = definicoes.get('nome_aplicacao')
        self.verificar_atualizacao: Final = definicoes.get('verificar_atualizacao', False)
        # Tratar opção modo de execução
        modo_execucao = definicoes.get('modo_execucao', self.MODO_LOCAL)
        if modo_execucao in (self.MODO_LOCAL, self.MODO_REDE):
            self.modo: Final = modo_execucao
        else:
            raise Exception(
                "Definição 'modo' deve ser Definicoes.CLIENTE "
                "ou Definicoes.SERVIDOR."
            )
        self.modo_local: Final = modo_execucao == self.MODO_LOCAL
        self.modo_rede: Final = modo_execucao == self.MODO_REDE
        # Definir modelo de configurações
        self.modelo_configuracao: Final = self.__definir_modelo_configuracoes()
        # Definir diretórios para armazenamento
        self.diretorio_base: Final = self.__definir_diretorio_base()
        self.diretorio_backup: Final = os.path.join(self.diretorio_base, 'backups')
        self.diretorio_de_trabalho: Final = WindowsPath.cwd()
        # Definir arquivos para armazenamento
        self.arquivo_configuracao: Final = os.path.join(self.diretorio_base, 'config.json')
        # Definir fuso horário padrão
        self.local_tz: Final = pytz.timezone('America/Sao_Paulo')
        # Contruir ambiente
        self.__mover_diretorio_antigo(self.diretorio_base)
        self.__criar_diretorios()
    
    def __criar_diretorios(self) -> None:
        # Criar diretório base caso não exista
        if not os.path.exists(self.diretorio_base):
            os.mkdir(self.diretorio_base)
        # Criar diretório de backups caso não exista
        if not os.path.exists(self.diretorio_backup):
            os.mkdir(self.diretorio_backup)

    def __definir_diretorio_base(self) -> None:
        if self.ambiente_desenvolvimento:
            return os.path.join(os.getcwd(), 'dev', self.abreviatura_nome)
        return os.path.join(os.environ['LOCALAPPDATA'], self.abreviatura_nome)
    
    def __definir_modelo_configuracoes(self):
        if 'modelo_configuracao' in self.__definicoes:
            return self.__definicoes.get('modelo_configuracao')
        elif self.modo_local:
            return 'QtUtils.configuracao.modelos.ConfiguracaoModeloLocal'
        return 'QtUtils.configuracao.modelos.ConfiguracaoModeloRede' 
    
    def __mover_diretorio_antigo(self, dir_novo: str) -> None:
        if not self.ambiente_desenvolvimento:
            dir_antigo = os.path.join(os.environ['APPDATA'], self.abreviatura_nome)
            if os.path.isdir(dir_antigo) and not os.path.exists(dir_novo):
                shutil.copytree(dir_antigo, dir_novo)
                shutil.rmtree(dir_antigo)
    

class QtUtilsBase(object):
    __aplication = None # Instância principal de aplicação
    __configuracao = None # Instância da classe configuracao.ConfiguracaoBase
    __configurado = False # Se execução foi devidamente configurado
    __controladores = [] # Controladores
    __definicoes = None # Definições de execução
    __instance = None # Intância única de QtUtilsBase
    __jp_classe = None # Classe de janela principal
    __jp_instancia = None # Instância de janela principal

    def __init__(self) -> None:
        # Configurar argumentos passados por linha de comando
        parser = argparse.ArgumentParser(description='QtUtils')
        parser.add_argument('--debug', action='store_true')
        parser.add_argument('--tarefas', nargs="*", type=str)
        self.arqs_pass: Final = parser.parse_args()

    def __new__(cls):
        if QtUtilsBase.__instance is None:
            QtUtilsBase.__instance = object.__new__(cls)
        return QtUtilsBase.__instance
    
    def __controladores_configurar(self):
        logger.debug("Configurando controladores.")
        controladores = [
            'QtUtils.crypto.ControladorCrypto',
            'QtUtils.servidor.ControladorServidor',
            'QtUtils.db.ControladorDB',
        ] 
        controladores += self.__definicoes.controladores
        controladores += ['QtUtils.user.controlador.ControladorAutenticacao']
        for con in controladores:
            logger.debug(f"Configurando controlador {con}.")
            con_classe = ImportarClasse(con).classe()
            con_instancia = con_classe()
            con_instancia.configurar()
            self.__controladores.append(con_instancia)
    
    def __controladores_encerrar(self):
        logger.debug("Encerrando controladores.")
        for con in self.__controladores:
            logger.debug(f"Encerrando controlador {con}.")
            con.encerrar()
    
    def __controladores_executar(self):
        logger.debug("Executando controladores.")
        for con in self.__controladores:
            logger.debug(f"Executando controlador {con}.")
            con.executar()
    
    @logger.catch
    def __executar_tarefas(self):
        if self.arqs_pass.tarefas:
            from QtUtils.tarefas import Tarefas
            Tarefas.executar_lote(*self.arqs_pass.tarefas)
            self.encerrar()

    def __verificar_atualizacao(self):
        if self.definicoes.verificar_atualizacao:
            from QtUtils.atualizacao.mantenedores import MantenedorAtualizacao
            from QtUtils.db import ControladorDB
            mant_atualizacao = MantenedorAtualizacao(
                data_hora_base=ControladorDB().data()
            )
            if mant_atualizacao.necessaria_atualizacao():
                mant_atualizacao.atualizar()        
    
    @property
    def config(self):
        return self.__configuracao.root

    @logger.catch
    def configurar(self, definicoes: dict) -> bool:
        if self.__configurado:
            raise Exception("Aplicativo já configurado.")
        
        self.__definicoes = Definicoes(**definicoes)

        # Inicialização de módulo e mantenedor de registros
        from QtUtils.registro import Registro
        Registro.configurar()
        
        # Conversão dos arquivos fonte de janela em classes
        if self.__definicoes.ambiente_desenvolvimento:
            from QtUtils.qt import converter
            converter()

        # Inicialização de instância principal da aplicação
        self.__aplication = self.__aplication or Application(sys.argv)

        # Inicialização de mantenedor de configurações do cliente
        from QtUtils.configuracao import Configuracao
        self.__configuracao = Configuracao(modelo=self.definicoes.modelo_configuracao)
        self.__configuracao.carregar()

        # Execução de rotinas iniciais
        self.__controladores_configurar()
        self.__verificar_atualizacao()
        self.__executar_tarefas()
        
        # Importação e registro da classe da janela principal
        self.__jp_classe = ImportarClasse(self.definicoes.classe_janela_principal).classe()

        # Sinaliza a execução da aplicação foi configurada
        self.__configurado = True
        return True
    
    @property
    def definicoes(self):
        return self.__definicoes
    
    @logger.catch
    def encerrar(self, codigo_saida=0, encerrar_processo=True):
        # Encerrar controladores
        self.__controladores_encerrar()
        
        # Encerra agendador de loop de menssagens de CEFPython
        if not self.__aplication is None:
            self.__aplication.stopTimer()
            codigo_saida = self.__aplication.quit()
        
        # Encerra CEFPython
        cefpython.Shutdown()

        # Encerra janela principal
        if not self.__jp_instancia is None:
            del self.__jp_instancia
        
        # Encerra o processo principal do python
        if encerrar_processo:
            sys.exit(codigo_saida)
        
        # Sinaliza necessidade de reconfigurar aplicação
        self.__configurado = False
    
    @logger.catch
    def executar(self):
        # Configuração é necessária para execução
        if not self.__configurado:
            raise Exception("Aplicativo não configurado.")
        
        # Inicialização de CEF
        if self.definicoes.inicializar_cef:
            settings = {
                "browser_subprocess_path": "{}/{}".format(
                    cefpython.GetModuleDirectory(),
                    "subprocess"
                ),
                "context_menu": {"enabled": False},
                "remote_debugging_port": -1,
            }
            cefpython.Initialize(settings)
        
        # Inicialização da janela principal
        self.__jp_instancia = self.__jp_classe()
        self.__jp_instancia.show()

        # Execução de controladores
        self.__controladores_executar()

        # Inicialização do loop de eventos do Qt
        self.encerrar(codigo_saida=self.__aplication.exec_())
    
    @property
    def instancia_janela(self):
        return self.__jp_instancia
    
    def salvar_configuracoes(self):
        self.__configuracao.save(self.__definicoes.arquivo_configuracao)


QtUtils = QtUtilsBase()