import datetime, os
from collections import OrderedDict
from typing import Union

from loguru import logger
from peewee import *
from playhouse.signals import Model
from PySide2 import QtWidgets

from QtUtils import QtUtils
from QtUtils.controladores import ControladorBase
from QtUtils.modulos import ImportarClasse
from .servidor import ControladorServidor


class ControladorDB(ControladorBase):
    __data_base: Union[PostgresqlDatabase, SqliteDatabase] = None
    __modelos: OrderedDict

    def configurar(self) -> None:
        logger.debug("Inicializando obtenção de modelos.")
        from QtUtils.backup.models import BackupHistorico
        from QtUtils.user.models import User
        self.__modelos = OrderedDict()
        self.__modelos[User._meta.table_name] = User
        self.__modelos[BackupHistorico._meta.table_name] = BackupHistorico
        
        # Obtendo modelos
        for mod in QtUtils.definicoes.modelos:
            logger.debug(f"Importando modelo '{mod}'.")
            cls_modelo = ImportarClasse(mod).classe()
            if cls_modelo is None:
                logger.error(f"Modelo {mod} não encontrado!")
                self.encerrar()
            else:
                self.__modelos[cls_modelo._meta.table_name] = cls_modelo

    def data(self):
        if isinstance(self.obter_database(), PostgresqlDatabase):
            date_time = self.obter_database().execute_sql("SELECT NOW()").fetchone()[0]
            return date_time.astimezone(QtUtils.definicoes.local_tz)
        return datetime.datetime.now(tz=QtUtils.definicoes.local_tz)
    
    def executar(self):
        # Criando tabelas
        logger.debug("Criando tabelas no banco de dados.")
        database = self.obter_database()
        for md in self.__modelos.values():
            md._meta.set_database(database=database)
        database.create_tables(self.__modelos.values(), safe=True)
    
    def modelos(self) -> dict:
        return self.__modelos.copy()

    def obter_database(self):
        if self.__data_base:
            return self.__data_base

        if QtUtils.definicoes.modo_rede:
            psql = ControladorServidor().postgres()
            self.__data_base = PostgresqlDatabase(**psql)
            try:
                self.__data_base.connect()
                return self.__data_base
            except OperationalError as e:
                msgBox = QtWidgets.QMessageBox()
                msgBox.setWindowTitle("Erro ao acessar o banco de dados.")
                msgBox.setInformativeText(
                    "Não foi possível a conexão ao banco de dados Postgres.\n"
                    "O banco de dados nas configurações será alterado "
                    "temporariamente para Sqlite3, até que seja corrigido o "
                    "problema.\n"
                    "A mensagem de erro retornada foi:\n%s" % e
                )
                msgBox.addButton(
                    "  Ir para configurações  ",
                    QtWidgets.QMessageBox.AcceptRole
                )
                msgBox.addButton(
                    "  Fechar aplicação  ",
                    QtWidgets.QMessageBox.RejectRole
                )
                ret = msgBox.exec_()
                if not ret:
                    if QtUtils.config.janela(parent=None).exec_():
                        self.__data_base = None
                        return self.obter_database()
                QtUtils.encerrar()


        database_file_name = 'database.db'
        if os.path.isfile(database_file_name):
            self.__database_dir = os.getcwd()
            self.__database_file = os.path.join(
                self.__database_dir, database_file_name
            )
        else:
            self.__database_file = os.path.join(
                QtUtils.definicoes.diretorio_base,
                database_file_name
            )
        self.__data_base = SqliteDatabase(self.__database_file)
        self.__data_base.connect()
        return self.__data_base

    def obter_database_file(self):
        return self.__database_file


DataBase = ControladorDB


class DateTimeTZField(DateTimeField):
    def adapt(self, value):
        if value and isinstance(value, str):
            return datetime.datetime.fromisoformat(value).astimezone(QtUtils.definicoes.local_tz)
        return value


class ModelBase(Model):
    def strftime(self, field, format='%d/%m/%Y'):
        field = getattr(self, field, None)
        if field:
            if isinstance(field, (datetime.date, datetime.datetime)):
                return field.strftime(format)
            elif isinstance(field, str):
                return field
        return ''
    
    def escolha(self, campo):
        if not campo is None and campo in self._meta.fields and self._meta.fields[campo].choices:
            return dict(self._meta.fields[campo].choices)[getattr(self,campo)]
        return None

    def dados(self):
        dados = self.__data__.copy()
        if 'id' in dados:
            dados.pop('id')
        return dados

    def campos(self):
        return list(self.dados().keys())
        
    def exportar_extras():
        return []
    
    def exportar_ignorar():
        return ['id',]
