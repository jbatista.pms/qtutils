import os
from datetime import datetime
from PySide2 import QtCore, QtWidgets

def mais_recente(arq1, arq2):
	if not arq2 in os.listdir():
		return True
	data_arq1 = datetime.fromtimestamp(os.path.getmtime(arq1))
	data_arq2 = datetime.fromtimestamp(os.path.getmtime(arq2))
	return data_arq1 > data_arq2

def converter_arquivo(arq):
	if arq.endswith('.ui'):
		result = arq[:-3]+'.py'
		if mais_recente(arq, result):
			os.system("pyside2-uic --from-imports {arq} -o {out}".format(
				arq=os.path.join('.',arq), out=result)
			)
	elif arq.endswith('.qrc'):
		result = arq[:-4]+'_rc.py'
		if mais_recente(arq, result):
			os.system("pyside2-rcc {arq} -o {out}".format(
				arq=arq, out=result)
			)

def converter(diretorio=os.getcwd()):
	os.chdir(diretorio)
	for d in os.listdir():
		if d in ['env', 'env_']:
			pass
		elif os.path.isfile(d):
			converter_arquivo(d)
		elif os.path.isdir(d):
			converter(os.path.join(diretorio,d))
			os.chdir(diretorio)


class QTableWidgetItem(QtWidgets.QTableWidgetItem):
	def setText(self, text):
		super().setText(" "+text+" ")
	
	def text(self):
		return super().text().strip()


def celula_data(celula):
	celula.setTextAlignment(
		QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter
	)