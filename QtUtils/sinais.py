from typing import Callable, Optional

from loguru import logger
from typing_extensions import Final


class Sinal(object):
    id = None # Id da instância
    __sinais = {} # Registro de objetos e seus respectivos sinais

    def __init__(self, ident: str=None) -> None:
        self.id: Final = ident or str(id(self))
        self.__sinais.update({self.id: {}})
    
    def __identificador(self, rec: Callable) -> str:
        return f"{rec.__self__.__class__.__name__}.{rec.__name__}.{rec.__hash__()}"

    def conectar(self, recebedor: Callable) -> Optional[str]:
        ident = self.__identificador(recebedor)
        logger.debug(f"Conectando o recebedor '{ident}' do sinal '{self.id}'.")
        if ident in self.__sinais[self.id]:
            raise ValueError(f"Registro de sinais já contém o recebedor '{ident}'")
        else:
            self.__sinais[self.id].update({ident: recebedor})
            return ident
    
    def desconectar(self, recebedor: Optional[Callable]=None) -> None:
        ident = self.__identificador(recebedor)
        logger.debug(f"Desconectando o recebedor '{ident}' do sinal '{self.id}'.")
        if ident in self.__sinais[self.id]:
            self.__sinais[self.id].pop(ident)
        else:
            raise ValueError(f"Não existe registro de sinais com o identificador '{ident}'.")
    
    def emitir(self, obj, *args, **kwargs) -> None:
        logger.debug(f"Emitindo o sinal '{self.id}'.")
        for recebedor in list(self.__sinais[self.id].values()):
            recebedor(obj=obj, sinal=self, *args, **kwargs)