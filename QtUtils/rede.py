import socketserver

def porta_disponivel() -> int:
    with socketserver.TCPServer(("localhost", 0), None) as s:
        return s.server_address[1]