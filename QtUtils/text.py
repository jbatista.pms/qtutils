from PySide2 import QtGui

TEXT_NOME = ['DO', 'DOS', 'DE', 'DA', 'DAS']

def text_format_uppercase(widget):
	fmt = QtGui.QTextCharFormat()
	fmt.setFontCapitalization(QtGui.QFont.AllUppercase)
	widget.setCurrentCharFormat(fmt)

def text_p_nome(texto):
	t = []
	for i in texto.split():
		if i.upper() in TEXT_NOME:
			t.append(i.lower())
		else:
			t.append(i.capitalize())
	return ' '.join(t)
