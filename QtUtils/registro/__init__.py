import os, shutil, sys, traceback
from datetime import timedelta
from loguru import logger
from PySide2 import QtWidgets

from QtUtils import QtUtils
from QtUtils.colecoes.dialogos import Erro
from QtUtils.singleton import Singleton


class RegistroBase(object, metaclass=Singleton):
    def __init__(self) -> None:
        self.__arq_registros = os.path.join(QtUtils.definicoes.diretorio_base, 'registros.log')
    
    def __configurar_excepthook(self) -> None:
        excepthook_nativo = sys.excepthook
        def novo_excepthook(exc_type, exc_value, exc_traceback):
            self.__mensagem_erro(f"{exc_type.__name__}: {exc_value}")
            logger.error(''.join(traceback.format_exception(exc_type, exc_value, exc_traceback)))
            excepthook_nativo(exc_type, exc_value, exc_traceback)
            QtUtils.encerrar()
        sys.excepthook = novo_excepthook
    
    def __configurar_loguru(self) -> None:
        if QtUtils.definicoes.ambiente_desenvolvimento or QtUtils.arqs_pass.debug:
            nivel = "DEBUG"
        else:
            nivel = "INFO"
        logger.add(
            catch=True,
            backtrace=True,
            enqueue=True,
            diagnose=True,
            level=nivel,
            sink=self.__arq_registros,
            retention=timedelta(days=30),
        )
        logger.debug(f"Iniciando registros em modo '{nivel}'.")
        logger.debug(
            "Variável 'ambiente_desenvolvimento'="
            f"{QtUtils.definicoes.ambiente_desenvolvimento}."
        )
        logger.debug(
            "Argumento de linha de comando 'debug'="
            f"{QtUtils.arqs_pass.debug}."
        )
    
    def __mensagem_erro(self, erro: str) -> None:
        if QtWidgets.QApplication.instance():
            msg = Erro()
            msg.setText("Ocorreu um erro grave e a aplicação será encerrada!")
            msg.setInformativeText(
                f"{erro}\n\n"
                "Para mais informações, consulte os registros."
            )
            msg.exec_()
    
    def arquivo_atual(self) -> str:
        return self.__arq_registros
    
    def configurar(self) -> None:
        self.__configurar_excepthook()
        self.__configurar_loguru()
    
    def dados(self) -> str:
        return open(self.__arq_registros, 'r').read()
    
    def salvar(self, destino: str) -> None:
        shutil.copy(self.__arq_registros, destino)


Registro = RegistroBase()