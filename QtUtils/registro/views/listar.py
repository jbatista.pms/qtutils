# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'listar.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(589, 444)
        Dialog.setMinimumSize(QSize(589, 444))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(-1, 0, -1, 0)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_3 = QPushButton(self.frame)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout.addWidget(self.pushButton_3)

        self.pushButton = QPushButton(self.frame)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(self.frame)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)


        self.gridLayout.addWidget(self.frame, 1, 0, 1, 2)

        self.conteudo = QTextEdit(Dialog)
        self.conteudo.setObjectName(u"conteudo")
        self.conteudo.setLineWrapMode(QTextEdit.NoWrap)
        self.conteudo.setReadOnly(True)

        self.gridLayout.addWidget(self.conteudo, 0, 1, 1, 1)

        QWidget.setTabOrder(self.conteudo, self.pushButton_3)
        QWidget.setTabOrder(self.pushButton_3, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.close)
        self.pushButton_3.released.connect(Dialog.copiar)
        self.pushButton_2.released.connect(Dialog.salvar)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Registros", None))
        self.pushButton_3.setText(QCoreApplication.translate("Dialog", u"Copiar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

