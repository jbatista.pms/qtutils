import os
from PySide2 import QtGui, QtWidgets

from QtUtils import subwindowsbase
from QtUtils.registro import Registro
from .views import listar


class ListarRegistro(subwindowsbase.Listar):
    classe_ui = listar.Ui_Dialog

    def copiar(self):
        QtWidgets.QApplication.clipboard().setText(self.ui.conteudo.toPlainText())

    def inicializar(self, *args, **kwargs):
        self.ui.conteudo.clear()
        self.ui.conteudo.setText(Registro.dados())
        self.ui.conteudo.moveCursor(QtGui.QTextCursor.End)
    
    def salvar(self):
        nome_arquivo = QtWidgets.QFileDialog.getSaveFileName(
            parent=self,
            dir=os.getenv("USERPROFILE"),
            caption="Exportar registros", 
            filter="Documentos de texto (*.log);;All Files (*)"
        )
        if nome_arquivo[0]:
            Registro.salvar(nome_arquivo[0])
