

class Mensageiro(object):
    __sub_windows = []
    
    def __limpar_sub_windows(self):
        if self in self.__sub_windows:
            self.__sub_windows.pop(self.__sub_windows.index(self))
    
    def accept(self):
        self.__limpar_sub_windows()
        return super().accept()

    def contruir(self, *args, **kwargs):
        if hasattr(super(), 'contruir'):
            super().contruir(*args, **kwargs)
        self.__sub_windows.append(self)

    def closeEvent(self, event):
        self.__limpar_sub_windows()
        if self.teste_fechar():
            event.accept()
        else:
            event.ignore()

    def enviar_sinal_atualizacao(self, instance):
        for sw in self.__sub_windows:
            sw.receber_sinal_atualizacao(instance)

    def enviar_sinal_exclusao(self, instance):
        for sw in self.__sub_windows:
            sw.receber_sinal_exclusao(instance)

    def receber_sinal_atualizacao(self, instance):
        """ Método chamado toda vez que uma janela é fechada.
        Usado para sinalizar possíveis alterações no banco de dados"""
        pass

    def receber_sinal_exclusao(self, instance):
        """ Método chamado toda vez que uma janela é fechada.
        Usado para sinalizar possíveis alterações no banco de dados"""
        pass
    
    def reject(self):
        self.__limpar_sub_windows()
        return super().reject()