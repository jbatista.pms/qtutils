import csv
from PySide2 import QtWidgets
from peewee import *
from QtUtils.colecoes import dialogos

def exportar(parent, query):
    def obter_colunas(model, origem=None, nomes={}, colunas=[]):
        model = model._meta.model
        if not model._meta.table_name in nomes:
            cols = {}
            for nome, tipo in model._meta.fields.items():
                novo_nome = origem+'__'+nome if not origem is None else nome
                if not nome in model.exportar_ignorar() and not isinstance(tipo, ForeignKeyField):
                    cols.update({nome: novo_nome})
                    colunas.append(novo_nome)
            if origem is None:
                for extras in model._meta.model.exportar_extras():
                    model_classe = model
                    referencias = model_classe._meta.refs
                    model_func = None
                    if extras.__class__ == list:
                        model_func = extras[1]
                        extras = extras[0]
                    for ex in extras.split('.'):
                        temp = getattr(model_classe, ex, None)
                        if callable(temp):
                            if model_func:
                                model_classe = model_func
                            else:
                                colunas.append(extras)
                                model_classe = None
                        else:
                            if temp:
                                model_classe = referencias[temp]
                                referencias = model_classe._meta.refs
                            else:
                                model_classe = None
                                break
                    if model_classe:
                        nomes_extras, colunas = obter_colunas(
                            model=model_classe,
                            origem=extras,
                            nomes=nomes,
                            colunas=colunas
                            )
                        cols.update(nomes_extras)
            nomes.update({model._meta.table_name: cols})
        return nomes, colunas
    
    def obter_registro(instance, nomes):
        """ Obter dados de registro """
        registro = {}
        model = instance._meta.model
        for nome, tipo in instance._meta.fields.items():
            if not nome in model.exportar_ignorar() and not isinstance(tipo, ForeignKeyField):
                valor = getattr(instance, nome)
                if isinstance(tipo, (DateField, DateTimeField)) and not valor is None:
                    valor = valor.strftime('%d/%m/%Y')
                elif not instance.escolha(nome) is None:
                    valor = instance.escolha(nome)
                registro.update({nomes[instance._meta.table_name][nome]: valor})
        return registro
    
    # Obter nome de arquivo de saída
    nome_arquivo = QtWidgets.QFileDialog.getSaveFileName(parent,
            "Exportar relatório", '',
            "CSV com cabeçalho (*.csv);;All Files (*)")
    if not (nome_arquivo and query):
        return
    
    # Abrir arquivo e verificar
    try:
        arq = open(nome_arquivo[0], 'w')
    except PermissionError:
        dialogos.Alerta(
            parent=parent,
            text="Não foi possível criar o arquivo.\nTalvez esteja aberto.\nVerifique!"
            ).exec_()
        raise PermissionError

    # Coletar todos os registros
    nomes, colunas = obter_colunas(query[0])
    registros = []
    for instance in query:
        registro = {i:'' for i in colunas}
        registro.update(obter_registro(instance, nomes))
        for extras in instance._meta.model.exportar_extras():
            instance_temp = instance
            instance_funct = None
            if extras.__class__ == list:
                instance_funct = extras[1]
                extras = extras[0]
            for ex in extras.split('.'):
                if callable(getattr(instance, ex, None)):
                    if instance_funct:
                        instance_temp = getattr(instance_temp, ex).__call__()
                        break
                    else:
                        registro.update({extras: getattr(instance_temp, ex, None).__call__()})
                        instance_temp = None
                else:
                    instance_temp = getattr(instance_temp, ex, None)
            if instance_temp:
                registro.update(obter_registro(instance_temp, nomes))
        registros.append(registro)

    # Salvar registros no arquivo
    colunas.sort()
    arq_csv = csv.DictWriter(
        arq, fieldnames=colunas, delimiter=';', lineterminator='\n'
        )
    arq_csv.writeheader()
    arq_csv.writerows(registros)
    arq.close()
