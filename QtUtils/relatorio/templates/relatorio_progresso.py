# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'relatorio_progresso.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(400, 113)
        Dialog.setMinimumSize(QSize(400, 113))
        Dialog.setMaximumSize(QSize(400, 113))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.mensagem = QLabel(Dialog)
        self.mensagem.setObjectName(u"mensagem")
        self.mensagem.setAlignment(Qt.AlignCenter)
        self.mensagem.setWordWrap(True)

        self.gridLayout.addWidget(self.mensagem, 0, 0, 1, 1)

        self.progressBar = QProgressBar(Dialog)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setValue(24)

        self.gridLayout.addWidget(self.progressBar, 1, 0, 1, 1)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Criando relat\u00f3rio", None))
        self.mensagem.setText(QCoreApplication.translate("Dialog", u"Ap\u00f3s a conclus\u00e3o o arquivo ser\u00e1 aberto no vizualizador de PDF padr\u00e3o do sistema.", None))
    # retranslateUi

