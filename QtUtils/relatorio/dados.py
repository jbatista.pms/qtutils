import json
import peewee
from collections import Iterable
from QtUtils.relatorio.odt import JSONEncoder


class ModeloRelatorio(object):
    dados = lambda o: {}
    extras = []
    modelo = None
    funcoes = {}


def dados_json(objetos, modelos=[]):
    """
    Exporte dados de registros serializados.
    Exemplo:
    obter_dados(
        objetos=[servidor,], 
        modelos=[ServidorModeloRelatorio,], 
    )
    """
    return json.dumps(obter_dados(objetos=objetos, modelos=modelos), cls=JSONEncoder)


def obter_dados(objetos, modelos=[]):
    """
    Exporte dados de registros.
    Exemplo:
    obter_dados(
        objetos=[servidor,], 
        modelos=[ServidorModeloRelatorio,],
    )
    """
    modelos_dict = {m.modelo:m for m in modelos}
    todos = []
    for o in objetos:
        if isinstance(o, dict):
            todos.append(o)
            continue
        mdl = modelos_dict.get(o.__class__, ModeloRelatorio)
        dados = o.__data__.copy()
        dados.update(mdl.dados(o))
        for m,f in mdl.funcoes.items():
            prdt = f(o)
            if isinstance(prdt, Iterable) and not isinstance(prdt, str):
                dados.update({m: obter_dados(objetos=prdt, modelos=modelos)})
            else:
                dados.update({m: prdt})
        for e in mdl.extras:
            ex = getattr(o, e)
            if isinstance(ex, peewee.ModelSelect):
                dados.update({e: obter_dados(objetos=list(ex.select()), modelos=modelos)})
            else:
                dados.update({e: ex})
        todos.append(dados)
    return todos