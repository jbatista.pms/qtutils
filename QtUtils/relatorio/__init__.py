import base64, os, shutil
from collections import defaultdict
from http.server import HTTPServer, BaseHTTPRequestHandler
from tempfile import mkdtemp

from loguru import logger
from py3o.template import Template
from PySide2 import QtCore, QtWidgets

from QtUtils import subwindowsbase
from QtUtils.colecoes.dialogos import Erro
from QtUtils.servidor import ControladorServidor

from .cef import CefWidget
from .dados import obter_dados
from .templates import relatorio_view, relatorio_progresso


class HandlerRelatorio(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/pdf')
        self.send_header('Content-Disposition', 'inline; filename="relatorio.pdf"')
        self.end_headers()
        self.wfile.write(self.server.relatorio)
    
    def log_message(self, format, *args):
        pass


class Relatorio(subwindowsbase.Formulario):
    _documento = None
    classe_ui = relatorio_progresso.Ui_Dialog

    def __init__(self, objetos, template, modelos=[], duplex=False, *args, **kwargs):
        self.objetos = objetos
        self.duplex = duplex
        self.template = template
        self.modelos = modelos
        self.dir_temp = mkdtemp()
        super(Relatorio, self).__init__(*args, **kwargs)

    def atualizar_progresso(self, progresso):
        self.ui.progressBar.setValue(int(progresso))

    def atualizar_mensagem(self, mensagem):
        self.ui.mensagem.setText(mensagem)

    def conclusao(self, documento):
        self._documento = documento
        self.close()

    def close(self):
        self.encerrar_tarefa()
        if os.path.exists(self.dir_temp):
            shutil.rmtree(self.dir_temp)
        return super().close()

    def encerrar_tarefa(self):
        if self.th and self.th.isRunning():
            self.th.terminate()

    def erro(self, msg: str) -> None:
        jnl = Erro(parent=self)
        jnl.setText("Falha na geração do relatório!")
        jnl.setInformativeText(msg)
        jnl.exec_()
        self.close()
    
    @property
    def documento(self):
        return self._documento
    
    def gerar(self) -> bool:
        self.exec_()
        return bool(self._documento)

    def inicializar(self, *args, **kwargs):
        self.th = RelatorioThread(
            self.objetos, 
            self.duplex, 
            self.template,
            self.modelos,
            dir_temp=self.dir_temp
        )
        self.th.conclusao.connect(self.conclusao)
        self.th.erro.connect(self.erro)
        self.th.updateProgress.connect(self.atualizar_progresso)
        self.th.updateMensagem.connect(self.atualizar_mensagem)
        self.th.conclusao.connect(self.conclusao)
        self.th.start()
    
    def ver_documento_subjanela(self):
        if self._documento:
            RelatorioViewSubWindows(parent=self.parent(), relatorio=self._documento).show()


class RelatorioThread(QtCore.QThread):
    conclusao = QtCore.Signal(bytes)
    erro = QtCore.Signal(str)
    updateProgress = QtCore.Signal(int)
    updateMensagem = QtCore.Signal(str)

    def __init__(self, objetos, duplex, template, modelos, dir_temp):
        self.objetos = objetos
        self.dir_temp = dir_temp
        self.duplex = duplex
        self.template = template
        self.modelos = modelos
        self.etapas = 4
        QtCore.QThread.__init__(self)
    
    def obter_documentos(self, dados: list) -> dict:
        agrupamento = defaultdict()
        for i, d in enumerate(dados):
            doc_nome = os.path.join(self.dir_temp, f'{i}.odt')
            doc = Template(template=self.template, outfile=doc_nome)
            doc.render({'obj': d, 'duplex': self.duplex})
            doc_base64 = base64.urlsafe_b64encode(s=open(doc_nome, 'rb').read())
            agrupamento[str(i)] = doc_base64.decode()
        return agrupamento
    
    def run(self):
        self.updateProgress.emit(1)
        self.updateMensagem.emit("(1/4) Preparando dados...")
        dados = obter_dados(self.objetos, modelos=self.modelos)
        self.updateProgress.emit(25)
        self.updateMensagem.emit("(2/4) Criando documento...")
        documentos = self.obter_documentos(dados=dados)
        self.updateProgress.emit(50)
        self.updateMensagem.emit("(3/4) Convertendo...")
        sucesso, id_ = ControladorServidor().enviar_documentos(documentos)
        if sucesso:
            self.updateProgress.emit(75)
            self.updateMensagem.emit("(4/4) Obtendo...")
            sucesso, doc_base64 = ControladorServidor().obter_documentos(id_=id_)
            if sucesso:
                docm = base64.urlsafe_b64decode(doc_base64)
                self.updateProgress.emit(100)
                self.updateMensagem.emit("Pronto!")
                self.conclusao.emit(docm)
        else:
            self.erro.emit(id_)


class RelatorioView(object):
    classe_ui = relatorio_view.Ui_Dialog
    unica_subwindow = False

    def __init__(self, parent, relatorio):
        self.relatorio = relatorio
        super().__init__(parent=parent)
    
    def carregar_url(self, url):
        self.cef.loadUrl(url)
    
    def closeEvent(self, event):
        self.close()
        self.th.terminate()
        self.cef.browser.CloseBrowser()
        event.accept()

    def inicializar(self, *args, **kwargs):
        self.cef = CefWidget(self)
        
        m_vbox = QtWidgets.QVBoxLayout()
        m_vbox.setContentsMargins(0, 0, 0, 0)
        m_vbox.addWidget(self.cef)

        self.ui.frame.setLayout(m_vbox)
        self.cef.embedBrowser()

        self.th = ServerThread(self.relatorio)
        self.th.enviar_url.connect(self.carregar_url)
        self.th.start()


class RelatorioViewDialog(RelatorioView, subwindowsbase.Formulario):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setGeometry(self.geometry_media())



class RelatorioViewSubWindows(RelatorioView, subwindowsbase.Listar):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.subwindow.setGeometry(0,0,self.mdiArea.width(),self.mdiArea.height())


class RenderizarArquivoThread(QtCore.QThread):
    conclusao = QtCore.Signal(bytes)

    def __init__(self, arquivo):
        self.arquivo = arquivo
        QtCore.QThread.__init__(self)


class ServerThread(QtCore.QThread):
    enviar_url = QtCore.Signal(str)
    finalizar = QtCore.Signal()

    def __init__(self, relatorio):
        self.relatorio = relatorio
        QtCore.QThread.__init__(self)
    
    def run(self):
        server = HTTPServer(('localhost', 0), HandlerRelatorio)
        server.timeout = 10
        server.relatorio = self.relatorio
        self.enviar_url.emit('http://%s:%i' % server.server_address)
        server.handle_request()
        self.finalizar.emit()
