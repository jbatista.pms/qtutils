import json
from datetime import date, datetime
from io import BytesIO

import requests


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.strftime('%d/%m/%Y %H:%M:%S')
        elif isinstance(o, date):
            return o.strftime('%d/%m/%Y')
        elif hasattr(o, '__data__'):
            return o.__data__
        return str(o)


def render_pdf(documento, dados, server):
    data = json.dumps(dados, cls=JSONEncoder)
    files = {'tmpl_file': open(documento, 'rb'),}
    fields = {
        "targetformat": 'pdf',
        "datadict": data,
        "image_mapping": json.dumps({}),
    }
    try:
        r = requests.post(server, data=fields, files=files)
    except Exception as e:
        return e, False
    if r.status_code == 400:
        # server says we have a problem...
        # let's give the info back to our human friend
        print(r.json())
    pdf = BytesIO()
    for chunk in r.iter_content(1024):
        pdf.write(chunk)
    files['tmpl_file'].close()
    return pdf.getvalue(), True