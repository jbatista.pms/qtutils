import logging
from copy import deepcopy
from PySide2 import QtCore, QtWidgets

from .mensageiro import Mensageiro


class SubWindowsBase(Mensageiro):
    classe_ui = None
    instance = None
    mdiArea = None
    sub_window_flags = ()
    unica_subwindow = True
    unica_subwindow_instance = None

    def __init__(self, parent=None, *args, **kwargs):
        self.instance = self.instanciar(*args, **kwargs)
        super(SubWindowsBase, self).__init__(parent)
        
    def atualizar(self):
        """ Carrega os dados na janela.
        Pode ser usado para atribuir características a janela ou carregar dados do banco de dados"""
        pass

    def contruir(self, *args, **kwargs):
        self.ui = self.obter_classe_ui(*args, **kwargs)
        self.ui.setupUi(self)
        super().contruir(*args, **kwargs)
        self.inicializar(*args, **kwargs)

    def close(self):
        if self.mdiArea and self.parent() in self.mdiArea.subWindowList():
            self.mdiArea.removeSubWindow(self.parent())
        super(SubWindowsBase, self).close()

    def geometry_media(self, geometry):
        width = geometry.width()//1.5
        height = geometry.height()//1.5
        left = (geometry.width()-width)//2
        top = (geometry.height()-height)//2
        return QtCore.QRect(left, top, width, height)

    def inicializar(self, *args, **kwargs):
        """ Inicializa a janela """
        pass

    def instanciar(self, *args, **kwargs):
        return deepcopy(kwargs.get('instance', None))

    def obter_classe_ui(self, *args, **kwargs):
        return self.classe_ui()

    def teste_unica(self, window):
        """ Teste complementar para verificar se janela é unica.
        Pode ser usado para verificar se existe outra janela referente
        ao mesmo registro do banco de dados."""
        return True
    
    def teste_fechar(self):
        return True

    def show(self):
        if self.unica_subwindow_instance:
            if self.mdiArea:
                self.mdiArea.setActiveSubWindow(self.unica_subwindow_instance)
        else:
            super(SubWindowsBase, self).show()


class Formulario(SubWindowsBase, QtWidgets.QDialog):
    sub_window_flags = (
        QtCore.Qt.CustomizeWindowHint |
        QtCore.Qt.WindowCloseButtonHint |
        QtCore.Qt.MSWindowsFixedSizeDialogHint
    )

    def __init__(self, *args, **kwargs):
        super(Formulario, self).__init__(*args, **kwargs)
        self.contruir(*args, **kwargs)
        
    def geometry_media(self):
        return super().geometry_media(QtWidgets.QDesktopWidget().screenGeometry())

    def instanciar(self, *args, **kwargs):
        return kwargs.get('instance', None)
        

class Listar(SubWindowsBase, QtWidgets.QWidget):
    sub_window_flags = (
        QtCore.Qt.CustomizeWindowHint |
        QtCore.Qt.WindowCloseButtonHint |
        QtCore.Qt.WindowSystemMenuHint |
        QtCore.Qt.WindowMaximized
    )

    def __init__(self, parent=None, *args, **kwargs):
        super().__init__(parent=parent, *args, **kwargs)
        self.mdiArea = getattr(parent, 'mdiArea', None)
        if self.unica_subwindow:
            if self.mdiArea:
                for i in self.mdiArea.subWindowList():
                    if type(self) == type(i.widget()) and self.teste_unica(i.widget()):
                        self.unica_subwindow_instance = i
                        self.ui = i.widget().ui
                        break

        if not self.unica_subwindow_instance:
            self.contruir(*args, **kwargs)
            if self.mdiArea:
                self.subwindow = self.mdiArea.addSubWindow(self, self.sub_window_flags)
    
    def geometry_media(self):
        return super().geometry_media(self.mdiArea.geometry())
