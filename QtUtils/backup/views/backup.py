# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'backup.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(589, 513)
        Dialog.setMinimumSize(QSize(589, 513))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.tabWidget = QTabWidget(Dialog)
        self.tabWidget.setObjectName(u"tabWidget")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout_2 = QGridLayout(self.tab)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.pushButton = QPushButton(self.tab)
        self.pushButton.setObjectName(u"pushButton")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.pushButton.sizePolicy().hasHeightForWidth())
        self.pushButton.setSizePolicy(sizePolicy1)

        self.gridLayout_2.addWidget(self.pushButton, 2, 3, 1, 1)

        self.destino = QLineEdit(self.tab)
        self.destino.setObjectName(u"destino")
        self.destino.setReadOnly(True)

        self.gridLayout_2.addWidget(self.destino, 2, 1, 1, 2)

        self.label_5 = QLabel(self.tab)
        self.label_5.setObjectName(u"label_5")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy2)

        self.gridLayout_2.addWidget(self.label_5, 2, 0, 1, 1)

        self.csv = QRadioButton(self.tab)
        self.csv.setObjectName(u"csv")

        self.gridLayout_2.addWidget(self.csv, 1, 0, 1, 2)

        self.sqlite = QRadioButton(self.tab)
        self.sqlite.setObjectName(u"sqlite")
        self.sqlite.setChecked(True)

        self.gridLayout_2.addWidget(self.sqlite, 0, 0, 1, 2)

        self.frame = QFrame(self.tab)
        self.frame.setObjectName(u"frame")
        sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy3)
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_3 = QGridLayout(self.frame)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.horizontalSpacer_2 = QSpacerItem(218, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer_2, 1, 4, 1, 1)

        self.pushButton_2 = QPushButton(self.frame)
        self.pushButton_2.setObjectName(u"pushButton_2")
        sizePolicy1.setHeightForWidth(self.pushButton_2.sizePolicy().hasHeightForWidth())
        self.pushButton_2.setSizePolicy(sizePolicy1)

        self.gridLayout_3.addWidget(self.pushButton_2, 1, 3, 1, 1)

        self.pushButton_5 = QPushButton(self.frame)
        self.pushButton_5.setObjectName(u"pushButton_5")

        self.gridLayout_3.addWidget(self.pushButton_5, 1, 2, 1, 1)

        self.horizontalSpacer = QSpacerItem(219, 25, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer, 1, 0, 1, 2)


        self.gridLayout_2.addWidget(self.frame, 5, 0, 1, 4)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.gridLayout_4 = QGridLayout(self.tab_2)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.frame_2 = QFrame(self.tab_2)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame_2)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_3)

        self.pushButton_6 = QPushButton(self.frame_2)
        self.pushButton_6.setObjectName(u"pushButton_6")

        self.horizontalLayout.addWidget(self.pushButton_6)

        self.pushButton_4 = QPushButton(self.frame_2)
        self.pushButton_4.setObjectName(u"pushButton_4")

        self.horizontalLayout.addWidget(self.pushButton_4)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)


        self.gridLayout_4.addWidget(self.frame_2, 3, 0, 1, 2)

        self.pushButton_3 = QPushButton(self.tab_2)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.gridLayout_4.addWidget(self.pushButton_3, 0, 1, 1, 1)

        self.label = QLabel(self.tab_2)
        self.label.setObjectName(u"label")

        self.gridLayout_4.addWidget(self.label, 1, 0, 1, 2)

        self.arquivo = QLineEdit(self.tab_2)
        self.arquivo.setObjectName(u"arquivo")
        self.arquivo.setReadOnly(True)

        self.gridLayout_4.addWidget(self.arquivo, 0, 0, 1, 1)

        self.label_2 = QLabel(self.tab_2)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout_4.addWidget(self.label_2, 2, 0, 1, 2)

        self.tabWidget.addTab(self.tab_2, "")

        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)

        self.historico = QTableWidget(Dialog)
        if (self.historico.columnCount() < 3):
            self.historico.setColumnCount(3)
        __qtablewidgetitem = QTableWidgetItem()
        self.historico.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.historico.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.historico.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        self.historico.setObjectName(u"historico")
        self.historico.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.historico.setSelectionMode(QAbstractItemView.SingleSelection)
        self.historico.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.historico.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.historico.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.historico.horizontalHeader().setMinimumSectionSize(115)
        self.historico.horizontalHeader().setDefaultSectionSize(115)
        self.historico.horizontalHeader().setHighlightSections(False)
        self.historico.verticalHeader().setVisible(False)
        self.historico.verticalHeader().setHighlightSections(False)

        self.gridLayout.addWidget(self.historico, 3, 0, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(-1, 11, -1, -1)
        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_2.addWidget(self.label_3)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_5)

        self.label_4 = QLabel(Dialog)
        self.label_4.setObjectName(u"label_4")

        self.horizontalLayout_2.addWidget(self.label_4)

        self.limite = QComboBox(Dialog)
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.setObjectName(u"limite")

        self.horizontalLayout_2.addWidget(self.limite)


        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 1)

        QWidget.setTabOrder(self.tabWidget, self.sqlite)
        QWidget.setTabOrder(self.sqlite, self.csv)
        QWidget.setTabOrder(self.csv, self.destino)
        QWidget.setTabOrder(self.destino, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.pushButton_5)
        QWidget.setTabOrder(self.pushButton_5, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.arquivo)
        QWidget.setTabOrder(self.arquivo, self.pushButton_3)
        QWidget.setTabOrder(self.pushButton_3, self.pushButton_6)
        QWidget.setTabOrder(self.pushButton_6, self.pushButton_4)
        QWidget.setTabOrder(self.pushButton_4, self.limite)
        QWidget.setTabOrder(self.limite, self.historico)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.selecionar_destino)
        self.pushButton_2.released.connect(Dialog.criar_backup)
        self.pushButton_3.released.connect(Dialog.selecionar_arquivo)
        self.pushButton_4.released.connect(Dialog.restaurar_backup)
        self.pushButton_5.released.connect(Dialog.reject)
        self.pushButton_6.released.connect(Dialog.reject)
        self.limite.currentIndexChanged.connect(Dialog.atualizar)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Backup", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"  Selecionar destino  ", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Destino:", None))
        self.csv.setText(QCoreApplication.translate("Dialog", u"Em arquivos csv em arquivo zipado (n\u00e3o possibilita restaura\u00e7\u00e3o)", None))
        self.sqlite.setText(QCoreApplication.translate("Dialog", u"Em banco de dados Sqlite3", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Criar backup", None))
        self.pushButton_5.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Dialog", u"Criar", None))
        self.pushButton_6.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.pushButton_4.setText(QCoreApplication.translate("Dialog", u"Restaurar", None))
        self.pushButton_3.setText(QCoreApplication.translate("Dialog", u"  Selecionar arquivo  ", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"- Ao resturar o backup, a aplica\u00e7\u00e3o ser\u00e1 encerrada em seguida por seguran\u00e7a.", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"- As senhas de usu\u00e1rios de um backup s\u00e3o zeradas.", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Dialog", u"Restaurar", None))
        ___qtablewidgetitem = self.historico.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Dialog", u"Data", None));
        ___qtablewidgetitem1 = self.historico.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Dialog", u"Usu\u00e1rio", None));
        ___qtablewidgetitem2 = self.historico.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Dialog", u"Opera\u00e7\u00e3o", None));
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Hist\u00f3rico de backups e restaura\u00e7\u00f5es", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Limite:", None))
        self.limite.setItemText(0, QCoreApplication.translate("Dialog", u"10", None))
        self.limite.setItemText(1, QCoreApplication.translate("Dialog", u"25", None))
        self.limite.setItemText(2, QCoreApplication.translate("Dialog", u"50", None))
        self.limite.setItemText(3, QCoreApplication.translate("Dialog", u"100", None))
        self.limite.setItemText(4, QCoreApplication.translate("Dialog", u"250", None))
        self.limite.setItemText(5, QCoreApplication.translate("Dialog", u"500", None))
        self.limite.setItemText(6, QCoreApplication.translate("Dialog", u"Todos", None))

    # retranslateUi

