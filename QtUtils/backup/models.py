from datetime import datetime
from peewee import *

from QtUtils.db import DataBase, ModelBase
from QtUtils.user.models import User
from QtUtils.user.usuario import Usuario


class BackupHistorico(ModelBase):
    BACKUP = 1
    BACKUP_CSV = 2
    RESTAURACAO = 3
    OPERACAO_ESCOLHAS = (
        (BACKUP, 'Backup em sqlite3'),
        (BACKUP_CSV, 'Backup em csv'),
        (RESTAURACAO, 'Restauração'),
    )

    data = DateTimeField(default=DataBase().data)
    operacao = IntegerField(choices=OPERACAO_ESCOLHAS)
    user = ForeignKeyField(User, default=Usuario().getUsuario,null=True)
    backup_restauracao = ForeignKeyField('self', null=True)

    def select2(*args, **kwargs):
        return BackupHistorico.select(*args, **kwargs).order_by(BackupHistorico.data.desc())