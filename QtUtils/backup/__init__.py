import csv
import os
import zipfile
import tempfile

from loguru import logger
from peewee import *

from QtUtils.user.models import User
from QtUtils.user.usuario import Usuario
from QtUtils import QtUtils
from QtUtils.db import ControladorDB
from .models import BackupHistorico


def backup_csv(bd, arq_zip):
    def registros(dicts, tabela):
        return list(dicts)

    dir_temp = tempfile.TemporaryDirectory()
    bd_close = bd.is_closed()
    if bd_close:
        bd.connect()

    arqs_backup = []
    tabelas = bd.get_tables()
    for tb in tabelas:
        nome_arquivo = '%s.csv' % tb
        colunas = [i.name for i in bd.get_columns(tb)]
        tabela = Table(tb, colunas)
        tabela.bind(bd)
        arq = os.path.join(dir_temp.name, nome_arquivo)
        arqs_backup.append((arq, nome_arquivo))
        with open(arq, 'w', newline='') as csvfile:
            write = csv.DictWriter(csvfile, fieldnames=colunas)
            write.writeheader()
            write.writerows(registros(tabela.select().dicts(), tb))
    with zipfile.ZipFile(arq_zip, mode='w', compression=zipfile.ZIP_DEFLATED) as zfile:
        for arq, name in arqs_backup:
            zfile.write(arq, arcname=name)

    if bd_close:
        bd.close()
    dir_temp.cleanup()
    BackupHistorico.create(operacao=BackupHistorico.BACKUP_CSV)


class BackupSqlite(object):
    def __init__(self, bd_alvo, bd_bkp):
        self.__bdalvo = bd_alvo
        self.__bdbkp = bd_bkp
        self.ctrl_db = ControladorDB()

    def __preparar(self):
        self.__bdalvo.connect(reuse_if_open=True)
        self.__bdbkp.connect(reuse_if_open=True)

    def __salvar_tabela(self, modelo):
        if not modelo._meta.table_name in self.__bdbkp.get_tables():
            return
        colunas = [i.name for i in self.__bdbkp.get_columns(modelo._meta.table_name)]
        tabela = Table(modelo._meta.table_name, colunas)
        tabela.bind(self.__bdbkp)
        registros = tabela.select().dicts()
        if registros:
            # Identificar campos que não existam mais
            colunas = [i.name for i in self.__bdalvo.get_columns(modelo._meta.table_name)]
            colunas = [i for i in registros[0].keys() if not i in colunas]
            for k in colunas:
                for r in registros:
                    r.pop(k)
            modelo.insert_many(list(registros)).execute()

    def contruir(self):
        def registros(dicts, tabela):
            return dicts

        self.__preparar()
        with self.__bdbkp.atomic() as dba:
            BackupHistorico.create(operacao=BackupHistorico.BACKUP)
            for tbl_nome, modelo in self.ctrl_db.modelos().items():
                rgs = registros(modelo.select().dicts(), tbl_nome)
                with self.__bdbkp.bind_ctx([modelo,]):
                    modelo.create_table(safe=True)
                    for i in rgs:
                        modelo.insert(i).execute()
        self.__bdbkp.close()

    def encerrar(self):
        self.__bdalvo.close()
        self.__bdbkp.close()

    def restaurar(self):
        logger.info("Iniciando restauração de backup.")
        self.__preparar()
        modelos = self.ctrl_db.modelos().values()
        with self.__bdalvo.atomic() as dba:
            self.__bdalvo.drop_tables(modelos)
            self.__bdalvo.create_tables(modelos)
            for tlb_nome, mdl in self.ctrl_db.modelos().items():
                logger.debug(f"Restaurando tabela {tlb_nome}")
                self.__salvar_tabela(mdl)
                if isinstance(self.__bdalvo, PostgresqlDatabase):
                    ultimo_registro = mdl.select(mdl.id).order_by(mdl.id.desc()).dicts().first()
                    if ultimo_registro:
                        ultima_chave = ultimo_registro['id']+1
                        self.__bdalvo.execute_sql(
                            "ALTER SEQUENCE %s RESTART WITH %i" %
                            (tlb_nome+"_id_seq", ultima_chave)
                    )
        with self.__bdalvo.bind_ctx([BackupHistorico, User]):
            usuario_sistema = Usuario().getUsuario()
            if usuario_sistema != None:
                usuario_banco = User.get_or_none(User.nome_acesso==usuario_sistema.nome_acesso)
                if usuario_banco is None:
                    usuario_sistema = User.create(**usuario_sistema.dados())
                elif usuario_banco.id != usuario_sistema.id:
                    usuario_sistema = usuario_banco
            BackupHistorico.insert(
                backup_restauracao=BackupHistorico.select2().first(),
                operacao=BackupHistorico.RESTAURACAO,
                user=usuario_sistema,
            ).execute()