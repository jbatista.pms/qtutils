import os

from peewee import *
from PySide2 import QtWidgets, QtCore

from QtUtils import subwindowsbase, qt, QtUtils
from QtUtils.colecoes.dialogos import Alerta, Confirmacao, Informacao
from QtUtils.db import DataBase

from . import BackupSqlite, backup_csv
from .models import BackupHistorico
from .views import backup, backup_executando


class BackupDialogo(subwindowsbase.Formulario):
    __diretorio = QtUtils.config.diretorio_backup
    classe_ui = backup.Ui_Dialog
    
    def arquivo_destino(self):
        if self.ui.csv.isChecked():
            extensao = "zip"
        else:
            extensao = "sqlite3.db"
        return os.path.join(
            self.ui.destino.text(), 
            "{abrev}_{data_hora}.{ext}".format(
                abrev=QtUtils.definicoes.abreviatura_nome,
                data_hora=DataBase().data().strftime("%Y-%m-%d_%H-%M-%S"),
                ext=extensao
            )
        )

    def atualizar(self):
        historico = BackupHistorico.select2()
        if self.ui.limite.currentText().isdigit():
            historico = historico.limit(int(self.ui.limite.currentText()))
        historico_conta = historico.count()
        self.ui.historico.setRowCount(historico_conta)
        contador = 0
        formato_data = '%d/%m/%Y %H:%M:%S'
        for hst in historico:
            # Data
            item = qt.QTableWidgetItem()
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            item.setText(hst.strftime('data', format=formato_data))
            self.ui.historico.setItem(contador,0,item)
            # Operação
            item = qt.QTableWidgetItem()
            if hst.backup_restauracao is None:
                item.setText(hst.escolha('operacao'))
            else:
                texto = "%s para backup de %s" % (
                    hst.escolha('operacao'), 
                    hst.backup_restauracao.strftime('data', format=formato_data)
                )
                if hst.backup_restauracao.user:
                    texto += " criado por %s" % hst.backup_restauracao.user
                item.setText(texto)
            self.ui.historico.setItem(contador,2,item)
            # Usuário
            if hst.user:
                item = qt.QTableWidgetItem()
                item.setText(str(hst.user))
                self.ui.historico.setItem(contador,1,item)
            contador += 1
        self.ui.historico.resizeColumnsToContents()

    def criar_backup(self):
        if self.destino_valido():
            BackupConstruirDialogo(self,
                destino=self.arquivo_destino(),
                bkp_csv=self.ui.csv.isChecked()
            ).exec_()
            self.atualizar()
    
    def destino_valido(self):
        destino = self.ui.destino.text()
        if not destino:
            Alerta(self, text="Nenhum destino foi selecionado.").exec_()
            return False
        elif not os.path.isdir(destino):
            Alerta(self, text="O diretório destino não existe.").exec_()
            return False
        else:
            return True
    
    def inicializar(self, *args, **kwargs):
        self.atualizar()
        self.ui.destino.setText(self.__diretorio)

    def restaurar_backup(self):
        msg = "A restauração apagará o banco de dados atual.\nDeseja continuar?"
        if Confirmacao(self, text=msg).exec_():
            arquivo = self.ui.arquivo.text()
            if arquivo:
                BackupRestaurarDialogo(self, bd_bkp=arquivo).exec_()
            else:
                Informacao(self, text="Nenhum arquivo foi selecionado.").exec_()

    def selecionar_arquivo(self):
        nome_arquivo = QtWidgets.QFileDialog.getOpenFileName(
            parent=self,
            caption='Selecionar arquivo',
            dir=self.__diretorio,
            filter=("Arquivo de banco de dados (*.db)"),
            )
        if nome_arquivo[0]:
            self.ui.arquivo.setText(nome_arquivo[0])
        else:
            self.ui.arquivo.setText("")

    def selecionar_destino(self):
        self.__diretorio = QtWidgets.QFileDialog.getExistingDirectory(
            parent=self,
            caption='Selecionar destino',
            dir=self.__diretorio,
        )
        self.ui.destino.setText(self.__diretorio)


class BackupConstruirDialogo(subwindowsbase.Formulario):
    classe_ui = backup_executando.Ui_Dialog

    def close(self):
        QtCore.QThreadPool().waitForDone()
        super().close()

    def inicializar(self, *args, **kwargs):
        self.th = BackupConstruirDialogoThread(
            destino=kwargs['destino'],
            bkp_csv=kwargs['bkp_csv'],
        )
        self.th.conclusao.connect(self.close)
        self.th.start()


class BackupConstruirDialogoThread(QtCore.QThread):
    conclusao = QtCore.Signal()

    def __init__(self, destino, bkp_csv):
        self.destino = destino
        self.bkp_csv = bkp_csv
        QtCore.QThread.__init__(self)

    def run(self):
        bd = DataBase().obter_database()
        if os.path.isfile(self.destino):
            os.remove(self.destino)
        if self.bkp_csv:
            backup_csv(bd, self.destino)
        else:
            bd_bkp = SqliteDatabase(self.destino)
            BackupSqlite(bd_alvo=bd, bd_bkp=bd_bkp).contruir()
        self.conclusao.emit()


class BackupRestaurarDialogo(subwindowsbase.Formulario):
    classe_ui = backup_executando.Ui_Dialog

    def close(self):
        self.th.wait()
        super().close()

    def finalizar(self):
        self.close()
        QtCore.QCoreApplication.instance().quit()

    def inicializar(self, *args, **kwargs):
        self.ui.label.setText("Excutando restauração...")
        self.th = BackupRestaurarDialogoThread(bd_bkp=kwargs['bd_bkp'])
        self.th.conclusao.connect(self.finalizar)
        self.th.start()


class BackupRestaurarDialogoThread(QtCore.QThread):
    conclusao = QtCore.Signal()

    def __init__(self, bd_bkp):
        self.bd_bkp = bd_bkp
        QtCore.QThread.__init__(self)

    def run(self):
        bd = DataBase().obter_database()
        bd_bkp = SqliteDatabase(self.bd_bkp)
        BackupSqlite(bd_alvo=bd, bd_bkp=bd_bkp).restaurar()
        self.conclusao.emit()
