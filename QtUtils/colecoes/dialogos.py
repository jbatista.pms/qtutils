from PySide2 import QtWidgets, QtCore
from .views import alerta


class Alerta(QtWidgets.QMessageBox):
    def __init__(self, parent=None, title="Alerta!", *args, **kwargs):
        super().__init__(parent=parent, *args, **kwargs)
        self.setWindowTitle(title)
        self.setIcon(QtWidgets.QMessageBox.Warning)
        self.addButton(QtWidgets.QMessageBox.Ok)


class Confirmacao(QtWidgets.QMessageBox):
    def __init__(self, parent=None, title="Confirmação", *args, **kwargs):
        super().__init__(parent=parent, *args, **kwargs)
        self.setWindowTitle(title)
        self.setIcon(QtWidgets.QMessageBox.Question)
        self.addButton("Não", QtWidgets.QMessageBox.NoRole)
        self.addButton("Sim", QtWidgets.QMessageBox.YesRole)

    def exec_(self):
        return bool(super().exec_())


class Erro(QtWidgets.QMessageBox):
    def __init__(self, parent=None, title="Erro!", *args, **kwargs):
        super().__init__(parent=parent, *args, **kwargs)
        self.setWindowTitle(title)
        self.setIcon(QtWidgets.QMessageBox.Critical)
        self.addButton(QtWidgets.QMessageBox.Ok)


class Informacao(QtWidgets.QMessageBox):
    def __init__(self, parent=None, title="Informação", *args, **kwargs):
        super().__init__(parent=parent, *args, **kwargs)
        self.setWindowTitle(title)
        self.setIcon(QtWidgets.QMessageBox.Information)
        self.addButton(QtWidgets.QMessageBox.Ok)
