from QtUtils import QtUtils
from typing import Dict, Optional
from PySide2 import QtWidgets


class ErrosFormularioDialogo(QtWidgets.QMessageBox):
    def __init__(self, erros: Dict) -> None:
        super().__init__(parent=QtUtils.instancia_janela)
        self.setWindowTitle("Validação")
        self.setText("Erros encontrados no formulário:")
        self.setInformativeText(self.__formatar_texto(erros=erros))
    
    def __formatar_texto(self, erros: Dict, nivel: int=0) -> str:
        msg = ''
        for k,v in erros.items():
            if isinstance(v, dict):
                msg += f"{' ' * 6 * nivel}- {k}:\n"
                msg += self.__formatar_texto(erros=v, nivel=nivel+1)
            else:
                msg += f"{' ' * 6 * nivel}- {k}: {v}\n"
        return msg