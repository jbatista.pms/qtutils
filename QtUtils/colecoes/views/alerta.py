# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'alerta.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(361, 130)
        Dialog.setMinimumSize(QSize(361, 130))
        Dialog.setMaximumSize(QSize(361, 130))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.pushButton = QPushButton(Dialog)
        self.pushButton.setObjectName(u"pushButton")

        self.gridLayout.addWidget(self.pushButton, 1, 1, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer, 1, 0, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_2, 1, 2, 1, 1)

        self.alerta = QLabel(Dialog)
        self.alerta.setObjectName(u"alerta")
        self.alerta.setTextFormat(Qt.PlainText)
        self.alerta.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.alerta, 0, 0, 1, 3)


        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Alerta", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Entendi", None))
        self.alerta.setText(QCoreApplication.translate("Dialog", u"alerta", None))
    # retranslateUi

