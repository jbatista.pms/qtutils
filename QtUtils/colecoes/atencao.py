from PySide2 import QtCore, QtWidgets

from .views.atencao import *


class Control(QtWidgets.QDialog):
	def __init__(self, parent=None, mensagem="Atenção!", titulo="Atenção"):
		super(Control, self).__init__(parent=None)
		self.ui = Ui_Dialog()
		self.ui.setupUi(self)
		self.ui.label.setText(mensagem)
		self.setWindowTitle(titulo)
