from datetime import datetime
from peewee import *
from QtUtils.db import ModelBase
from QtUtils.user.usuario import Usuario
from QtUtils.user.models import User


class Historico(ModelBase):
    user = ForeignKeyField(User, null=True, db_column='user')
    data_criacao = DateTimeField(default=datetime.now)
    data_modificacao = DateTimeField(default=datetime.now)
    data_exclusao = DateTimeField(null=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._original = self.__data__.copy()

    def __salvar_historico(self, not_user=False):
        novo = ({c: getattr(self, c) for c in self.__data__.keys()})
        novo.pop('id')
        novo['user'] = None if not_user else Usuario().getUsuario()
        novo['origem'] = self
        novo['data_modificacao'] = datetime.today()
        return self._meta.model.insert(novo).execute()

    def ignorar_campos_mudancas():
        return ['origem','user','data_criacao','data_modificacao']
    
    def excluir_instancia(self):
        self.data_exclusao = datetime.today()
        self.save()

    def exportar_ignorar():
        return Historico.ignorar_campos_mudancas() + ModelBase.exportar_ignorar()

    def save(self, historiar=True, *args, **kwargs):
        if historiar:
            if self.id != None and self.origem is None and self.is_dirty():
                for drt in self._dirty:
                    if self.__data__[drt] != self._original[drt]:
                        self.__salvar_historico()
                        break
            # Salvar historico caso seja registro novo
            elif self.id is None and self.origem is None:
                rtrn = super().save(*args, **kwargs)
                self.__salvar_historico(not_user=True)
                return rtrn
            self.data_modificacao = datetime.today()
            self._original = self.__data__.copy()
        return super().save(*args, **kwargs)

    class Meta:
        abstract = True
