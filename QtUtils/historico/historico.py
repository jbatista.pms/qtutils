from PySide2 import QtWidgets, QtCore
from QtUtils import subwindowsbase
from .views import historico
from . import models, views, mudancas


class Historico(subwindowsbase.Formulario):
    classe_ui = historico.Ui_Dialog
    
    def atualizar(self):
        # Obter historicos e limita-lo
        self.historico = self.instance.historico.select().order_by(
                self.instance._meta.model.data_modificacao.desc()
            )
        if self.ui.limite.currentText().isdigit():
            self.historico = list(self.historico.limit(int(self.ui.limite.currentText())))
        else:
            self.historico = list(self.historico)
        self.historico_count = len(self.historico)
        # Popular tabela
        self.ui.tabela.setRowCount(self.historico_count)
        for h in range(self.historico_count):
            hst = self.historico[h]

            item = QtWidgets.QTableWidgetItem()
            item.setText(str(hst.id))
            self.ui.tabela.setItem(h,0, item)

            item = QtWidgets.QTableWidgetItem()
            item.setText(" "+hst.strftime('data_modificacao')+" ")
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.tabela.setItem(h,1, item)

            item = QtWidgets.QTableWidgetItem()
            item.setText(" "+hst.user.nome+" " if hst.user else '')
            self.ui.tabela.setItem(h,2, item)
        self.ui.tabela.resizeColumnsToContents()

    def inicializar(self, *args, **kwargs):
        headerH = self.ui.tabela.horizontalHeader()
        headerH.setSectionHidden(0,True)
        self.atualizar()

    def mudancas(self, row, column):
        hst = self.historico[row]
        hst_a = self.historico[row+1] if row < self.historico_count-1 else None
        mudancas.Mudancas(
            parent=self,
            instance=hst,
            historico_ant=hst_a,
            ).exec_()
