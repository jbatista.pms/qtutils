from PySide2 import QtWidgets, QtCore
from peewee import *
from QtUtils import subwindowsbase, qt
from .views import mudancas
from . import models, views


class Mudancas(subwindowsbase.Formulario):
    classe_ui = mudancas.Ui_Dialog

    def inicializar(self, *args, **kwargs):
        historico_ant = kwargs['historico_ant']
        self.ui.data.setText(
            self.instance.strftime('data_modificacao', '%d/%m/%Y %H:%M')
            )
        self.ui.usuario.setText(
            self.instance.user.nome if self.instance.user else ''
            )
        campos = [
            (k,v) for k,v in self.instance.dados().items()
            if not k in self.instance._meta.model.ignorar_campos_mudancas()
        ]
        self.controle_count = 0
        self.ui.tabela.setRowCount(len(campos))
        for key, value in campos:
            item0 = qt.QTableWidgetItem()
            item0.setText(key)
            item1 = qt.QTableWidgetItem()
            if not value is None:
                if self.instance.escolha(key) is None:
                    item1.setText(str(value))
                else:
                    item1.setText(self.instance.escolha(key))
            item2 = qt.QTableWidgetItem()
            if not historico_ant is None:
                value_ant = historico_ant.dados()[key]
                if not value_ant is None:
                    if historico_ant.escolha(key) is None:
                        item2.setText(str(value_ant))
                    else:
                        item2.setText(historico_ant.escolha(key))
                if value_ant != value:
                    font = item0.font()
                    font.setBold(True)
                    item0.setFont(font)
                    item1.setFont(font)
                    item2.setFont(font)
            self.ui.tabela.setItem(self.controle_count,0, item0)
            self.ui.tabela.setItem(self.controle_count,1, item1)
            self.ui.tabela.setItem(self.controle_count,2, item2)

            self.controle_count += 1
        self.ui.tabela.resizeColumnsToContents()
