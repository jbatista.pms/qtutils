# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'historico.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(416, 539)
        Dialog.setMinimumSize(QSize(416, 539))
        Dialog.setMaximumSize(QSize(416, 539))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.pushButton = QPushButton(Dialog)
        self.pushButton.setObjectName(u"pushButton")

        self.gridLayout.addWidget(self.pushButton, 2, 1, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer, 2, 2, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_2, 2, 0, 1, 1)

        self.tabela = QTableWidget(Dialog)
        if (self.tabela.columnCount() < 3):
            self.tabela.setColumnCount(3)
        __qtablewidgetitem = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tabela.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        self.tabela.setObjectName(u"tabela")
        self.tabela.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tabela.setAlternatingRowColors(True)
        self.tabela.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tabela.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tabela.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tabela.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tabela.horizontalHeader().setHighlightSections(False)
        self.tabela.verticalHeader().setVisible(False)
        self.tabela.verticalHeader().setHighlightSections(False)

        self.gridLayout.addWidget(self.tabela, 1, 0, 1, 3)

        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_2 = QGridLayout(self.frame)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.limite = QComboBox(self.frame)
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.addItem("")
        self.limite.setObjectName(u"limite")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.limite.sizePolicy().hasHeightForWidth())
        self.limite.setSizePolicy(sizePolicy)

        self.gridLayout_2.addWidget(self.limite, 0, 1, 1, 1)

        self.label = QLabel(self.frame)
        self.label.setObjectName(u"label")
        sizePolicy1 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy1)

        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer_3, 0, 2, 1, 1)


        self.gridLayout.addWidget(self.frame, 0, 0, 1, 3)

        QWidget.setTabOrder(self.limite, self.tabela)
        QWidget.setTabOrder(self.tabela, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.reject)
        self.tabela.cellDoubleClicked.connect(Dialog.mudancas)
        self.limite.currentIndexChanged.connect(Dialog.atualizar)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Hist\u00f3rico", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        ___qtablewidgetitem = self.tabela.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Dialog", u"id", None));
        ___qtablewidgetitem1 = self.tabela.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Dialog", u"Data", None));
        ___qtablewidgetitem2 = self.tabela.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Dialog", u"Usu\u00e1rio", None));
        self.limite.setItemText(0, QCoreApplication.translate("Dialog", u"10", None))
        self.limite.setItemText(1, QCoreApplication.translate("Dialog", u"25", None))
        self.limite.setItemText(2, QCoreApplication.translate("Dialog", u"50", None))
        self.limite.setItemText(3, QCoreApplication.translate("Dialog", u"100", None))
        self.limite.setItemText(4, QCoreApplication.translate("Dialog", u"250", None))
        self.limite.setItemText(5, QCoreApplication.translate("Dialog", u"500", None))
        self.limite.setItemText(6, QCoreApplication.translate("Dialog", u"Todos", None))

        self.label.setText(QCoreApplication.translate("Dialog", u"Limite:", None))
    # retranslateUi

