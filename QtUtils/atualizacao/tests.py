import os, unittest
from datetime import datetime

from QtUtils import Definicoes, QtUtils
from .controles import ControleVersao, DadosInstalacao, DadosVersoesLocal

QtUtils.configurar({
    'abreviatura_nome': 'QtUtils',
    'classe_janela_principal': 'QtUtils.app.JanelaPrincipal',
    'inicializar_cef': True,
    'nome_aplicacao': 'QtUtils',
    'modo_execucao': Definicoes.MODO_LOCAL,
    'verificar_atualizacao': True,
})

data_posterior = datetime(2021,4,15,16,32, tzinfo=QtUtils.definicoes.local_tz)
data_meio = datetime(2021,4,5,16,32, tzinfo=QtUtils.definicoes.local_tz)

diretorio_testes = os.path.join(os.getcwd(), 'QtUtils', 'atualizacao', 'testes')
arquivo_instalacao = os.path.join(diretorio_testes, 'instalacao.ver')
arquivo_instalacao_desatualizado = os.path.join(diretorio_testes, 'instalacao_desatualizado.ver')
arquivo_instalacao_incorreta = os.path.join(diretorio_testes, 'instalacao_incorreto.ver')
diretorio_versoes = os.path.join(diretorio_testes, 'versoes')
diretorio_versoes_arquivo_mal  = os.path.join(diretorio_testes, 'versoes_arquivo_mal')
diretorio_versoes_inexistente = os.path.join(diretorio_testes, 'versoes_inexistente')
diretorio_versoes_sem_arquivo = os.path.join(diretorio_testes, 'versoes_sem_arquivo')
diretorio_versoes_sem_registros = os.path.join(diretorio_testes, 'versoes_sem_registros')

DadosInstalacao(arquivo_instalacao).gravar_arquivo(versao='21.04.03')
DadosInstalacao(arquivo_instalacao_desatualizado).gravar_arquivo(versao='21.04.03')
DadosInstalacao(arquivo_instalacao_incorreta).gravar_arquivo()


class ControleVersaoTestCase(unittest.TestCase):
    def test_perfeito(self):
        self.controle = ControleVersao(
            data_hora_atual=data_meio,
            arquivo_instalacao=arquivo_instalacao,
            diretorio_versoes=diretorio_versoes,
        )
        self.assertIsNotNone(self.controle.ultima_versao)
        self.assertFalse(self.controle.instalacao_desatualizada)

    def test_instalacao(self):
        self.controle = ControleVersao(
            data_hora_atual=data_posterior,
            arquivo_instalacao=arquivo_instalacao_desatualizado,
            diretorio_versoes=diretorio_versoes,
        )
        self.assertIsNotNone(self.controle.ultima_versao)
        self.assertTrue(self.controle.instalacao_desatualizada)
        self.assertIsNone(self.controle.instalar_nova_versao())


class DadosInstalacaoTestCase(unittest.TestCase):
    def test_arquivo_instalacao(self):
        dados = DadosInstalacao(arquivo_instalacao)
        self.assertEqual(dados.versao(), '21.04.03')

    def test_arquivo_instalacao_incorreto(self):
        dados = DadosInstalacao(arquivo_instalacao_incorreta)
        self.assertEqual(dados.versao(), '')


class DadosVersoesLocalTestCase(unittest.TestCase):
    def test_arquivo_mal_formatado(self):
        diretorio = DadosVersoesLocal(
            data_hora_atual=data_posterior,
            diretorio=diretorio_versoes_arquivo_mal,
        )
        self.assertTrue(diretorio.existe_arquivo())
        self.assertIsInstance(diretorio.registros, dict)
        self.assertFalse(diretorio.registros)
        self.assertIsNone(diretorio.obter_dados_versao('21.04.03'))
        self.assertEqual(
            diretorio.verificar_erros(),
            "Erro na formatação do arquivo de controle de versões: must be str, not None",
        )

    def test_arquivo_sem_registros(self):
        diretorio = DadosVersoesLocal(
            data_hora_atual=data_posterior,
            diretorio=diretorio_versoes_sem_registros,
        )
        self.assertTrue(diretorio.existe_arquivo())
        self.assertIsInstance(diretorio.registros, dict)
        self.assertFalse(diretorio.registros)
        self.assertIsNone(diretorio.obter_dados_versao('21.04.03'))
        self.assertEqual(
            diretorio.verificar_erros(),
            "Arquivo de controle de versões não possui registros",
        )

    def test_diretorio_inexistente(self):
        diretorio = DadosVersoesLocal(
            data_hora_atual=data_posterior,
            diretorio=diretorio_versoes_inexistente,
        )
        self.assertFalse(diretorio.existe_arquivo())
        self.assertIsInstance(diretorio.registros, dict)
        self.assertFalse(diretorio.registros)
        self.assertIsNone(diretorio.obter_dados_versao('21.04.03'))
        self.assertEqual(
            diretorio.verificar_erros(),
            "Diretório de controle de versões inexistente",
        )

    def test_diretorio_sem_arquivo(self):
        diretorio = DadosVersoesLocal(
            data_hora_atual=data_posterior,
            diretorio=diretorio_versoes_sem_arquivo,
        )
        self.assertFalse(diretorio.existe_arquivo())
        self.assertIsInstance(diretorio.registros, dict)
        self.assertFalse(diretorio.registros)
        self.assertIsNone(diretorio.obter_dados_versao('21.04.03'))
        self.assertEqual(
            diretorio.verificar_erros(),
            "Arquivo de controle de versões não encontrado",
        )

    def test_perfeito(self):
        diretorio = DadosVersoesLocal(
            data_hora_atual=data_posterior,
            diretorio=diretorio_versoes,
        )
        self.assertIsNone(diretorio.verificar_erros())
        self.assertIsInstance(diretorio.registros, dict)
        self.assertTrue(diretorio.registros)
        self.assertIsNone(diretorio.obter_dados_versao('versao_inexistente'))
        self.assertIsInstance(diretorio.obter_dados_versao('21.04.03'), dict)