import os

import requests
from loguru import logger
from PySide2 import QtCore

from QtUtils import QtUtils, subwindowsbase
from QtUtils.servidor import ControladorServidor
from .estruturas import atualizar_instalacao, atualizar_instalacao_progresso


class AtualizarInstalacaoDialogo(subwindowsbase.Formulario):
    classe_ui = atualizar_instalacao.Ui_Dialog

    def __init__(self, nv, va=None, *args, **kwargs):
        super().__init__(nova_versao=nv, versao_atual=va, *args, **kwargs)

    def inicializar(self, nova_versao, versao_atual):
        self.ui.nova_versao.setText(nova_versao)
        self.ui.versao_atual.setText(versao_atual or 'Não identificada.')


class AtualizarInstalacaoProgressoDialogo(subwindowsbase.Formulario):
    classe_ui = atualizar_instalacao_progresso.Ui_Dialog

    def __init__(self, loc_inst: str, loc_dest: str, loc_assinatura: str, *args, **kwargs):
        self.__localizacao_instalador = loc_inst
        self.__localizacao_destino = loc_dest
        self.__localizacao_assinatura = loc_assinatura
        super().__init__(*args, **kwargs)
    
    def conclusao(self, c: bool):
        self.accept()

    def inicializar(self, *args, **kwargs):
        self.th = ObterInstaladorThread(
            loc_assinatura=self.__localizacao_assinatura,
            loc_inst=self.__localizacao_instalador,
            loc_dest=self.__localizacao_destino,
        )
        self.th.atualizar_progresso.connect(self.progresso)
        self.th.conclusao.connect(self.conclusao)
        self.th.start()
    
    def progresso(self, p: int):
        self.ui.progresso.setValue(p)


class ObterInstaladorThread(QtCore.QThread):
    atualizar_progresso = QtCore.Signal(int)
    conclusao = QtCore.Signal(bool)

    def __init__(self, loc_assinatura: str, loc_inst: str, loc_dest: str) -> None:
        self.__localizacao_assinatura = loc_assinatura
        self.__localizacao_instalador = loc_inst
        self.__localizacao_destino = loc_dest
        QtCore.QThread.__init__(self)
    
    def __obter_local(self):
        copiado = 0
        tamanho = os.stat(self.__localizacao_instalador).st_size
        origem = open(self.__localizacao_instalador, 'rb')
        destino = open(self.__localizacao_destino, 'wb')
        while True:
            dados = origem.read(32768)
            if not dados:
                break
            destino.write(dados)
            copiado += len(dados)
            self.atualizar_progresso.emit(copiado * 100 / tamanho)
        origem.close()
        destino.close()
    
    def __obter_rede(self):
        url = ControladorServidor().endereco(*self.__localizacao_instalador.split('/'))
        with requests.get(url=url, stream=True) as resp:
            resp.raise_for_status()
            tamanho = int(resp.raw.headers.get("Content-Length"))
            logger.debug(f"O arquivo possui {tamanho} bytes.")
            with open(self.__localizacao_destino, 'wb') as fl:
                copiado = 0
                for dado in resp.iter_content(chunk_size=8192):
                    fl.write(dado)
                    copiado += 8192
                    self.atualizar_progresso.emit(copiado * 100 / tamanho)
            with open(self.__localizacao_assinatura, 'w') as f:
                f.write(resp.headers['Assinatura-Arquivo'])

    def run(self):
        logger.info("Obtendo nova versão.")
        if QtUtils.definicoes.modo_local:
            self.__obter_local()
        else:
            self.__obter_rede()
        self.conclusao.emit(True)