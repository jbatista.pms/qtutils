import csv, json, os
from collections import OrderedDict
from datetime import datetime
from typing import Optional
from loguru import logger

from QtUtils import QtUtils
from QtUtils.servidor import ControladorServidor


class ControleVersao(object):
    __instalacao = None
    __versoes = None
    __ultima_versao = None
    __versao_atual = None

    def __init__(self, data_hora_atual, arquivo_instalacao=None, diretorio_versoes=None):
        self.__instalacao = DadosInstalacao(arquivo_instalacao=arquivo_instalacao)
        if QtUtils.definicoes.modo_local:
            self.__versoes = DadosVersoesLocal(
                data_hora_atual=data_hora_atual,
                diretorio=diretorio_versoes,
            )
        else:
            self.__versoes = DadosVersoesRede()
        self.__ultima_versao = self.__versoes.ultima_versao()
        self.__versao_atual = self.__versoes.obter_dados_versao(
            versao=self.__instalacao.versao(),
        )
    
    @property
    def versoes(self):
        return self.__versoes
    
    @property
    def instalacao(self):
        return self.__instalacao
    
    @property
    def instalacao_desatualizada(self):
        if self.__ultima_versao is None:
            logger.warning("Sem informação de última versão.")
            return False
        elif self.__versao_atual is None:
            logger.warning("Sem informação de versão instalada.")
            return True
        return self.__versao_atual['data'] < self.__ultima_versao['data']
    
    @property
    def ultima_versao(self):
        if self.__ultima_versao:
            return self.__ultima_versao.copy()
    
    @property
    def versao_atual(self):
        if self.__versao_atual:
            return self.__versao_atual.copy()


class DadosInstalacao(object):
    __arquivo_instalacao = None
    __dados_instalacao = None

    def __init__(self, arquivo_instalacao=None):
        self.__arquivo_instalacao = arquivo_instalacao or self.__obter_arquivo_instalacao()
    
    def __obter_arquivo_instalacao(self):
        return os.path.join(os.getcwd(), 'instalacao.ver')
    
    def __obter_dados_instalacao(self):
        logger.debug(f"Obtendo dados de instalação de '{self.__arquivo_instalacao}'.")
        if os.path.isfile(self.__arquivo_instalacao):
            try:
                with open(self.__arquivo_instalacao, 'r') as arq:
                    self.__dados_instalacao = json.loads(bytes.fromhex(arq.read()).decode())
                logger.debug(
                    "Versão do software instalada: "
                    f"{self.__dados_instalacao['versao']}."
                )
            except Exception as erro:
                logger.error(f"Erro ao obter dados da instalação: {erro}")
                self.__dados_instalacao = {}
        else:
            logger.debug("Dados da instalação não encontrados.")
            self.__dados_instalacao = {}
    
    def dados_instalacao(self) -> dict:
        if self.__dados_instalacao is None:
            self.__obter_dados_instalacao()
        return self.__dados_instalacao.copy()
    
    def gravar_arquivo(self, **dados) -> None:
        dados_modelo = {
            'versao': ''
        }
        dados_modelo.update(dados)
        with open(self.__arquivo_instalacao, 'w') as arq:
            arq.write(json.dumps(dados_modelo).encode().hex())
    
    def versao(self) -> str:
        return self.dados_instalacao().get('versao', '')


class DadosVersoesLocal(object):
    __arquivo = None
    __diretorio = None
    __formato_data = '%Y-%m-%dT%H:%M:%S.%f%z'
    __data_hora_atual = None
    __registros_dict = None
    __erro_obter_registros = None

    def __init__(self, data_hora_atual, diretorio):
        self.__diretorio = diretorio or QtUtils.config.diretorio_versionamento
        self.__arquivo = os.path.join(self.__diretorio, 'versoes.csv')
        self.__data_hora_atual = data_hora_atual
    
    def __extrair_registros_arquivo(self, arquivo_csv):
        registros = {}
        next(arquivo_csv)
        for ver in arquivo_csv:
            implantacao = datetime.strptime(ver['implantacao'], self.__formato_data)
            if implantacao < self.__data_hora_atual:
                registros.update({ver['versao']: {
                    'versao': ver['versao'],
                    'data': datetime.strptime(ver['data'], self.__formato_data),
                    'executavel': ver['executavel'],
                    'loc_executavel': os.path.join(self.__diretorio, ver['executavel']),
                    'implantacao': implantacao,
                }})
        return registros

    def __obter_registros(self):
        try:
            with open(self.__arquivo, 'r') as arquivo:
                colunas = ['versao', 'data', 'executavel', 'implantacao']
                arq_csv = csv.DictReader(arquivo, fieldnames=colunas, delimiter=';')
                return self.__extrair_registros_arquivo(arquivo_csv=arq_csv)
        except Exception as erro:
            self.__erro_obter_registros = erro
            return {}
    
    def arquivos_diretorio(self):
        return os.listdir(self.__diretorio)
    
    def configurado(self):
        return bool(self.__diretorio)
    
    def existe_arquivo(self):
        return os.path.isfile(self.__arquivo)
    
    def existe_diretorio(self):
        return os.path.isdir(self.__diretorio)
    
    def obter_dados_versao(self, versao):
        if versao in self.registros():
            return self.registros()[versao]
    
    def registros(self):
        if self.__registros_dict is None:
            self.__registros_dict = self.__obter_registros()
        return self.__registros_dict.copy()
    
    def ultima_versao(self):
        if self.registros():
            registros = sorted(
                self.registros().values(),
                key=lambda versao: versao['data'],
                reverse=True
            )
            return registros[0]
    
    def verificar_erros(self, dir=True, arq=True, fmt=True, reg=True, *args, **kwargs):
        if dir and not self.existe_diretorio():
            return "Diretório de controle de versões inexistente"
        if arq and not self.existe_arquivo():
            return "Arquivo de controle de versões não encontrado"
        if fmt:
            if self.__registros_dict is None:
                self.__registros_dict = self.__obter_registros()
            if self.__erro_obter_registros:
                return (
                    "Erro na formatação do arquivo de controle de versões: "
                    "{}".format(str(self.__erro_obter_registros))
                )
            if reg and len(self.registros().values()) == 0:
                return "Arquivo de controle de versões não possui registros"


class DadosVersoesRede(object):
    __registros_dict = None
    __servidor_dados = None
    __ultima_versao = None
    
    def __init__(self) -> None:
        self.__servidor_dados = ControladorServidor()
        self.__registros_dict = OrderedDict()
        for ver in self.__servidor_dados.versoes():
            self.__registros_dict[ver['nome']] = {
                'data': datetime.fromisoformat(ver['data']),
                'executavel': 'instalador.exe',
                'loc_executavel': ver['url'],
                'versao': ver['nome'],
            }
        logger.debug(f"{len(self.__registros_dict)} versões encontradas.")
        if self.__registros_dict:
            self.__ultima_versao = list(self.__registros_dict.values())[0]
            logger.debug(f"Última versão encontrada: {self.__ultima_versao}.")
    
    def configurado(self) -> bool:
        return True
    
    def obter_dados_versao(self, versao):
        if versao in self.__registros_dict:
            return self.__registros_dict[versao]
        logger.debug(f"Não há dados para a versão {versao} no servidor.")
    
    def registros(self) -> dict:
        return self.__registros_dict.copy()
    
    def ultima_versao(self) -> Optional[dict]:
        return self.__ultima_versao
    
    def verificar_erros(self, web=True, *args, **kwargs) -> Optional[str]:
        if web and len(self.registros()) < 1:
            logger.debug("Não há registros de versões no servidor.")
            return "Não há registros de versões no servidor."