import ctypes
import hashlib
import os
import tempfile

from loguru import logger
from typing_extensions import Final

from QtUtils import QtUtils
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.crypto import ControladorCrypto

from .controles import ControleVersao
from .janelas import AtualizarInstalacaoDialogo, AtualizarInstalacaoProgressoDialogo


class MantenedorAtualizacao(object):
    __ctrl_versao = None
    __erros_dados_versoes = None

    def __init__(self, data_hora_base=None):
        self.__ctrl_versao = ControleVersao(data_hora_atual=data_hora_base)
        if self.__ctrl_versao.versoes.configurado():
            self.__erros_dados_versoes = self.__ctrl_versao.versoes.verificar_erros(reg=False)
    
    def atualizar(self):
        if self.__ctrl_versao.ultima_versao:
            if AtualizarInstalacaoDialogo(nv=self.ultima_versao, va=self.versao_atual).exec_():
                mant_inst = MantenedorInstalacao(versao=self.__ctrl_versao.ultima_versao)
                if not mant_inst.instalar():
                    alerta = Alerta()
                    alerta.setText(mant_inst.erro)
                    alerta.exec_()
            QtUtils.encerrar()
        else:
            logger.info('Não há instalador para proceder com a instalação')
        
    
    def erros_dados_versao(self):
        return self.__erros_dados_versoes
    
    def necessaria_atualizacao(self):
        """
        Verifica se é necessário uma nova instalação, se há problemas na verificação
        e notifica o usuário.
        """
        if self.__ctrl_versao.ultima_versao is None:
            return False
        elif self.__ctrl_versao.instalacao_desatualizada:
            return True
        elif self.__erros_dados_versoes:
            # Verifica se há erros nos dados de versões
            alerta = Alerta()
            alerta.setText(self.__erros_dados_versoes)
            alerta.exec_()
            QtUtils.encerrar()
        return False
    
    @property
    def ultima_versao(self):
        if isinstance(self.__ctrl_versao.ultima_versao, dict):
            return self.__ctrl_versao.ultima_versao['versao']
    
    @property
    def versao_atual(self):
        if isinstance(self.__ctrl_versao.versao_atual, dict):
            return self.__ctrl_versao.versao_atual['versao']


class MantenedorInstalacao(object):
    __erro = None

    def __init__(self, versao: dict) -> None:
        self.__versao: Final = versao
    
    def __obter_instalador(self) -> None:
        localizacao_destino = os.path.join(tempfile.mkdtemp(), self.__versao['executavel'])
        localizacao_assinatura = os.path.join(tempfile.mkdtemp(), 'assinatura.txt')
        AtualizarInstalacaoProgressoDialogo(
            loc_inst=self.__versao['loc_executavel'],
            loc_dest=localizacao_destino,
            loc_assinatura=localizacao_assinatura,
        ).exec_()
        return localizacao_destino, localizacao_assinatura
    
    def __processar_erro(self, erro: str) -> None:
        logger.info(erro)
        self.__erro = erro
    
    def __verificar_assinatura(self, exe, assinatura):
        with open(exe, 'rb') as f:
            h = hashlib.blake2s(digest_size=16)
            h.update(f.read())
            data = h.hexdigest()
        
        with open(assinatura, 'r') as f:
            signature = f.read()
        
        ctrl_crypt = ControladorCrypto()
        if not ctrl_crypt.server_key.verify(data=data, signature=signature):
            alerta = Alerta()
            alerta.setText("Erro verificar instalador!")
            alerta.setInformativeText(
                "A assinatura criptográfica do servidor é inválida.\n"
                "Tente novamente!\n"
                "Se o erro persistir procure ajuda."
            )
            alerta.exec_()
            QtUtils.encerrar()
        
        logger.debug("Assinatura validada!")

    @property
    def erro(self) -> str:
        return self.__erro
    
    def instalar(self) -> bool:
        if not self.validar_versao():
            return False
        exe, assinatura = self.__obter_instalador()
        if QtUtils.definicoes.modo_rede:
            self.__verificar_assinatura(exe=exe, assinatura=assinatura)
        erro = ctypes.windll.shell32.ShellExecuteW(None, "runas", exe, "/SILENT", None, 1)
        if erro < 32:
            if erro == 5:
                self.__processar_erro(
                    'Não foi concedida permissão para execução do instalador.\n'
                    'Erro ShellExecute nº {}.'.format(erro)
                )
            else:
                self.__processar_erro(
                    'Erro ShellExecute nº {} na execução do instalador.'.format(erro)
                )
            return False
        return True
    
    def validar_versao(self) -> bool:
        if not isinstance(self.__versao, dict):
            self.__processar_erro('Não há registro de versão.')
        if QtUtils.definicoes.modo_local:
            if not 'executavel' in self.__versao:
                self.__processar_erro('Não há registro do instalador.')
            elif not os.path.isfile(self.__versao['loc_executavel']):
                self.__processar_erro('Instalador não foi encontrado.')
            else:
                return True
        else:
            return True
        return False