# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'atualizar_instalacao_progresso.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(295, 58)
        Dialog.setMinimumSize(QSize(295, 58))
        Dialog.setMaximumSize(QSize(295, 58))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")

        self.verticalLayout.addWidget(self.label)

        self.progresso = QProgressBar(Dialog)
        self.progresso.setObjectName(u"progresso")
        self.progresso.setValue(0)

        self.verticalLayout.addWidget(self.progresso)


        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Atualiza\u00e7\u00e3o", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Obtendo atualiza\u00e7\u00e3o...", None))
    # retranslateUi

