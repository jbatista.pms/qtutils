# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'atualizar_instalacao.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(405, 116)
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        self.gridLayout = QGridLayout(self.widget_2)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_2 = QLabel(self.widget_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.label_3 = QLabel(self.widget_2)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.versao_atual = QLabel(self.widget_2)
        self.versao_atual.setObjectName(u"versao_atual")

        self.gridLayout.addWidget(self.versao_atual, 2, 1, 1, 1)

        self.nova_versao = QLabel(self.widget_2)
        self.nova_versao.setObjectName(u"nova_versao")

        self.gridLayout.addWidget(self.nova_versao, 1, 1, 1, 1)


        self.verticalLayout.addWidget(self.widget_2)

        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        self.horizontalLayout = QHBoxLayout(self.widget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(107, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton = QPushButton(self.widget)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.pushButton_2 = QPushButton(self.widget)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.horizontalSpacer_2 = QSpacerItem(106, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addWidget(self.widget)


        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.accept)
        self.pushButton_2.released.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Nova vers\u00e3o dispon\u00edvel", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nova vers\u00e3o dispon\u00edvel. Atualiza\u00e7\u00e3o necess\u00e1ria!", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Vers\u00e3o atual:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Nova vers\u00e3o:", None))
        self.versao_atual.setText(QCoreApplication.translate("Dialog", u"21.04.03", None))
        self.nova_versao.setText(QCoreApplication.translate("Dialog", u"21.04.17", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Atualizar", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
    # retranslateUi

