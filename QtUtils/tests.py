import unittest
from datetime import date

from .prazos import Prazo


class PrazoTestcase(unittest.TestCase):
    def test_bissexto(self):
        self.assertFalse(Prazo.e_bissexto(1900))
        self.assertFalse(Prazo.e_bissexto(1950))
        self.assertFalse(Prazo.e_bissexto(5000))
        self.assertTrue(Prazo.e_bissexto(2000))
        self.assertTrue(Prazo.e_bissexto(2016))
        self.assertTrue(Prazo.e_bissexto(1904))
    
    def test_prorrogacao(self):
        prazo = Prazo(date(2016,2,29))
        self.assertEqual(prazo.prorrogar(12), date(2017,2,28))
        self.assertEqual(prazo.prorrogar(24), date(2018,2,28))
        self.assertEqual(prazo.prorrogar(36), date(2019,2,28))
        self.assertEqual(prazo.prorrogar(48), date(2020,2,28))
        prazo = Prazo(date(2015,3,1))
        self.assertEqual(prazo.prorrogar(12), date(2016,2,29))
        self.assertEqual(prazo.prorrogar(24), date(2017,2,28))
        self.assertEqual(prazo.prorrogar(36), date(2018,2,28))
        self.assertEqual(prazo.prorrogar(48), date(2019,2,28))
        self.assertEqual(prazo.prorrogar(60), date(2020,2,29))