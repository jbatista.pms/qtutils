from PySide2 import QtWidgets

from QtUtils import QtUtils
from QtUtils.backup import backup
from QtUtils.mensageiro import Mensageiro
from QtUtils.registro.listar import ListarRegistro
from QtUtils.relatorio import Relatorio
from QtUtils.user import autenticacao, listar as user_listar, cadastro, usuario
from QtUtils.user.models import User

from .views import app


class JanelaPrincipal(QtWidgets.QMainWindow, Mensageiro):
    def __init__(self, parent=None):
        super(JanelaPrincipal, self).__init__(parent)
        self.ui = app.Ui_MainWindow()
        self.ui.setupUi(self)
        self.ui.mdiArea.setActivationOrder(
            QtWidgets.QMdiArea.ActivationHistoryOrder
        )
        self.mdiArea = self.ui.mdiArea
        self.carregar_titulo()
        self.contruir()
    
    def alterar_nome(self):
        cadastro.AlterarNome(self, usuario.Usuario().getUsuario()).exec_()

    def alterar_senha(self):
        cadastro.AlterarSenha(self, usuario.Usuario().getUsuario()).exec_()

    def backup_db(self):
        backup.BackupDialogo(self).exec_()

    def cadastrar_cargo(self):
        pass
    
    def carregar_titulo(self):
        if not usuario.Usuario().getUsuario():
            self.ui.menuUsuarios.setEnabled(False)
            self.setWindowTitle(QtUtils.definicoes.nome_aplicacao)
        else:
            self.setWindowTitle(
                "%s -*-*-*- Usuário logado: %s" % (
                    QtUtils.definicoes.nome_aplicacao, usuario.Usuario().getUsuario().nome_acesso
                )
            )

    def configuracao(self):
        QtUtils.config.janela(parent=self).exec_()

    def exportar_dados(self):
        pass

    def importar_dados(self):
        pass

    def listar_cargos(self):
        pass

    def listar_lotacoes(self):
        pass

    def listar_servidores(self):
        pass

    def listar_avaliacoes(self):
        pass

    def nova_lotacao(self):
        pass

    def novo_servidor(self):
        pass

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, User):
            if usuario.Usuario().getUsuario() is None:
                autenticacao.Autenticacao(self).exec_()
            elif instance == usuario.Usuario().getUsuario():
                usuario.Usuario().setUsuario(instance)
                self.carregar_titulo()

    def registros(self):
        ListarRegistro(self).show()
    
    def teste_imprimir_relatorio(self):
        relatorio = Relatorio(
            parent=self,
            objetos=[{'n_documento': 1}, {'n_documento': 2}],
            template='QtUtils/app/documentos/teste_imprimir_relatorio.odt',
        )
        if relatorio.gerar():
            relatorio.ver_documento_subjanela()

    def usuarios(self):
        user_listar.Listar(self).show()
