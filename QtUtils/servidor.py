import platform
import uuid
from typing import Dict, Optional

import requests
from loguru import logger
from QtUtils import Application, QtUtils
from QtUtils.colecoes.dialogos import Alerta
from QtUtils.controladores import ControladorBase
from QtUtils.crypto import Certificate, ControladorCrypto


class ControladorServidor(ControladorBase):
    __dados: dict = None # Dados de requisições
    __estado = None # Estado da aplicação no servidor de dados
    __postgres = None # Dados para acesso ao banco de dados
    __requisicao_codigo = None # Código resposta do servidor
    __versoes = None # Dados de versoes obtidos do servidor
    ctrl_crypt: ControladorCrypto = None
    response = requests.Response

    def __alerta(self, msg: str) -> None:
        logger.info(msg.replace('\n', ' '))
        if Application.instance():
            alerta = Alerta(parent=QtUtils.instancia_janela)
            alerta.setWindowTitle("Servidor de dados")
            alerta.setText(msg)
            alerta.exec_()
    
    def api_ativa(self, notificar: bool=True) -> bool:
        if self.requisicao(''):
            if self.__dados.get('api', False):
                return True
            else:
                self.__alerta("Servidor retornou indisponibilidade.")
        if notificar:
            if self.codigo() is None:
                msg = (
                    f"Não foi possível estabelecer uma conexão com o servidor "
                    f"'{QtUtils.config.rede.servidor}:{QtUtils.config.rede.porta}'."
                )
            elif self.codigo() != 200:
                msg = f"O servidor retornou o erro http {self.codigo()}"
            self.__alerta(msg)
        return False
    
    def codigo(self) -> Optional[int]:
        return self.__requisicao_codigo
    
    def configurar(self) -> None:
        self.ctrl_crypt = ControladorCrypto()
    
        if QtUtils.definicoes.modo_local:
            return
        
        # Verificar se api está ativa
        if not self.api_ativa():
            if QtUtils.config.janela().exec_():
                self.configurar()
            else:
                QtUtils.encerrar()
        elif self.__estado == 'indisponivel':
            msg = "O servidor retornou indisponibilidade para o sistema."
            logger.info(msg)
            self.__alerta(msg + "\nO servidor será encerrado.")
            QtUtils.encerrar()
        
        # Verificar cliente e obter dados de acesso
        if not self.obter_dados_acesso():
            if QtUtils.config.janela().exec_():
                self.configurar()
            else:
                QtUtils.encerrar()
    
    def endereco(self, *recursos):
        return "http://{url}:{porta}/{recursos}/".format(
            porta=QtUtils.config.rede.porta,
            recursos='/'.join(recursos),
            url=QtUtils.config.rede.servidor,
        )
    
    def enviar_documentos(self, documentos: dict) -> tuple:
        try:
            dados = {
                'documentos': documentos,
                'cli_id': self.ctrl_crypt.public_key.id()
            }
            dados = self.ctrl_crypt.server_key.encrypt_dict(dados)
            response = requests.post(
                url=self.endereco('relatorio'),
                json=dados
            )
        except Exception as erro:
            erro_requisicao = f"{type(erro)} - {str(erro)}"
            logger.exception(f"Erro na requisição:")
            return False, erro_requisicao
        else:
            if response.status_code == 200:
                return True, self.ctrl_crypt.private_key.decrypt_dict(response.json())['id_']
            elif response.status_code in [404, 503]:
                return False, response.json()['mensagem']
            else:
                erro_requisicao = (
                    "O servidor retornou o erro http "
                    f"{self.__requisicao_codigo}."
                )
                logger.error(erro_requisicao)
                return False, erro_requisicao
    
    def postgres(self) -> Dict:
        if isinstance(self.__postgres, dict):
            return self.__postgres.copy()
        self.__alerta(msg="Servidor de dados não configurado.")
        QtUtils.encerrar()
    
    def obter_dados_acesso(self) -> None:
        id_cli = self.ctrl_crypt.public_key.id()
        dados = self.ctrl_crypt.server_key.encrypt_dict({
            'id_': id_cli,
            'app': QtUtils.definicoes.abreviatura_nome.lower(),
            'nome': platform.node(),
            'pkey': self.ctrl_crypt.public_key.to_string(),
        })
        if self.requisicao('cliente', dados=dados):
            if self.codigo() in [401, 403]:
                msg = (
                    f"O Servidor de dados retornou a seguinte mensagem: "
                    f"{self.__dados['mensagem']}"
                )
                logger.warning(msg)
                self.__alerta(msg=msg)
                QtUtils.encerrar()
            elif self.codigo() == 200:
                dados = self.ctrl_crypt.private_key.decrypt_dict(self.__dados)
                self.__estado = dados.get('estado')
                self.__postgres = dados.get('postgres')
                self.__postgres.update({
                    'sslmode': 'require',
                    'sslrootcert': self.ctrl_crypt.loc_certificate,
                })
                if not self.__postgres.get('host', None):
                    self.__postgres.update(
                        {'host': QtUtils.config.rede.servidor}
                    )
                self.__versoes = dados.get('versoes')
                cert = Certificate.from_string(s=dados['certificado'])
                cert.save(path=self.ctrl_crypt.loc_certificate)
                return True
            else:
                logger.warning(
                    f"O Servidor retornou código http inesperado: {self.response.status_code}."
                )
        logger.info("Não foi possível obter dados de acesso. Encerrando aplicação.")
        self.__alerta(
            "Não foi possível obter dados de acesso do servidor.\n"
            "Consulte os logs para mais informações.\n"
            "O programa será encerrado."
        )
        QtUtils.encerrar()
    
    def obter_documentos(self, id_: str) -> tuple:
        try:
            response = requests.get(url=self.endereco('relatorio', id_))
        except Exception as erro:
            erro_requisicao = str(erro)
            logger.error(f"Erro na requisição: {erro_requisicao}")
            return False, erro_requisicao
        else:
            if response.status_code == 200:
                return True, self.ctrl_crypt.private_key.decrypt_dict(response.json())['doc']
            elif response.status_code in [404, 503]:
                return False, response.json()['mensagem']
            else:
                erro = (
                    "O servidor retornou o erro http "
                    f"{self.__requisicao_codigo}."
                )
                logger.error(erro)
                return False, erro
    
    def requisicao(self, *recursos, dados: Optional[dict]=None) -> bool:
        self.__dados = None
        self.__requisicao_codigo = None
        self.response = None
        try:
            response = requests.get(url=self.endereco(*recursos), json=dados)
        except Exception as erro:
            logger.error(f"Erro na requisição: {erro}")
        else:
            self.response = response
            self.__requisicao_codigo = response.status_code
            if response.headers['Content-Type'] == 'application/json':
                self.__dados = response.json()
            else:
                self.__dados = response.content
            return True
        return False
    
    def versoes(self) -> list:
        return self.__versoes