from calendar import monthrange
from datetime import datetime, date, timedelta, tzinfo
from typing import Optional

from PySide2.QtCore import QDate, QTime, QDateTime

from QtUtils import QtUtils

DIAS_MESES = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def e_bissexto(ano):
   return (ano % 4 == 0 and (ano % 400 == 0 or ano % 100 != 0))

def datetime_qdate(data):
    return QDate(data.year, data.month, data.day)

def datetime_qtime(dt: datetime) -> QTime:
    return QTime(dt.hour, dt.minute, dt.second, dt.microsecond//1000)

def datetime_qdatetime(data: datetime) -> QDateTime:
    qdatetime = QDateTime()
    qdatetime.setDate(QDate(data.year, data.month, data.day))
    print(QDate(data.year, data.month, data.day), QTime(data.hour, data.minute, data.second, data.microsecond//1000))
    qdatetime.setTime(QTime(data.hour, data.minute, data.second, data.microsecond//1000))
    return qdatetime

def datestr_datetime(data):
    return datetime.strptime(data, "%d/%m/%Y")

def datestr_qdate(data):
    data = datestr_datetime(data)
    return datetime_qdate(data)

def qdateqtime_datetime(d: QDate, t: QTime, tzinfo: Optional[tzinfo]=None) -> datetime:
    dt = d.toPython()
    tm = t.toPython()
    return datetime.combine(date=dt, time=tm).astimezone(tzinfo or QtUtils.definicoes.local_tz)

def qdate_datetime(data):
    return date(data.year(), data.month(), data.day())

def datestr_date(data):
    return datestr_datetime(data).date()

def acrescentar_meses(data_inicial, acrescentar, dia_base):
    dia = data_inicial.day
    mes = data_inicial.month
    ano = data_inicial.year
    subtrair_ultimo_dia = True

    mes_futuro = mes + acrescentar
    anos_ultrapassados = (mes_futuro-1) // 12
    if mes_futuro > 12:
        mes_futuro = mes_futuro - anos_ultrapassados * 12
    ano_futuro = ano + anos_ultrapassados

    ultimo_dia_mes = monthrange(ano_futuro, mes_futuro)[1]
    if ultimo_dia_mes >= dia_base:
        dia_futuro = dia_base
    elif mes_futuro == 2 and e_bissexto(ano_futuro):
        dia_futuro = 29
        subtrair_ultimo_dia = False
    else:
        dia_futuro = ultimo_dia_mes
        subtrair_ultimo_dia = False

    data_futura = date(ano_futuro, mes_futuro, dia_futuro)
    if subtrair_ultimo_dia:
        data_futura -= timedelta(days=1)
    return data_futura
