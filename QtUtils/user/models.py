from passlib.hash import pbkdf2_sha256
from peewee import *
from QtUtils.db import ModelBase

pwd_context = pbkdf2_sha256.using(rounds=180000)


class User(ModelBase):
    data_exclusao = DateTimeField(null=True)
    nome_acesso = CharField()
    nome_completo = CharField()
    senha = CharField(column_name="senha")

    def __str__(self):
        return self.nome_acesso
    
    @property
    def nome(self):
        return self.nome_acesso
    
    def alterar_senha(self, senha):
        self.senha = pwd_context.hash(senha).encode()
    
    def validar_senha(self, senha):
        return pwd_context.verify(senha, self.senha)
    
    def select2(excluidos=False, *args, **kwargs):
        return User.select(*args, **kwargs).where(
            User.data_exclusao.is_null(not excluidos)
        ).order_by(User.nome_acesso)