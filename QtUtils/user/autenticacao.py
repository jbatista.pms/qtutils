from PySide2 import QtWidgets


from QtUtils import configuracao, QtUtils
from QtUtils.mensageiro import Mensageiro
from .models import User
from .usuario import Usuario
from .views import autenticacao, falha_autenticacao


class Autenticacao(QtWidgets.QDialog, Mensageiro):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ui = autenticacao.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.usuario.setFocus()
        self.setWindowTitle('Acesso - ' + QtUtils.definicoes.abreviatura_nome)
        self.contruir()

    def accept(self):
        usuario = self.ui.usuario.text()
        senha = self.ui.senha.text()
        usuario = User.get_or_none(nome_acesso=usuario)
        if usuario and usuario.data_exclusao is None:
            if usuario.validar_senha(senha):
                Usuario().setUsuario(usuario)
                self.enviar_sinal_atualizacao(usuario)
                return super().accept()
        FalhaAutenticacao(self).exec_()

    def configuracao(self):
        configuracao.controllers.Configuracao(parent=self).exec_()

    def exec_(self):
        if User.select2():
            return super().exec_()
        self.close()
        return True
    
    def reject(self):
        QtUtils.encerrar()


class FalhaAutenticacao(QtWidgets.QDialog):
    def __init__(self, parent):
        super().__init__(parent=parent)
        self.ui = falha_autenticacao.Ui_Dialog()
        self.ui.setupUi(self)
