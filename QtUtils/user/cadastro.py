from datetime import datetime

from QtUtils import QtUtils, subwindowsbase
from QtUtils.colecoes.dialogos import Alerta, Confirmacao

from .models import User
from .usuario import Usuario
from .utils import validador_nome_acesso
from .views import cadastro, alterar_senha, alterar_nome


class Cadastro(subwindowsbase.Formulario):
    classe_ui = cadastro.Ui_Dialog

    def accept(self):
        nome_acesso = self.ui.nome_acesso.text()
        nome_completo = self.ui.nome_completo.text()
        senha = self.ui.senha.text()
        if senha != self.ui.confirmar_senha.text():
            Alerta(self, text="As senhas não conferem!").exec_()
        elif nome_acesso and nome_completo and senha:
            usuario = User.get_or_none(nome_acesso=nome_acesso)
            if usuario:
                Alerta(self, text="O nome de acesso ja está em uso.").exec_()
            else:
                usuario = User(nome_acesso=nome_acesso, nome_completo=nome_completo)
                usuario.alterar_senha(senha)
                usuario.save()
                self.enviar_sinal_atualizacao(usuario)
                return super().accept()
        else:
            Alerta(self, text="Todos os campos devem ser preenchidos.").exec_()

    def alterar_senha(self):
        AlterarSenha(self, self.instance).exec_()

    def alterar_nome(self):
        AlterarNome(self, self.instance).exec_()

    def atualizar(self):
        self.setWindowTitle(self.instance.nome_completo)
        self.ui.nome_acesso.setText(self.instance.nome_acesso)
        self.ui.nome_completo.setText(self.instance.nome_completo)
    
    def excluir(self):
        if Confirmacao(self, text="Excluir permanentemente o usuário?").exec_():
            self.instance.data_exclusao = datetime.today()
            self.instance.save()
            if Usuario().getUsuario() == self.instance:
                QtUtils.encerrar()
            self.enviar_sinal_atualizacao(self.instance)
            self.close()

    def inicializar(self, *args, **kwargs):
        if self.instance.id:
            self.atualizar()
            self.ui.nome_acesso.setEnabled(False)
            self.ui.nome_completo.setEnabled(False)
            self.ui.senha.setEnabled(False)
            self.ui.confirmar_senha.setEnabled(False)
            self.ui.salvar.hide()
            if Usuario().getUsuario() is None or self.instance.id != Usuario().getUsuario().id:
                self.ocultar_alteracoes()
            if self.instance.data_exclusao != None:
                self.ui.excluir.setEnabled(False)
        else:
            self.ui.nome_acesso.setValidator(validador_nome_acesso)
            self.ocultar_alteracoes()
            self.ui.excluir.hide()

    def instanciar(self, *args, **kwargs):
        return kwargs.get('instance', User())

    def ocultar_alteracoes(self):
        self.ui.alterar_senha.hide()
        self.ui.alterar_nome.hide()
    
    def receber_sinal_atualizacao(self, instance):
        if instance == self.instance:
            self.instance = instance
            self.atualizar()


class AlterarBase(subwindowsbase.Formulario):
    def instanciar(self, *args, **kwargs):
        return Usuario().getUsuario()


class AlterarNome(AlterarBase):
    classe_ui = alterar_nome.Ui_Dialog

    def accept(self):
        senha = self.ui.senha.text()
        if self.instance.validar_senha(senha):
            self.instance.nome_acesso = self.ui.nome_acesso.text()
            self.instance.nome_completo = self.ui.nome_completo.text()
            self.instance.save()
            self.enviar_sinal_atualizacao(self.instance)
            super().accept()
        else:
            Alerta(self, text="Senha incorreta.").exec_()
            self.ui.senha.setFocus()

    def inicializar(self, *args, **kwargs):
        self.ui.nome_acesso.setFocus()
        self.ui.nome_acesso.setValidator(validador_nome_acesso)
        self.ui.nome_acesso.setText(self.instance.nome_acesso)
        self.ui.nome_completo.setText(self.instance.nome_completo)


class AlterarSenha(AlterarBase):
    classe_ui = alterar_senha.Ui_Dialog

    def accept(self):
        senha = self.ui.senha.text()
        if self.instance.validar_senha(senha):
            nova_senha = self.ui.nova_senha.text()
            confirmar = self.ui.confirmar.text()
            if nova_senha == confirmar:
                self.instance.senha = nova_senha
                self.instance.save()
                self.enviar_sinal_atualizacao(self.instance)
                super().accept()
            else:
                Alerta(self, text="Senhas não conferem.").exec_()
                self.ui.nova_senha.setFocus()
        else:
            Alerta(self, text="Senha atual incorreta.").exec_()
            self.ui.senha.setFocus()

    def inicializar(self, *args, **kwargs):
        self.ui.senha.setFocus()