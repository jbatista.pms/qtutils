class Usuario(object):
    __instance = None

    def __new__(cls):
        if Usuario.__instance is None:
            Usuario.__instance = object.__new__(cls)
            Usuario.__instance.usuario = None
        return Usuario.__instance

    def setUsuario(self, usuario):
        self.usuario = usuario

    def getUsuario(self):
        return self.usuario
