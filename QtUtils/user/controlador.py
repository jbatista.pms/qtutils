from QtUtils import QtUtils
from QtUtils.controladores import ControladorBase
from .autenticacao import Autenticacao


class ControladorAutenticacao(ControladorBase):
    def executar(self) -> None:
        Autenticacao(QtUtils.instancia_janela).exec_()