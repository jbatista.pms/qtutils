from PySide2 import QtCore, QtWidgets

from .cadastro import Cadastro
from .models import User
from .views.listar import Ui_Form
from QtUtils import subwindowsbase, qt


class Listar(subwindowsbase.Listar):
    classe_ui = Ui_Form

    def atualizar(self):
        excluidos = self.ui.excluidos.isChecked()
        self.ui.tabela.horizontalHeader().setSectionHidden(3,not excluidos)
        usuarios = User.select2(excluidos=excluidos)
        pesquisa = self.ui.pesquisa.text()
        if pesquisa:
            usuarios = usuarios.where(
                (User.nome_acesso.contains(pesquisa)) |
                (User.nome_completo.contains(pesquisa))
            )
        

        qtd = usuarios.count()
        self.ui.tabela.setRowCount(qtd)
        for f in range(qtd):
            self.ui.tabela.setItem(f,0, qt.QTableWidgetItem())
            self.ui.tabela.setItem(f,1, qt.QTableWidgetItem())
            self.ui.tabela.setItem(f,2, qt.QTableWidgetItem())
            self.ui.tabela.item(f,0).setText(str(usuarios[f].id))
            self.ui.tabela.item(f,1).setText(usuarios[f].nome_acesso)
            self.ui.tabela.item(f,2).setText(usuarios[f].nome_completo)
            item = qt.QTableWidgetItem()
            item.setTextAlignment(QtCore.Qt.AlignHCenter|QtCore.Qt.AlignVCenter)
            item.setText(usuarios[f].strftime('data_exclusao', '%d/%m/%Y %H:%M:%S'))
            self.ui.tabela.setItem(f,3, item)
        self.ui.tabela.resizeColumnsToContents()
        self.usuarios = {str(u.id): u for u in usuarios}

    def editar(self, row, column):
        id_user = self.ui.tabela.item(row,0).text()
        Cadastro(self, instance=self.usuarios[id_user]).exec_()

    def inicializar(self, *args, **kwargs):
        self.ui.pesquisa.setFocus()
        self.atualizar()
        self.ui.tabela.horizontalHeader().setSectionHidden(0,True)

    def novo(self):
        Cadastro(self).exec_()

    def receber_sinal_atualizacao(self, instance):
        if isinstance(instance, User):
            self.atualizar()
