# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'cadastro.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(455, 193)
        Dialog.setMinimumSize(QSize(455, 193))
        Dialog.setMaximumSize(QSize(547, 193))
        self.gridLayout_2 = QGridLayout(Dialog)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label, 2, 0, 1, 1)

        self.confirmar_senha = QLineEdit(Dialog)
        self.confirmar_senha.setObjectName(u"confirmar_senha")
        self.confirmar_senha.setEchoMode(QLineEdit.Password)

        self.gridLayout_3.addWidget(self.confirmar_senha, 4, 1, 1, 1)

        self.nome_completo = QLineEdit(Dialog)
        self.nome_completo.setObjectName(u"nome_completo")

        self.gridLayout_3.addWidget(self.nome_completo, 2, 1, 1, 1)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_3.addWidget(self.label_2, 3, 0, 1, 1)

        self.senha = QLineEdit(Dialog)
        self.senha.setObjectName(u"senha")
        self.senha.setEchoMode(QLineEdit.Password)

        self.gridLayout_3.addWidget(self.senha, 3, 1, 1, 1)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_3.addWidget(self.label_3, 4, 0, 1, 1)

        self.label_4 = QLabel(Dialog)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_3.addWidget(self.label_4, 0, 0, 1, 1)

        self.nome_acesso = QLineEdit(Dialog)
        self.nome_acesso.setObjectName(u"nome_acesso")

        self.gridLayout_3.addWidget(self.nome_acesso, 0, 1, 1, 1)

        self.label_5 = QLabel(Dialog)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout_3.addWidget(self.label_5, 1, 1, 1, 1)


        self.gridLayout_2.addLayout(self.gridLayout_3, 0, 0, 1, 2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.alterar_nome = QPushButton(Dialog)
        self.alterar_nome.setObjectName(u"alterar_nome")

        self.horizontalLayout.addWidget(self.alterar_nome)

        self.alterar_senha = QPushButton(Dialog)
        self.alterar_senha.setObjectName(u"alterar_senha")

        self.horizontalLayout.addWidget(self.alterar_senha)

        self.pushButton_2 = QPushButton(Dialog)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.excluir = QPushButton(Dialog)
        self.excluir.setObjectName(u"excluir")

        self.horizontalLayout.addWidget(self.excluir)

        self.salvar = QPushButton(Dialog)
        self.salvar.setObjectName(u"salvar")

        self.horizontalLayout.addWidget(self.salvar)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.gridLayout_2.addLayout(self.horizontalLayout, 1, 0, 1, 2)

        QWidget.setTabOrder(self.nome_acesso, self.nome_completo)
        QWidget.setTabOrder(self.nome_completo, self.senha)
        QWidget.setTabOrder(self.senha, self.confirmar_senha)
        QWidget.setTabOrder(self.confirmar_senha, self.alterar_nome)
        QWidget.setTabOrder(self.alterar_nome, self.alterar_senha)
        QWidget.setTabOrder(self.alterar_senha, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.excluir)
        QWidget.setTabOrder(self.excluir, self.salvar)

        self.retranslateUi(Dialog)
        self.salvar.released.connect(Dialog.accept)
        self.pushButton_2.released.connect(Dialog.reject)
        self.alterar_senha.released.connect(Dialog.alterar_senha)
        self.alterar_nome.released.connect(Dialog.alterar_nome)
        self.excluir.released.connect(Dialog.excluir)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cadastro de Usu\u00e1rio", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nome completo:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Senha:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Confirmar senha:", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Nome de acesso:", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"* Apenas caracteres alfanum\u00e9ricos min\u00fasculos ASCII.", None))
        self.alterar_nome.setText(QCoreApplication.translate("Dialog", u"  Alterar nome  ", None))
        self.alterar_senha.setText(QCoreApplication.translate("Dialog", u"  Alterar senha  ", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

