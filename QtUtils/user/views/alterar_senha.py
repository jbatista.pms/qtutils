# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'alterar_senha.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(428, 156)
        Dialog.setMinimumSize(QSize(428, 156))
        Dialog.setMaximumSize(QSize(428, 156))
        self.gridLayout = QGridLayout(Dialog)
        self.gridLayout.setObjectName(u"gridLayout")
        self.frame = QFrame(Dialog)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.frame)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)

        self.pushButton_2 = QPushButton(self.frame)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.pushButton = QPushButton(self.frame)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)


        self.gridLayout.addWidget(self.frame, 4, 0, 1, 2)

        self.senha = QLineEdit(Dialog)
        self.senha.setObjectName(u"senha")
        self.senha.setEchoMode(QLineEdit.Password)

        self.gridLayout.addWidget(self.senha, 0, 1, 1, 1)

        self.nova_senha = QLineEdit(Dialog)
        self.nova_senha.setObjectName(u"nova_senha")
        self.nova_senha.setEchoMode(QLineEdit.Password)

        self.gridLayout.addWidget(self.nova_senha, 1, 1, 1, 1)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 1, 0, 1, 1)

        self.confirmar = QLineEdit(Dialog)
        self.confirmar.setObjectName(u"confirmar")
        self.confirmar.setEchoMode(QLineEdit.Password)

        self.gridLayout.addWidget(self.confirmar, 2, 1, 1, 1)

        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 3, 0, 1, 2)


        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.accept)
        self.pushButton_2.released.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Alterar senha de usu\u00e1rio", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Nova senha:", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Senha atual:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Confirmar senha:", None))
    # retranslateUi

