# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'alterar_nome.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(444, 164)
        Dialog.setMinimumSize(QSize(444, 164))
        Dialog.setMaximumSize(QSize(444, 164))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label = QLabel(Dialog)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label, 2, 0, 1, 1)

        self.nome_acesso = QLineEdit(Dialog)
        self.nome_acesso.setObjectName(u"nome_acesso")

        self.gridLayout_2.addWidget(self.nome_acesso, 0, 1, 1, 1)

        self.nome_completo = QLineEdit(Dialog)
        self.nome_completo.setObjectName(u"nome_completo")

        self.gridLayout_2.addWidget(self.nome_completo, 2, 1, 1, 1)

        self.senha = QLineEdit(Dialog)
        self.senha.setObjectName(u"senha")
        self.senha.setEchoMode(QLineEdit.Password)

        self.gridLayout_2.addWidget(self.senha, 3, 1, 1, 1)

        self.label_2 = QLabel(Dialog)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_2, 3, 0, 1, 1)

        self.label_3 = QLabel(Dialog)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout_2.addWidget(self.label_3, 0, 0, 1, 1)

        self.label_4 = QLabel(Dialog)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_2.addWidget(self.label_4, 1, 1, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout_2)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)

        self.pushButton_2 = QPushButton(Dialog)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout_2.addWidget(self.pushButton_2)

        self.pushButton = QPushButton(Dialog)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout_2.addWidget(self.pushButton)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        QWidget.setTabOrder(self.nome_acesso, self.nome_completo)
        QWidget.setTabOrder(self.nome_completo, self.senha)
        QWidget.setTabOrder(self.senha, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.accept)
        self.pushButton_2.released.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Alterar nome de usu\u00e1rio", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nome completo:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Senha:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Nome de acesso", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"* Apenas caracteres alfanum\u00e9ricos min\u00fasculos ASCII.", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Cancelar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

