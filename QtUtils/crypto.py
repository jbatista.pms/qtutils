import base64
import binascii
import datetime
import hashlib
import json
from pathlib import WindowsPath
import platform

from cryptography import fernet
from cryptography import x509
from cryptography.exceptions import InvalidSignature
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.hazmat.primitives.asymmetric import padding, rsa
from cryptography.x509.oid import NameOID

from QtUtils import QtUtils, controladores


class Certificate:
    def __init__(self, crypt_cert: x509.Certificate) -> None:
        self.crypt_cert = crypt_cert
    
    @classmethod
    def create(cls, 
               path: WindowsPath, 
               pri_key: 'RSAPrivateKey', 
               pub_key: 'RSAPublicKey') -> 'Certificate':
        
        subject = issuer = x509.Name([
            x509.NameAttribute(NameOID.COMMON_NAME, u"QtUtilsServer"),
        ])
        cert = x509.CertificateBuilder().subject_name(
            subject
        ).issuer_name(
            issuer
        ).public_key(
            pub_key.pkey
        ).serial_number(
            x509.random_serial_number()
        ).not_valid_before(
            datetime.datetime.utcnow()
        ).not_valid_after(
            datetime.datetime.utcnow() + datetime.timedelta(days=365*10)
        ).add_extension(
            x509.SubjectAlternativeName([x509.DNSName(platform.node())]),
            critical=False,
        ).sign(private_key=pri_key.pkey, algorithm=hashes.SHA256())

        with path.open('wb') as f:
            f.write(cert.public_bytes(serialization.Encoding.PEM))
        return cls(crypt_cert=cert)
    
    @classmethod
    def from_string(cls, s: str) -> 'Certificate':
        return cls(crypt_cert=x509.load_pem_x509_certificate(
            data=base64.urlsafe_b64decode(s)
        ))
    
    @classmethod
    def load(cls, path: WindowsPath) -> 'Certificate':
        with path.open('rb') as arq:
            return cls(crypt_cert=x509.load_pem_x509_certificate(data=arq.read()))
    
    def save(self, path: WindowsPath) -> None:
        with path.open('wb') as f:
            f.write(self.crypt_cert.public_bytes(serialization.Encoding.PEM))
    
    def to_string(self) -> str:
        pem = self.crypt_cert.public_bytes(
            encoding=serialization.Encoding.PEM,
        )
        return base64.urlsafe_b64encode(s=pem).decode()


class ControladorCrypto(controladores.ControladorBase):
    certificate: Certificate = None
    private_key: 'RSAPrivateKey' = None
    public_key: 'RSAPublicKey' = None
    server_key: 'RSAPublicKey' = None
    
    def configurar(self) -> None:
        self.dir_crypto = WindowsPath(QtUtils.definicoes.diretorio_base, 'crypto')
        self.loc_certificate = self.dir_crypto.joinpath('server.pem')
        self.loc_rsa_pri = self.dir_crypto.joinpath('pkey')
        self.loc_rsa_pub = self.dir_crypto.joinpath('pkey.pub')
        self.loc_rsa_server = QtUtils.definicoes.diretorio_de_trabalho.joinpath('server.key')

        self.dir_crypto.mkdir(exist_ok=True)
        if not self.loc_rsa_pri.exists():
            self.private_key = RSAPrivateKey.create(path=self.loc_rsa_pri)
            self.public_key = RSAPublicKey.create(
                private_key=self.private_key,
                path=self.loc_rsa_pub,
            )
        
        self.private_key = RSAPrivateKey.load(path=self.loc_rsa_pri)
        self.public_key = RSAPublicKey.load(path=self.loc_rsa_pub)
        
        if QtUtils.definicoes.modo_rede:
            self.server_key = RSAPublicKey.load(path=self.loc_rsa_server)
        elif not self.loc_certificate.exists():
            self.certificate = Certificate.create(
                path=self.loc_certificate,
                pri_key=self.private_key,
                pub_key=self.public_key,
            )
    
    def executar(self) -> None:
        self.certificate = Certificate.load(path=self.loc_certificate)


class RSAPrivateKey:
    def __init__(self, pkey: rsa.RSAPrivateKey) -> None:
        self.pkey = pkey
    
    @classmethod
    def create(cls, path: WindowsPath) -> 'RSAPrivateKey':
        pkey = rsa.generate_private_key(
            public_exponent=65537,
            key_size=2048,
        )
        pem = pkey.private_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PrivateFormat.PKCS8,
            encryption_algorithm=serialization.NoEncryption(),
        )
        with path.open('wb') as f:
            f.write(pem)
        return cls(pkey=pkey)

    def decrypt(self, ciphertext: str) -> str:
        try:
            return self.pkey.decrypt(
                ciphertext=base64.urlsafe_b64decode(s=ciphertext),
                padding=padding.OAEP(
                    mgf=padding.MGF1(algorithm=hashes.SHA256()),
                    algorithm=hashes.SHA256(),
                    label=None
                )
            ).decode()
        except binascii.Error:
            return None
    
    def decrypt_dict(self, d: dict) -> dict:
        token = self.decrypt(ciphertext=d.get('token'))
        f = fernet.Fernet(key=token.encode())
        return json.loads(s=f.decrypt(token=d['data']))
    
    @classmethod
    def load(cls, path: WindowsPath) -> 'RSAPrivateKey':
        with path.open('rb') as f:
            pkey = serialization.load_pem_private_key(
                data=f.read(),
                password=None,
            )
        return cls(pkey=pkey)

    def sign(self, data: str) -> str:
        signature = self.pkey.sign(
            data=data.encode(),
            padding=padding.PSS(
                mgf=padding.MGF1(hashes.SHA256()),
                salt_length=padding.PSS.MAX_LENGTH
            ),
            algorithm=hashes.SHA256(),
        )
        return base64.b64encode(s=signature).decode()


class RSAPublicKey:
    def __init__(self, pkey: rsa.RSAPublicKey) -> None:
        self.pkey = pkey

    def __pem(self) -> bytes:
        return self.pkey.public_bytes(
            encoding=serialization.Encoding.PEM,
            format=serialization.PublicFormat.SubjectPublicKeyInfo,
        )
    
    @classmethod
    def create(cls, private_key: RSAPrivateKey, path: WindowsPath) -> 'RSAPublicKey':
        public_key = RSAPublicKey(pkey=private_key.pkey.public_key())
        with path.open('wb') as f:
            f.write(public_key.__pem())
        return public_key
    
    def encrypt(self, data: str) -> str:
        ciphertext = self.pkey.encrypt(
            data.encode(),
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        return base64.urlsafe_b64encode(s=ciphertext).decode()
    
    def encrypt_dict(self, d: dict) -> dict:
        token = fernet.Fernet.generate_key()
        f = fernet.Fernet(key=token)
        return {
            'data': f.encrypt(data=json.dumps(d).encode()).decode(),
            'token': self.encrypt(data=token.decode()),
        }
    
    @classmethod
    def from_string(cls, s: str) -> 'RSAPublicKey':
        pem = base64.urlsafe_b64decode(s=s)
        return cls(pkey=serialization.load_pem_public_key(data=pem))

    def id(self) -> str:
        hexdigest = hashlib.blake2b(self.__pem(), digest_size=9).hexdigest()
        return '-'.join([hexdigest[i:i+3] for i in range(0, 9, 3)]).upper()
    
    @classmethod
    def load(cls, path: WindowsPath) -> 'RSAPublicKey':
        with path.open('rb') as f:
            return cls(pkey=serialization.load_pem_public_key(data=f.read()))
    
    def to_string(self) -> str:
        return base64.standard_b64encode(s=self.__pem()).decode()
    
    def verify(self, data: str, signature: str) -> bool:
        try:
            self.pkey.verify(
                signature=base64.b64decode(s=signature),
                data=data.encode(),
                padding=padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                algorithm=hashes.SHA256()
            )
            return True
        except InvalidSignature:
            return False