from .singleton import Singleton


class ControladorBase(metaclass=Singleton):
    def configurar(self) -> None:
        pass

    def encerrar(self) -> None:
        pass
    
    def executar(self) -> None:
        pass