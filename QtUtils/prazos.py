from datetime import date, timedelta

meses_ref = {1: 31, 3: 31, 4:30, 5:31, 6:30, 7:31, 8:31, 9:30, 10:31, 11:30, 12:31}


class Prazo(object):
    __in = None

    def __init__(self, inicio):
        self.__in = inicio
    
    def __dia_prorrogacao(self, dia, dias_mes):
        if dia > dias_mes:
            return dias_mes
        else:
            return self.__in.day
    
    @staticmethod
    def e_bissexto(ano):
        # 1) É um número divisível por 4.
        if ano % 4 != 0:
            return False
        # 2) É um número divisível por 4, mas não é divisível por 100.
        cem = ano % 100 != 0
        if cem:
            return True
        # 3) É um número divisível por 4, por 100 e por 400.
        return ano % 400 == 0 and not cem

    def prorrogar(self, meses):
        # Definir ano e mês da prorrogação
        ano_prorr = self.__in.year + meses // 12
        mes_prorr = self.__in.month + meses % 12
        # Definir dia da prorrogação
        if mes_prorr in meses_ref:
            dia_prorr = self.__dia_prorrogacao(self.__in.day, meses_ref[mes_prorr])
        elif mes_prorr == 2:
            if Prazo.e_bissexto(ano_prorr):
                dia_prorr = self.__dia_prorrogacao(self.__in.day, 29)
            else:
                dia_prorr = self.__dia_prorrogacao(self.__in.day, 28)
        else:
            dia_prorr = self.__in.day
        # Definindo data de prorrogação
        data_prorr = date(ano_prorr, mes_prorr, dia_prorr)
        if data_prorr.day == self.__in.day:
            return data_prorr - timedelta(days=1)
        return data_prorr