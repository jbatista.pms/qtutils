from importlib import import_module
from pydoc import locate
from typing import Any

from loguru import logger


class ImportarClasse(object):
    __classe = None
    __modulo = None
    __modulos = set()

    def __init__(self, classe_texto: str) -> None:
        self.__classe = locate(classe_texto)
        self.__modulo = '.'.join(classe_texto.split('.')[:-1])
        self.__modulos.add(self.__modulo)
    
    def classe(self) -> Any:
        return self.__classe
    
    def modulo(self) -> Any:
        return import_module(self.__modulo)
    
    @classmethod
    def modulos_registrados(cls) -> list:
        return list(cls.__modulos)