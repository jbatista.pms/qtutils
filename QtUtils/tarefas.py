import os
from pydoc import locate

from QtUtils.db import DataBase


class TarefasBase(object):
    __arquivos = []
    __diretorio = None
    __instance = None

    def __init__(self):
        self.__diretorio = os.path.join(os.getcwd(), 'tarefas')
        if os.path.isdir(self.__diretorio):
            self.__arquivos = os.listdir(self.__diretorio)

    def __new__(cls):
        if TarefasBase.__instance is None:
            TarefasBase.__instance = object.__new__(cls)
        return TarefasBase.__instance
    
    def executar(self, tarefa):
        if tarefa + '.py' in self.__arquivos:
            modulo = locate('tarefas.'+tarefa)
            with DataBase().obter_database().atomic():
                modulo.executar()
        else:
            raise Exception("Tarefa '%s' não encontrada." % tarefa)
    
    def executar_lote(self, *tarefas):
        for t in tarefas:
            self.executar(tarefa=t)


Tarefas = TarefasBase()