import os, time

from loguru import logger
from PySide2 import QtCore, QtGui, QtWidgets

from QtUtils import Application, QtUtils
from QtUtils.controladores import ControladorBase


class ControladorBandejaSistema(ControladorBase):
    __th_bandeja = None # Instância de thread para monitoramento da bandeja

    def __init__(self) -> None:
        self.bandeja = QtWidgets.QSystemTrayIcon(parent=Application.instance())
    
    def mostrar_janela(self, reacao):
        if reacao == QtWidgets.QSystemTrayIcon.DoubleClick:
            self.mostrar_janela_forcado()
    
    def mostrar_janela_forcado(self):
        logger.debug("Mostrar janela a partir da bandeja do sistema forçadamente.")
        QtUtils.instancia_janela.show(forcar=True)
    
    def mostrar_menu(self) -> bool:
        if QtWidgets.QSystemTrayIcon.isSystemTrayAvailable():
            logger.debug("Bandeja do sistema disponível. Incializando menu.")
            self.bandeja.show()
        else:
            logger.debug("Bandeja do sistema indisponível.")
    
    def encerrar(self):
        self.bandeja.hide()
        if self.__th_bandeja:
            self.__th_bandeja.terminate()
    
    def encerrar_aplicacao(self):
        logger.debug("Encerrando aplicação através da bandeja do sistema.")
        Application.instance().quit()
    
    def executar(self) -> None:
        icone = QtGui.QIcon(
            os.path.join(os.getcwd(), "QtUtilsServer", "bandeja", "icones", "bandeja.png")
        )
        acao_mostrar = QtWidgets.QAction(parent=self.bandeja, text='Mostrar')
        acao_mostrar.triggered.connect(self.mostrar_janela_forcado)
        acao_encerrar = QtWidgets.QAction(parent=self.bandeja, text='Encerrar')
        acao_encerrar.triggered.connect(self.encerrar_aplicacao)
        menu = QtWidgets.QMenu(parent=QtUtils.instancia_janela, title='QtUtilsServer')
        menu.addAction(acao_mostrar)
        menu.addSeparator()
        menu.addAction(acao_encerrar)
        self.bandeja.setContextMenu(menu)
        self.bandeja.setIcon(icone)
        self.bandeja.activated.connect(self.mostrar_janela)
        self.__th_bandeja = MostrarBandejaThread(bandeja=self.bandeja)
        self.__th_bandeja.mostrar.connect(self.mostrar_menu)
        self.__th_bandeja.start()
        

class MostrarBandejaThread(QtCore.QThread):
    """ 
    Forçar mostrar o menu na bandeja caso não esteja visível.
    Para os casos em que o sistema esteja indisponível.
    """
    mostrar = QtCore.Signal()

    def __init__(self, bandeja):
        self.__bandeja = bandeja
        QtCore.QThread.__init__(self)
    
    def run(self):
        while not self.__bandeja.isVisible():
            self.mostrar.emit()
            time.sleep(1)