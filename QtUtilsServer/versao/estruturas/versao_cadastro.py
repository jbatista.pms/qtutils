# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'versao_cadastro.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(261, 149)
        Dialog.setMinimumSize(QSize(261, 149))
        Dialog.setMaximumSize(QSize(261, 149))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        self.gridLayout = QGridLayout(self.widget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.label_2 = QLabel(self.widget)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.cmp_hora = QTimeEdit(self.widget)
        self.cmp_hora.setObjectName(u"cmp_hora")
        self.cmp_hora.setCalendarPopup(True)

        self.gridLayout.addWidget(self.cmp_hora, 3, 1, 1, 1)

        self.cmp_data = QDateEdit(self.widget)
        self.cmp_data.setObjectName(u"cmp_data")
        self.cmp_data.setCalendarPopup(True)

        self.gridLayout.addWidget(self.cmp_data, 3, 0, 1, 1)

        self.label_3 = QLabel(self.widget)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 2, 1, 1, 1)

        self.cmp_nome = QLineEdit(self.widget)
        self.cmp_nome.setObjectName(u"cmp_nome")

        self.gridLayout.addWidget(self.cmp_nome, 1, 0, 1, 2)

        self.label = QLabel(self.widget)
        self.label.setObjectName(u"label")

        self.gridLayout.addWidget(self.label, 0, 0, 1, 2)


        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy)
        self.horizontalLayout = QHBoxLayout(self.widget_2)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, -1, 0, 0)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(self.widget_2)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.btn_excluir = QPushButton(self.widget_2)
        self.btn_excluir.setObjectName(u"btn_excluir")

        self.horizontalLayout.addWidget(self.btn_excluir)

        self.btn_salvar = QPushButton(self.widget_2)
        self.btn_salvar.setObjectName(u"btn_salvar")

        self.horizontalLayout.addWidget(self.btn_salvar)


        self.verticalLayout.addWidget(self.widget_2)

        QWidget.setTabOrder(self.cmp_nome, self.cmp_data)
        QWidget.setTabOrder(self.cmp_data, self.cmp_hora)
        QWidget.setTabOrder(self.cmp_hora, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.btn_excluir)
        QWidget.setTabOrder(self.btn_excluir, self.btn_salvar)

        self.retranslateUi(Dialog)
        self.pushButton_2.released.connect(Dialog.reject)
        self.btn_excluir.released.connect(Dialog.excluir)
        self.btn_salvar.released.connect(Dialog.salvar)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cadastro de vers\u00e3o", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Data de implanta\u00e7\u00e3o:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Hora de implanta\u00e7\u00e3o:", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.btn_excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.btn_salvar.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

