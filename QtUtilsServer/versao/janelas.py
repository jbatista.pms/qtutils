import os

from peewee import *
from PySide2 import QtWidgets

from QtUtils import QtUtils
from QtUtils.db import DataBase
from QtUtils.date import datetime_qdate, datetime_qtime, qdateqtime_datetime
from QtUtilsServer.aplicativo.modelos import Aplicativo
from QtUtilsServer.utils.janelas import DialogoInstanciaBase

from .estruturas import versao_cadastro
from .modelos import Versao


class VersaoCadastro(DialogoInstanciaBase):
    classe_janela = versao_cadastro.Ui_Dialog

    def __init__(self, aplicativo: Aplicativo, instalador=None, *args, **kwargs) -> None:
        self.aplicativo = aplicativo
        self.instalador = instalador
        super().__init__(*args, **kwargs)

    def excluir(self):
        if self.e_instancia:
            if self.instancia.excluir():
                self.accept()

    def inicializar(self):
        if self.e_instancia:
            self.ui.cmp_nome.setText(self.instancia.nome)
            self.ui.cmp_data.setDate(datetime_qdate(self.instancia.dt_implantacao))
            self.ui.cmp_hora.setTime(datetime_qtime(self.instancia.dt_implantacao))
            if self.instancia.implementada():
                self.ui.btn_excluir.setEnabled(False)
                self.ui.btn_salvar.setEnabled(False)
                self.ui.cmp_data.setEnabled(False)
                self.ui.cmp_hora.setEnabled(False)
                self.ui.cmp_nome.setEnabled(False)
        else:
            data = DataBase().data()
            self.ui.cmp_data.setDate(datetime_qdate(data))
            self.ui.cmp_hora.setTime(datetime_qtime(data))
            self.ui.btn_excluir.setVisible(False)

    @classmethod
    def nova(cls, aplicativo: Aplicativo) -> None:
        instalador = QtWidgets.QFileDialog.getOpenFileName(
            parent=QtUtils.instancia_janela,
            caption='Selecionar o instalador',
            dir=os.getenv("USERPROFILE"),
            filter=("Aplicativo (*.exe)"),
        )
        if instalador[0]:
            cls(aplicativo=aplicativo, instalador=instalador[0]).exec_()

    def salvar(self):
        formulario = {
            'aplicativo': self.aplicativo,
            'dt_implantacao': qdateqtime_datetime(
                d=self.ui.cmp_data.date(),
                t=self.ui.cmp_hora.time(),
            ),
            'instalador_criar': self.instalador,
            'nome': self.ui.cmp_nome.text(),
        }
        if self.e_instancia:
            formulario.pop('instalador_criar')
            self.instancia.atualizar(**formulario)
            if self.instancia.salvar():
                self.accept()
        else:
            versao = Versao(**formulario)
            if versao.salvar():
                self.accept()