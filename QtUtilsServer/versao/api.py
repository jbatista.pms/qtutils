import hashlib
from pathlib import WindowsPath
from flask import Blueprint, send_file
from flask_restful import Api, Resource

from QtUtils.crypto import ControladorCrypto, RSAPrivateKey

from .modelos import Versao

bp = Blueprint('versao', __name__, url_prefix='/versao/')
api = Api(bp)


class VersaoAPI(Resource):
    def __init__(self) -> None:
        super().__init__()
        self.ctrl_crypto: ControladorCrypto = ControladorCrypto()
        
    def get(self, id_):
        versao: Versao = Versao.get_or_none(id_=id_)
        if versao is None:
            return {'mensagem': 'Instalador não encontrado.'}, 503
        
        with open(versao.arquivo_instalacao, 'rb') as f:
            h = hashlib.blake2s(digest_size=16)
            h.update(f.read())
        
        assinatura = self.ctrl_crypto.private_key.sign(data=h.hexdigest())
        
        response = send_file(
            path_or_file=versao.arquivo_instalacao,
            as_attachment=False,
            attachment_filename=versao.instalador_nome(),
        )
        response.headers['Assinatura-Arquivo'] = assinatura
        return response


api.add_resource(VersaoAPI, '/<id_>/', endpoint='baixar')