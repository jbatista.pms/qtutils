import os
import hashlib
import shutil
import uuid
from typing import Optional, Type

from flask import url_for
from peewee import *

from QtUtils.db import DataBase, DateTimeTZField
from QtUtilsServer.aplicativo.modelos import Aplicativo
from QtUtilsServer.utils.modelos import ModeloBase


class Versao(ModeloBase):
    id_ = UUIDField(default=uuid.uuid4, unique=True)
    aplicativo = ForeignKeyField(Aplicativo, related_name='versoes', on_delete='CASCADE')
    dt_criacao =  DateTimeTZField(default=DataBase().data)
    dt_implantacao =  DateTimeTZField()
    nome = CharField()

    def __str__(self):
        return self.nome

    @property
    def arquivo_instalacao(self):
        return os.path.join(self.diretorio, 'instalador.exe')
    
    @classmethod
    def dados_api(cls, aplicativo: Aplicativo) -> list:
        versoes = cls.filter(
            cls.aplicativo==aplicativo,
            cls.dt_implantacao<=DataBase().data(),
        )
        return [{
            'id_': str(v.id_),
            'data': v.dt_criacao.isoformat(),
            'dt_implantacao': v.dt_implantacao.isoformat(),
            'nome': v.nome,
            'url': url_for('versao.baixar', id_=v.id_),
        } for v in versoes.order_by(cls.dt_implantacao.desc())]
        
    @property
    def diretorio(self):
        diretorio = os.path.join(self.aplicativo.diretorio_versoes, self.nome)
        # Criando diretório caso não exista
        if not os.path.isdir(diretorio):
            os.mkdir(diretorio)
        return diretorio
    
    def excluir(self, msg: Optional[str]=None):
        obj = super().excluir()
        if obj:
            shutil.rmtree(self.diretorio)
            return obj
    
    def implementada(self) -> bool:
        return self.dt_implantacao <= DataBase().data()
    
    def instalador_nome(self) -> str:
        return f"{self.aplicativo} {self.nome}.exe"
    
    def salvar(self):
        obj = super().salvar()
        if obj:
            if hasattr(self, 'instalador_criar'):
                shutil.copy(src=self.instalador_criar, dst=self.arquivo_instalacao)
            return obj
    
    @classmethod
    def selecionar_ordem(cls, *args, **kwargs):
        return cls.selecione(*args, **kwargs).order_by(cls.dt_implantacao.desc())
    
    @classmethod
    def ultima_versao(cls, aplicativo: Aplicativo) -> Type[Aplicativo]:
        return cls.selecionar_ordem(
            cls.aplicativo==aplicativo,
            cls.dt_implantacao<DataBase().data()
        ).first()

    def validar(self) -> bool:
        erros = {}
        if self.id:
            versao = Versao.get(id=self.id)
            if versao.implementada():
                erros.update({'Implementação': 'Versão já implementada!'})
            else:
                # Impedindo alteração do instalador edição dos campos aplicativo e nome
                if versao.aplicativo != self.aplicativo:
                    erros.update({'Aplicativo': 'Uma vez salvo não pode ser alterado.'})
                if versao.nome != self.nome:
                    erros.update({'Nome': 'Uma vez salvo não pode ser alterado.'})
                if hasattr(self, 'instalador_criar'):
                    delattr(self, 'instalador_criar')
                    erros.update({'Instalador': 'Uma vez salvo não pode ser alterado.'})
        else:
            # Validando aplicativo
            if not self.aplicativo:
                erros.update({'Aplicativo': 'Não informado.'})
                outra_versao = None
            else:
                outra_versao = Versao.selecione(nome=self.nome, aplicativo=self.aplicativo).first()
            # Validando nome
            if not self.nome:
                erros.update({'Nome': 'Não informado.'})
            if outra_versao and not outra_versao.id == self.id:
                erros.update({'Nome': 'Já existente.'})
            # Validando arquivo de instalação
            if not hasattr(self, 'instalador_criar'):
                erros.update({
                    'Instalador': 'Para novas instalações, é necessário que informe o instalador.',
                })
            elif not os.path.exists(self.instalador_criar):
                erros.update({'Instalador': f'Arquivo {self.instalador_criar} não encontrado.'})
        # Validando data de implantação
        if not self.dt_implantacao:
            erros.update({'Data de implantação': 'Não informado.'})
        elif self.dt_implantacao <= DataBase().data():
            erros.update({'Data de implantação': 'Anterior a data atual do sistema.'})
        # Mostrando ou não erros
        if erros:
            Aplicativo.mostrar_erros_validacao(
                erros=erros,
                texto="Erros encontrados:",
            )
            return False
        return True