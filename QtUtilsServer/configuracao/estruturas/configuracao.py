# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'configuracao.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(444, 208)
        Dialog.setMinimumSize(QSize(444, 208))
        Dialog.setMaximumSize(QSize(444, 208))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        self.verticalLayout_2 = QVBoxLayout(self.widget)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.tabWidget = QTabWidget(self.widget)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.gridLayout = QGridLayout(self.tab_3)
        self.gridLayout.setObjectName(u"gridLayout")
        self.widget_5 = QWidget(self.tab_3)
        self.widget_5.setObjectName(u"widget_5")
        self.formLayout_3 = QFormLayout(self.widget_5)
        self.formLayout_3.setObjectName(u"formLayout_3")
        self.label_6 = QLabel(self.widget_5)
        self.label_6.setObjectName(u"label_6")

        self.formLayout_3.setWidget(2, QFormLayout.LabelRole, self.label_6)

        self.cmp_bd_usuario = QLineEdit(self.widget_5)
        self.cmp_bd_usuario.setObjectName(u"cmp_bd_usuario")

        self.formLayout_3.setWidget(2, QFormLayout.FieldRole, self.cmp_bd_usuario)

        self.label_5 = QLabel(self.widget_5)
        self.label_5.setObjectName(u"label_5")

        self.formLayout_3.setWidget(0, QFormLayout.LabelRole, self.label_5)

        self.cmp_bd_porta = QSpinBox(self.widget_5)
        self.cmp_bd_porta.setObjectName(u"cmp_bd_porta")
        self.cmp_bd_porta.setMinimum(1)
        self.cmp_bd_porta.setMaximum(65535)
        self.cmp_bd_porta.setValue(5432)

        self.formLayout_3.setWidget(0, QFormLayout.FieldRole, self.cmp_bd_porta)

        self.label_7 = QLabel(self.widget_5)
        self.label_7.setObjectName(u"label_7")

        self.formLayout_3.setWidget(1, QFormLayout.LabelRole, self.label_7)

        self.cmp_bd_senha = QLineEdit(self.widget_5)
        self.cmp_bd_senha.setObjectName(u"cmp_bd_senha")
        self.cmp_bd_senha.setEchoMode(QLineEdit.Password)

        self.formLayout_3.setWidget(1, QFormLayout.FieldRole, self.cmp_bd_senha)


        self.gridLayout.addWidget(self.widget_5, 0, 0, 1, 1)

        self.widget_6 = QWidget(self.tab_3)
        self.widget_6.setObjectName(u"widget_6")
        self.formLayout_4 = QFormLayout(self.widget_6)
        self.formLayout_4.setObjectName(u"formLayout_4")

        self.gridLayout.addWidget(self.widget_6, 0, 1, 1, 1)

        self.tabWidget.addTab(self.tab_3, "")
        self.tab_4 = QWidget()
        self.tab_4.setObjectName(u"tab_4")
        self.gridLayout_2 = QGridLayout(self.tab_4)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.cmp_na_bandeja = QCheckBox(self.tab_4)
        self.cmp_na_bandeja.setObjectName(u"cmp_na_bandeja")

        self.gridLayout_2.addWidget(self.cmp_na_bandeja, 1, 0, 1, 1)

        self.cmp_com_sistema = QCheckBox(self.tab_4)
        self.cmp_com_sistema.setObjectName(u"cmp_com_sistema")

        self.gridLayout_2.addWidget(self.cmp_com_sistema, 0, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_2.addItem(self.verticalSpacer, 2, 0, 1, 1)

        self.tabWidget.addTab(self.tab_4, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.formLayout_2 = QFormLayout(self.tab_2)
        self.formLayout_2.setObjectName(u"formLayout_2")
        self.formLayout_2.setRowWrapPolicy(QFormLayout.WrapAllRows)
        self.label_2 = QLabel(self.tab_2)
        self.label_2.setObjectName(u"label_2")

        self.formLayout_2.setWidget(0, QFormLayout.LabelRole, self.label_2)

        self.widget_4 = QWidget(self.tab_2)
        self.widget_4.setObjectName(u"widget_4")
        self.horizontalLayout_3 = QHBoxLayout(self.widget_4)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.cmp_office = QLineEdit(self.widget_4)
        self.cmp_office.setObjectName(u"cmp_office")
        self.cmp_office.setReadOnly(True)

        self.horizontalLayout_3.addWidget(self.cmp_office)

        self.pushButton_4 = QPushButton(self.widget_4)
        self.pushButton_4.setObjectName(u"pushButton_4")

        self.horizontalLayout_3.addWidget(self.pushButton_4)


        self.formLayout_2.setWidget(0, QFormLayout.FieldRole, self.widget_4)

        self.tabWidget.addTab(self.tab_2, "")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.formLayout = QFormLayout(self.tab)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setRowWrapPolicy(QFormLayout.WrapAllRows)
        self.label = QLabel(self.tab)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.widget_3 = QWidget(self.tab)
        self.widget_3.setObjectName(u"widget_3")
        self.horizontalLayout_2 = QHBoxLayout(self.widget_3)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.cmp_diretorio_backup = QLineEdit(self.widget_3)
        self.cmp_diretorio_backup.setObjectName(u"cmp_diretorio_backup")
        self.cmp_diretorio_backup.setReadOnly(True)

        self.horizontalLayout_2.addWidget(self.cmp_diretorio_backup)

        self.pushButton_3 = QPushButton(self.widget_3)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout_2.addWidget(self.pushButton_3)


        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.widget_3)

        self.label_3 = QLabel(self.tab)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.label_3)

        self.cmp_porta_api = QSpinBox(self.tab)
        self.cmp_porta_api.setObjectName(u"cmp_porta_api")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.cmp_porta_api.sizePolicy().hasHeightForWidth())
        self.cmp_porta_api.setSizePolicy(sizePolicy)
        self.cmp_porta_api.setMinimumSize(QSize(100, 0))
        self.cmp_porta_api.setMinimum(1)
        self.cmp_porta_api.setMaximum(65535)

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.cmp_porta_api)

        self.tabWidget.addTab(self.tab, "")

        self.verticalLayout_2.addWidget(self.tabWidget)


        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy1)
        self.horizontalLayout = QHBoxLayout(self.widget_2)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer = QSpacerItem(345, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(self.widget_2)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.pushButton = QPushButton(self.widget_2)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)


        self.verticalLayout.addWidget(self.widget_2)

        QWidget.setTabOrder(self.tabWidget, self.cmp_bd_porta)
        QWidget.setTabOrder(self.cmp_bd_porta, self.cmp_bd_senha)
        QWidget.setTabOrder(self.cmp_bd_senha, self.cmp_bd_usuario)
        QWidget.setTabOrder(self.cmp_bd_usuario, self.cmp_com_sistema)
        QWidget.setTabOrder(self.cmp_com_sistema, self.cmp_na_bandeja)
        QWidget.setTabOrder(self.cmp_na_bandeja, self.cmp_office)
        QWidget.setTabOrder(self.cmp_office, self.pushButton_4)
        QWidget.setTabOrder(self.pushButton_4, self.cmp_diretorio_backup)
        QWidget.setTabOrder(self.cmp_diretorio_backup, self.pushButton_3)
        QWidget.setTabOrder(self.pushButton_3, self.cmp_porta_api)
        QWidget.setTabOrder(self.cmp_porta_api, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.reject)
        self.pushButton_2.released.connect(Dialog.salvar)
        self.pushButton_4.released.connect(Dialog.selecionar_office)
        self.pushButton_3.released.connect(Dialog.selecionar_diretorio_backup)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Configura\u00e7\u00e3o", None))
        self.label_6.setText(QCoreApplication.translate("Dialog", u"Usu\u00e1rio:", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Porta:", None))
        self.label_7.setText(QCoreApplication.translate("Dialog", u"Senha:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), QCoreApplication.translate("Dialog", u"Banco de dados", None))
        self.cmp_na_bandeja.setText(QCoreApplication.translate("Dialog", u"Manter na bandeja do sistema. (inicializa\u00e7\u00e3o, fechamento)", None))
        self.cmp_com_sistema.setText(QCoreApplication.translate("Dialog", u"Inicializar junto com o sistema.", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_4), QCoreApplication.translate("Dialog", u"Comportamento", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Localiza\u00e7\u00e3o do LibreOffice:", None))
        self.pushButton_4.setText(QCoreApplication.translate("Dialog", u"Selecionar", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Dialog", u"Ferramentas", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Diret\u00f3rio padr\u00e3o de backups:", None))
        self.pushButton_3.setText(QCoreApplication.translate("Dialog", u"Selecionar", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Porta da api:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Dialog", u"Sistema", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
    # retranslateUi

