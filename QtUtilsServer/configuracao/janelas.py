import os
from PySide2 import QtWidgets
from loguru import logger
from QtUtils import colecoes, subwindowsbase, QtUtils
from QtUtils.colecoes.erros import ErrosFormularioDialogo

from ..relatorio.base import Office
from .estruturas import configuracao


class ConfiguracaoJanela(subwindowsbase.Formulario):
    classe_ui = configuracao.Ui_Dialog

    def inicializar(self, *args, **kwargs):
        self.ui.cmp_diretorio_backup.setText(QtUtils.config.diretorio_backup)
        self.ui.cmp_office.setText(QtUtils.config.office)
        self.ui.cmp_porta_api.setValue(QtUtils.config.porta_api)
        # Banco de dados
        self.ui.cmp_bd_porta.setValue(QtUtils.config.postgres.port)
        self.ui.cmp_bd_usuario.setText(QtUtils.config.postgres.user)
        self.ui.cmp_bd_senha.setText(QtUtils.config.postgres.password)
        # Comportamento
        self.ui.cmp_com_sistema.setChecked(QtUtils.config.comportamento.com_sistema)
        self.ui.cmp_na_bandeja.setChecked(QtUtils.config.comportamento.na_bandeja)

    def salvar(self):
        QtUtils.config.diretorio_backup = self.ui.cmp_diretorio_backup.text()
        QtUtils.config.office = self.ui.cmp_office.text()
        QtUtils.config.porta_api = int(self.ui.cmp_porta_api.text())
        # Banco de dados
        QtUtils.config.postgres.port = int(self.ui.cmp_bd_porta.text())
        QtUtils.config.postgres.user = self.ui.cmp_bd_usuario.text()
        QtUtils.config.postgres.password = self.ui.cmp_bd_senha.text()
        # Comportamento
        QtUtils.config.comportamento.com_sistema = self.ui.cmp_com_sistema.isChecked()
        QtUtils.config.comportamento.na_bandeja = self.ui.cmp_na_bandeja.isChecked()
        # Validação
        erros = QtUtils.config.validate()
        if erros is None:
            QtUtils.salvar_configuracoes()
            QtUtils.config.comportamento.manutencao_inicializacao()
            self.accept()
        else:
            ErrosFormularioDialogo(erros=erros).exec_()

    def selecionar_diretorio_backup(self):
        diretorio = QtWidgets.QFileDialog.getExistingDirectory(
            parent=self,
            caption='Selecionar diretório',
            dir=os.environ['HOMEPATH'],
        )
        if diretorio:
            self.ui.cmp_diretorio_backup.setText(diretorio)

    def selecionar_office(self):
        programa = QtWidgets.QFileDialog.getOpenFileName(
            parent=self,
            caption='Selecionar executável do Office',
            dir=os.environ['HOMEPATH'],
            filter=("Aplicativo (*.exe)"),
        )
        executavel = programa[0]
        if executavel:
            erro = Office.validar(localizacao=executavel)
            if erro is None:
                self.ui.cmp_office.setText(executavel)
                Office.emitir_sinal_ativo()
                lo = Office(localizacao=executavel)
                logger.info(f"Instalação do {lo.instalacao} versão {lo.versao} selecionada.")
            else:
                Office.emitir_sinal_inativo()
                msg = colecoes.dialogos.Alerta(parent=self)
                msg.setText("Erro no executável selecionado:")
                msg.setInformativeText(erro)
                msg.exec_()
                self.ui.cmp_office.setText("")