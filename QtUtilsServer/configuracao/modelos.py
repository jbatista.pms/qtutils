import sys
import os
import win32com.client

from loguru import logger

from QtUtils import QtUtils
from QtUtils.rede import porta_disponivel
from QtUtils.configuracao.base import *

from .janelas import ConfiguracaoJanela


class ConfigComportamento(Node):
    com_sistema = BooleanField(default=False)
    na_bandeja = BooleanField(default=False)
    
    def __init__(self) -> None:
        super().__init__()
        self.__link_inic = os.path.join(
            os.getenv('APPDATA'), 'Microsoft', 'Windows', 'Start Menu',
            'Programs', 'Startup', f"{QtUtils.definicoes.abreviatura_nome}.lnk",
        )

    def manutencao_inicializacao(self):
        if self.com_sistema:
            if not os.path.exists(self.__link_inic):
                logger.debug(f"Criando atalho de '{sys.argv[0]}' em '{self.__link_inic}'")
                shell = win32com.client.Dispatch("WScript.Shell")
                atalho = shell.CreateShortCut(self.__link_inic)
                atalho.Targetpath = sys.argv[0]
                atalho.WindowStyle = 1
                atalho.WorkingDirectory = os.path.dirname(os.path.abspath(sys.argv[0]))
                atalho.save()
                logger.info("Adicionado a inicialização do sistema.")
        else:
            if os.path.exists(self.__link_inic):
                os.remove(self.__link_inic)
                logger.info("Removido da inicialização do sistema.")


class ConfigLocalPostgres(Node):
    port = IntegerField(default=5432)
    user = StringField(null=True, default='')
    password = StringField(null=True, default='')


class ConfigQtUtilsServer(Node):
    comportamento = ExtendedField(ConfigComportamento)
    diretorio_backup = StringField(default=QtUtils.definicoes.diretorio_backup)
    diretorio_versionamento = StringField(default='')
    modo = StringField(default='local')
    office = StringField(default='')
    porta_api = IntegerField(default=porta_disponivel)
    postgres = ExtendedField(ConfigLocalPostgres)
    # Classe da janela para editar configurações
    janela = ConfiguracaoJanela