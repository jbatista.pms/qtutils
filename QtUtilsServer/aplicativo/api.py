from flask import Blueprint, request, send_file
from flask_restful import Api, Resource
from loguru import logger

from QtUtils.crypto import ControladorCrypto
from QtUtilsServer.versao.modelos import Versao
from .modelos import Aplicativo, Cliente

bp = Blueprint('cliente', __name__, url_prefix='/cliente/')
api = Api(bp)


class AplicativoDownloadVersaoAPI(Resource):
    def get(self, codigo):
        aplicativo = Aplicativo.get_or_none(codigo=codigo)
        if aplicativo is None:
            return {'mensagem': 'Aplicativo inexistente.'}, 404
        versao = Versao.ultima_versao(aplicativo=aplicativo)
        if versao is None:
            return {'mensagem': 'Instalador não encontrado.'}, 404
        return send_file(
            path_or_file=versao.arquivo_instalacao,
            as_attachment=False,
            attachment_filename=versao.instalador_nome(),
        )


class ClienteAPI(Resource):
    def __init__(self) -> None:
        super().__init__()
        self.ctrl_crypto: ControladorCrypto = ControladorCrypto()

    def get(self):
        if request.content_type != 'application/json':
            msg = "Conteúdo incorreto. Espera-se 'content_type' 'application/json'."
            logger.error(msg)
            return {'mensagem': msg}, 401

        dados = request.json.get('data', None)
        token = request.json.get('token', None)
        if dados is None or token is None:
            return {'mensagem': "Faltam argumentos."}, 401

        dados = self.ctrl_crypto.private_key.decrypt_dict(request.json)

        app_name = dados.get('app', None)
        app = Aplicativo.get_or_none(codigo=app_name)
        if app is None:
            msg = f"Aplicação '{app_name}' não existe."
            logger.error(msg)
            return {'mensagem': msg}, 404
        
        dados['app'] = app
        cliente, _ = Cliente.get_or_create(
            id_=dados['id_'],
            defaults=dados,
        )
        logger.info(f"Dados de acesso requisitado para o cliente '{cliente}'")

        if not cliente.autorizado:
            logger.info(f"Dados de acesso negado para o cliente '{cliente}'")
            return {'mensagem': 'Cliente inexistente ou não autorizado.'}, 403
        
        logger.info(f"Dados de acesso concedido para o cliente '{cliente}'.")
        return cliente.dados_de_acesso()


api.add_resource(ClienteAPI, '/')
api.add_resource(AplicativoDownloadVersaoAPI, '/baixar/')