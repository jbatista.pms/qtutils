from loguru import logger

from peewee import *
from PySide2 import QtCore, QtWidgets

from QtUtils.colecoes.dialogos import Confirmacao, Informacao
from QtUtils.colecoes.erros import ErrosFormularioDialogo
from QtUtilsServer.aplicativo.modelos import Cliente
from QtUtilsServer.utils.janelas import DialogoInstanciaBase
from QtUtilsServer.versao.janelas import VersaoCadastro
from QtUtilsServer.versao.modelos import Versao

from .estruturas import (
    aplicativo_cadastro,
    aplicativo_cadastro_bd,
    aplicativo_detalhe,
    aplicativo_lista,
)
from .modelos import Aplicativo


class AplicativoCadastroBD(DialogoInstanciaBase):
    classe_janela = aplicativo_cadastro_bd.Ui_Dialog

    def inicializar(self):
        self.ui.bd_servidor.setText(self.instancia.bd_servidor)
        self.ui.bd_porta.setValue(self.instancia.bd_porta or 0)
        self.ui.bd_nome.setText(self.instancia.bd_nome)
        self.ui.bd_usuario.setText(self.instancia.bd_usuario)
        self.ui.bd_senha.setText(self.instancia.bd_senha)
    
    def salvar(self):
        bd_nome = self.ui.bd_nome.text()
        bd_usuario = self.ui.bd_usuario.text()
        bd_senha = self.ui.bd_senha.text()
        bd_servidor = self.ui.bd_servidor.text()
        bd_porta = self.ui.bd_porta.value()
        # Validação
        if bd_nome and bd_usuario and bd_senha and bd_servidor and bd_porta:
            self.instancia.bd_nome = bd_nome
            self.instancia.bd_usuario = bd_usuario
            self.instancia.bd_senha = bd_senha
            self.instancia.bd_servidor = bd_servidor
            self.instancia.bd_porta = bd_porta
            self.instancia.salvar()
            super().accept()
        else:
            erros = {'Campos': 'Todos precisam ser preenchidos.'}
            ErrosFormularioDialogo(erros=erros).exec_()


class AplicativoCadastro(DialogoInstanciaBase):
    classe_janela = aplicativo_cadastro.Ui_Dialog

    def inicializar(self):
        if self.e_instancia:
            self.ui.codigo.setEnabled(False)
            self.ui.codigo.setText(self.instancia.codigo)
            self.ui.nome.setText(self.instancia.nome)
        else:
            self.ui.btn_excluir.setVisible(False)

    def excluir(self):
        if self.e_instancia:
            if self.instancia.excluir():
                self.accept()
    
    def salvar(self):
        formulario = {
            'codigo': self.ui.codigo.text(),
            'nome': self.ui.nome.text(),
        }
        if self.e_instancia:
            self.instancia.atualizar(**formulario)
            if self.instancia.salvar():
                self.accept()
        else:
            aplicatico = Aplicativo(**formulario)
            if aplicatico.salvar():
                self.hide()
                AplicativoDetalhe(instancia=aplicatico).exec_()
                self.accept()


class AplicativoDetalhe(DialogoInstanciaBase):
    classe_janela = aplicativo_detalhe.Ui_Dialog
    modelo = Aplicativo

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        Versao.sinal_excluir.conectar(recebedor=self.sinal_versao)
        Versao.sinal_salvar.conectar(recebedor=self.sinal_versao)
        Aplicativo.sinal_salvar.conectar(recebedor=self.inicializar)

    def cadastro(self):
        AplicativoCadastro(instancia=self.instancia).exec_()
    
    def cliente(self, row: int, column: int) -> None:
        self.ui.tbl_clientes.item(row,0).text()
        cliente = self.lista_clientes[self.ui.tbl_clientes.item(row,0).text()]
        dialogo = Confirmacao(parent=self)
        dialogo.setText(f"Alterar autorização de acesso do cliente '{cliente}'?")
        dialogo.setInformativeText(
            f"Clicando em 'SIM' o cliente estará "
            f"{'DESAUTORIZADO' if cliente.autorizado else 'AUTORIZADO'} "
            f"a acessar o aplicativo {self.instancia}."
        )
        if dialogo.exec_():
            cliente.autorizado = not cliente.autorizado
            cliente.salvar()
            self.cliente_listar()
    
    def cliente_listar(self) -> None:
        clientes = Cliente.todos(app=self.instancia).objects()
        self.lista_clientes = {str(c.id_):c for c in clientes}
        self.ui.tbl_clientes.setRowCount(len(self.lista_clientes))
        for num, cli in enumerate(self.lista_clientes.values()):
            # Cliente Id
            item = QtWidgets.QTableWidgetItem()
            item.setText(cli.id_)
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ui.tbl_clientes.setItem(num, 0, item)
            # Cliente Autorizado
            item = QtWidgets.QTableWidgetItem()
            item.setTextAlignment(QtCore.Qt.AlignCenter)
            item.setText("SIM" if cli.autorizado else "NÃO")
            self.ui.tbl_clientes.setItem(num, 1, item)
            # Cliente Nome
            item = QtWidgets.QTableWidgetItem()
            item.setText("  " + cli.nome)
            self.ui.tbl_clientes.setItem(num, 2, item)
        #self.ui.tbl_clientes.resizeColumnsToContents()
    
    def editar_bd(self):
        AplicativoCadastroBD(parent=self, instancia=self.instancia).exec_()
    
    def encerrando(self) -> None:
        Versao.sinal_excluir.desconectar(recebedor=self.sinal_versao)
        Versao.sinal_salvar.desconectar(recebedor=self.sinal_versao)
        return super().encerrando()

    def inicializar(self, *args, **kwargs):
        self.ui.lb_codigo.setText(self.instancia.codigo)
        self.ui.lb_nome.setText(self.instancia.nome)
        self.ui.bd_nome.setText(self.instancia.bd_nome)
        self.ui.bd_usuario.setText(self.instancia.bd_usuario)
        self.ui.bd_senha.setText(self.instancia.bd_senha)
        self.cliente_listar()
        self.versao_listar()
    
    def resetar_bd(self):
        dialogo = Confirmacao(parent=self)
        dialogo.setText('Resetar dados do banco de dados?')
        if dialogo.exec_():
            self.instancia.criar_dados_bd()
            self.instancia.salvar()
            dialogo = Informacao(parent=self)
            dialogo.setText('Dados resetados.')
            dialogo.exec_()

    
    def sinal_excluir(self, obj, sinal):
        if self.instancia is None:
            self.reject()

    def sinal_salvar(self, obj, sinal):
        if obj.id == self.instancia.id:
            self.instancia = obj
            self.inicializar()
    
    def sinal_versao(self, obj, sinal):
        self.versao_listar()
    
    def versao_cadastro(self, row, column):
        versao = self.lista_versoes[self.ui.tbl_versoes.verticalHeaderItem(row).text()]
        VersaoCadastro(aplicativo=self.instancia, instancia=versao).exec_()
    
    def versao_listar(self):
        vers = Versao.selecionar_ordem(aplicativo=self.instancia).objects()
        self.lista_versoes = {str(a.id):a for a in vers}
        self.ui.tbl_versoes.setRowCount(len(self.lista_versoes))
        for num, ver in enumerate(self.lista_versoes.values()):
            # Id
            item = QtWidgets.QTableWidgetItem()
            item.setText(str(ver.id))
            self.ui.tbl_versoes.setVerticalHeaderItem(num, item)
            # Nome
            item = QtWidgets.QTableWidgetItem()
            item.setText(f'    {ver.nome}    ')
            self.ui.tbl_versoes.setItem(num, 0, item)
            # Data de implementação
            item = QtWidgets.QTableWidgetItem()
            item.setText(f"    {ver.dt_implantacao.strftime('%d/%m/%Y %H:%M:%S')}    ")
            self.ui.tbl_versoes.setItem(num, 1, item)
        self.ui.tbl_versoes.resizeColumnsToContents()
    
    def versao_nova(self):
        logger.debug(f"Adicionar nova versão de {self.instancia.codigo}.")
        VersaoCadastro.nova(aplicativo=self.instancia)


class AplicativoLista(DialogoInstanciaBase):
    classe_janela = aplicativo_lista.Ui_Dialog
    lista_aplicativos = []
    modelo = Aplicativo

    def abrir_cadastro(self, row, column):
        id_app = self.ui.aplicativos.verticalHeaderItem(row).text()
        AplicativoDetalhe(instancia=self.lista_aplicativos[id_app]).exec_()
    
    def adicionar(self):
        AplicativoCadastro().exec_()
    
    def inicializar(self):
        self.lista_aplicativos = {str(a.id):a for a in Aplicativo.select()}
        self.ui.aplicativos.clear()
        self.ui.aplicativos.setRowCount(len(self.lista_aplicativos))
        self.ui.aplicativos.resizeColumnsToContents()
        for num, app in enumerate(self.lista_aplicativos.values()):
            # Id
            item = QtWidgets.QTableWidgetItem()
            item.setText(str(app.id))
            self.ui.aplicativos.setVerticalHeaderItem(num, item)
            # Código e nome
            item = QtWidgets.QTableWidgetItem()
            item.setText(app.nome)
            self.ui.aplicativos.setItem(num, 0, item)
    
    def sinal_excluir(self, *args, **kwargs):
        self.inicializar()

    def sinal_salvar(self, *args, **kwargs):
        self.inicializar()