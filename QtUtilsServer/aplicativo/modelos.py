import os
import secrets
import shutil
from string import ascii_lowercase

from loguru import logger
from peewee import *
from typing_extensions import Final

from QtUtils import QtUtils
from QtUtils.crypto import ControladorCrypto, RSAPublicKey
from QtUtils.db import DataBase
from QtUtilsServer.utils.modelos import ModeloBase


class Aplicativo(ModeloBase):
    # Campos
    bd_nome = CharField()
    bd_usuario = CharField()
    bd_senha = CharField()
    bd_servidor = CharField(null=True)
    bd_porta = IntegerField(null=True)
    codigo = CharField(unique=True)
    nome = CharField()

    # Diretorios
    diretorio_comum: Final = os.path.join(QtUtils.definicoes.diretorio_base, 'aplicativos')
    __diretorio_proprio = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.id:
            self.__diretorio_proprio = os.path.join(self.diretorio_comum, self.codigo)
    
    def __str__(self):
        return self.nome
    
    def antes_de_salvar(self) -> None:
        # Criando credenciais do banco de dados
        if self.id is None:
            self.criar_dados_bd()
            # Sufixo em nomes em ambiente de desenvolvimento
            if QtUtils.definicoes.ambiente_desenvolvimento:
                self.bd_nome += '_dev'
                self.bd_usuario += '_dev'
    
    def apos_excluir(self) -> None:
        self.diretorio_excluir()
    
    def apos_salvar(self) -> None:
        self.diretorio_manusear()
    
    def criar_dados_bd(self):
        self.bd_nome = f"{self.codigo}_{secrets.token_hex(3)}"
        self.bd_usuario = f"{self.codigo}_{secrets.token_hex(3)}"
        self.bd_senha = secrets.token_hex(16)
        self.bd_servidor = None
        self.bd_porta = None
    
    def dados_api(self) -> dict:
        return {
            'estado': 'operacional',
            'postgres': {
                'host': self.bd_servidor,
                'port': self.bd_porta or QtUtils.config.postgres.port,
                'database': self.bd_nome,
                'user': self.bd_usuario,
                'password': self.bd_senha,
            },
        }
    
    def diretorio_excluir(self):
        if self.diretorio_existente():
            mover = os.path.join(
                self.diretorio_comum, 
                f"{self.codigo}_excluido_{DataBase().data().strftime('%Y%m%d%H%M%S')}"
            )
            shutil.move(src=self.diretorio, dst=mover)
        else:
            logger.warning(f"Diretório '{self.diretorio_comum}' inexistente para exclusão.")
    
    @property
    def diretorio(self):
        return os.path.join(self.diretorio_comum, self.codigo)
    
    def diretorio_existente(self) -> bool:
        return os.path.exists(self.diretorio)
    
    @property
    def diretorio_versoes(self):
        diretorio = os.path.join(self.diretorio, 'versoes')
        # Criando diretório caso não exista
        if not os.path.isdir(diretorio):
            os.mkdir(diretorio)
        return diretorio
    
    def diretorio_manusear(self) -> None:
        # Criar diretório comum dos aplicativos
        if not os.path.isdir(self.diretorio_comum):
            os.mkdir(self.diretorio_comum)
        # Criar/Alterar diretorio do aplicativo
        if self.diretorio != self.__diretorio_proprio:
            if self.__diretorio_proprio and os.path.exists(self.__diretorio_proprio):
                shutil.move(src=self.__diretorio_proprio, dst=self.diretorio)
            else:
                if os.path.exists(self.diretorio):
                    # Mover diretório caso exista
                    mover = os.path.join(
                        self.diretorio_comum, 
                        f"{self.codigo}_movido_{DataBase().data().strftime('%Y%m%d%H%M%S')}",
                    )
                    logger.debug(f"Diretório '{self.diretorio}' existente movido para '{mover}'.")
                    shutil.move(src=self.diretorio, dst=mover)
                os.mkdir(self.diretorio)
        self.__diretorio_proprio = self.diretorio

    def validar(self) -> bool:
        erros = {}
        # Validando código
        if not self.codigo:
            erros.update({'Código': 'Não preenchido.'})
        else:
            self.codigo = ''.join([c for c in self.codigo.lower() if c in ascii_lowercase])
        outro_app = Aplicativo.selecione(codigo=self.codigo).first()
        if outro_app:
            if outro_app.id == self.id:
                if outro_app.codigo != self.codigo:
                    erros.update({'Código': 'Não é possível a alteração.'})
            else:
                erros.update({'Código': 'Já existente.'})
        # Validando nome
        if not self.nome:
            erros.update({'Nome': 'Não preenchido.'})
        # Mostrando ou não erros
        if erros:
            Aplicativo.mostrar_erros_validacao(
                erros=erros,
                texto="Erros encontrados:",
            )
            return False
        return True


class Cliente(ModeloBase):
    id_ = CharField(unique=True)
    app: Aplicativo = ForeignKeyField(model=Aplicativo, backref='clientes')
    autorizado = BooleanField(default=False)
    nome = CharField(default='')
    pkey = TextField()

    def __str__(self) -> str:
        return f'{self.nome} - ({self.id_})'
    
    @classmethod
    def todos(cls, **kwargs):
        return cls.filter(**kwargs).order_by(cls.nome.asc())

    def dados_de_acesso(self) -> dict:
        ctrl_crypt = ControladorCrypto()
        return self.public_key().encrypt_dict({
            **self.app.dados_api(),
            'certificado': ctrl_crypt.certificate.to_string(),
            'versoes': self.list_subclass['Versao'].dados_api(aplicativo=self.app)
        })
    
    def public_key(self) -> RSAPublicKey:
        return RSAPublicKey.from_string(s=self.pkey)

    def validar(self) -> bool:
        return True