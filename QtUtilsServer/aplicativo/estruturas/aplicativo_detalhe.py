# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'aplicativo_detalhe.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(606, 375)
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget.sizePolicy().hasHeightForWidth())
        self.widget.setSizePolicy(sizePolicy)
        self.gridLayout = QGridLayout(self.widget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_2 = QLabel(self.widget)
        self.label_2.setObjectName(u"label_2")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy1)
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_2, 0, 0, 1, 1)

        self.label_4 = QLabel(self.widget)
        self.label_4.setObjectName(u"label_4")
        sizePolicy1.setHeightForWidth(self.label_4.sizePolicy().hasHeightForWidth())
        self.label_4.setSizePolicy(sizePolicy1)
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_4, 3, 0, 1, 1)

        self.lb_codigo = QLabel(self.widget)
        self.lb_codigo.setObjectName(u"lb_codigo")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.lb_codigo.sizePolicy().hasHeightForWidth())
        self.lb_codigo.setSizePolicy(sizePolicy2)

        self.gridLayout.addWidget(self.lb_codigo, 0, 1, 1, 1)

        self.lb_nome = QLabel(self.widget)
        self.lb_nome.setObjectName(u"lb_nome")

        self.gridLayout.addWidget(self.lb_nome, 3, 1, 1, 1)


        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy3)
        self.gridLayout_2 = QGridLayout(self.widget_2)
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.tabWidget = QTabWidget(self.widget_2)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.verticalLayout_2 = QVBoxLayout(self.tab)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.widget_4 = QWidget(self.tab)
        self.widget_4.setObjectName(u"widget_4")
        sizePolicy.setHeightForWidth(self.widget_4.sizePolicy().hasHeightForWidth())
        self.widget_4.setSizePolicy(sizePolicy)
        self.horizontalLayout_2 = QHBoxLayout(self.widget_4)
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.pushButton_3 = QPushButton(self.widget_4)
        self.pushButton_3.setObjectName(u"pushButton_3")

        self.horizontalLayout_2.addWidget(self.pushButton_3)

        self.horizontalSpacer_2 = QSpacerItem(480, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)


        self.verticalLayout_2.addWidget(self.widget_4)

        self.widget_5 = QWidget(self.tab)
        self.widget_5.setObjectName(u"widget_5")
        self.gridLayout_3 = QGridLayout(self.widget_5)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.tbl_versoes = QTableWidget(self.widget_5)
        if (self.tbl_versoes.columnCount() < 2):
            self.tbl_versoes.setColumnCount(2)
        __qtablewidgetitem = QTableWidgetItem()
        self.tbl_versoes.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.tbl_versoes.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        if (self.tbl_versoes.rowCount() < 2):
            self.tbl_versoes.setRowCount(2)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.tbl_versoes.setVerticalHeaderItem(0, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.tbl_versoes.setVerticalHeaderItem(1, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.tbl_versoes.setItem(0, 0, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.tbl_versoes.setItem(0, 1, __qtablewidgetitem5)
        __qtablewidgetitem6 = QTableWidgetItem()
        self.tbl_versoes.setItem(1, 0, __qtablewidgetitem6)
        __qtablewidgetitem7 = QTableWidgetItem()
        self.tbl_versoes.setItem(1, 1, __qtablewidgetitem7)
        self.tbl_versoes.setObjectName(u"tbl_versoes")
        self.tbl_versoes.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tbl_versoes.setAlternatingRowColors(True)
        self.tbl_versoes.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tbl_versoes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tbl_versoes.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tbl_versoes.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tbl_versoes.horizontalHeader().setHighlightSections(False)
        self.tbl_versoes.verticalHeader().setVisible(False)

        self.gridLayout_3.addWidget(self.tbl_versoes, 0, 0, 1, 1)


        self.verticalLayout_2.addWidget(self.widget_5)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_3 = QVBoxLayout(self.tab_2)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.widget_7 = QWidget(self.tab_2)
        self.widget_7.setObjectName(u"widget_7")
        sizePolicy.setHeightForWidth(self.widget_7.sizePolicy().hasHeightForWidth())
        self.widget_7.setSizePolicy(sizePolicy)
        self.horizontalLayout_3 = QHBoxLayout(self.widget_7)
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.btn_editar_bd = QPushButton(self.widget_7)
        self.btn_editar_bd.setObjectName(u"btn_editar_bd")

        self.horizontalLayout_3.addWidget(self.btn_editar_bd)

        self.pushButton_4 = QPushButton(self.widget_7)
        self.pushButton_4.setObjectName(u"pushButton_4")

        self.horizontalLayout_3.addWidget(self.pushButton_4)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_3)


        self.verticalLayout_3.addWidget(self.widget_7)

        self.widget_6 = QWidget(self.tab_2)
        self.widget_6.setObjectName(u"widget_6")
        self.formLayout = QFormLayout(self.widget_6)
        self.formLayout.setObjectName(u"formLayout")
        self.label = QLabel(self.widget_6)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.bd_nome = QLineEdit(self.widget_6)
        self.bd_nome.setObjectName(u"bd_nome")
        sizePolicy4 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.bd_nome.sizePolicy().hasHeightForWidth())
        self.bd_nome.setSizePolicy(sizePolicy4)
        self.bd_nome.setReadOnly(True)

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.bd_nome)

        self.label_3 = QLabel(self.widget_6)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_3)

        self.bd_usuario = QLineEdit(self.widget_6)
        self.bd_usuario.setObjectName(u"bd_usuario")
        sizePolicy4.setHeightForWidth(self.bd_usuario.sizePolicy().hasHeightForWidth())
        self.bd_usuario.setSizePolicy(sizePolicy4)
        self.bd_usuario.setReadOnly(True)

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.bd_usuario)

        self.label_5 = QLabel(self.widget_6)
        self.label_5.setObjectName(u"label_5")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_5)

        self.bd_senha = QLineEdit(self.widget_6)
        self.bd_senha.setObjectName(u"bd_senha")
        sizePolicy5 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy5.setHorizontalStretch(0)
        sizePolicy5.setVerticalStretch(0)
        sizePolicy5.setHeightForWidth(self.bd_senha.sizePolicy().hasHeightForWidth())
        self.bd_senha.setSizePolicy(sizePolicy5)
        self.bd_senha.setEchoMode(QLineEdit.Normal)
        self.bd_senha.setReadOnly(True)

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.bd_senha)


        self.verticalLayout_3.addWidget(self.widget_6)

        self.tabWidget.addTab(self.tab_2, "")
        self.clientes = QWidget()
        self.clientes.setObjectName(u"clientes")
        self.gridLayout_4 = QGridLayout(self.clientes)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.tbl_clientes = QTableWidget(self.clientes)
        if (self.tbl_clientes.columnCount() < 3):
            self.tbl_clientes.setColumnCount(3)
        __qtablewidgetitem8 = QTableWidgetItem()
        self.tbl_clientes.setHorizontalHeaderItem(0, __qtablewidgetitem8)
        __qtablewidgetitem9 = QTableWidgetItem()
        self.tbl_clientes.setHorizontalHeaderItem(1, __qtablewidgetitem9)
        __qtablewidgetitem10 = QTableWidgetItem()
        self.tbl_clientes.setHorizontalHeaderItem(2, __qtablewidgetitem10)
        if (self.tbl_clientes.rowCount() < 2):
            self.tbl_clientes.setRowCount(2)
        __qtablewidgetitem11 = QTableWidgetItem()
        self.tbl_clientes.setVerticalHeaderItem(0, __qtablewidgetitem11)
        __qtablewidgetitem12 = QTableWidgetItem()
        self.tbl_clientes.setVerticalHeaderItem(1, __qtablewidgetitem12)
        __qtablewidgetitem13 = QTableWidgetItem()
        self.tbl_clientes.setItem(0, 0, __qtablewidgetitem13)
        __qtablewidgetitem14 = QTableWidgetItem()
        self.tbl_clientes.setItem(0, 1, __qtablewidgetitem14)
        __qtablewidgetitem15 = QTableWidgetItem()
        self.tbl_clientes.setItem(0, 2, __qtablewidgetitem15)
        __qtablewidgetitem16 = QTableWidgetItem()
        self.tbl_clientes.setItem(1, 0, __qtablewidgetitem16)
        __qtablewidgetitem17 = QTableWidgetItem()
        self.tbl_clientes.setItem(1, 1, __qtablewidgetitem17)
        __qtablewidgetitem18 = QTableWidgetItem()
        self.tbl_clientes.setItem(1, 2, __qtablewidgetitem18)
        self.tbl_clientes.setObjectName(u"tbl_clientes")
        self.tbl_clientes.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.tbl_clientes.setAlternatingRowColors(True)
        self.tbl_clientes.setSelectionMode(QAbstractItemView.SingleSelection)
        self.tbl_clientes.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.tbl_clientes.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tbl_clientes.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.tbl_clientes.setSortingEnabled(True)
        self.tbl_clientes.horizontalHeader().setHighlightSections(False)
        self.tbl_clientes.horizontalHeader().setStretchLastSection(True)
        self.tbl_clientes.verticalHeader().setVisible(False)

        self.gridLayout_4.addWidget(self.tbl_clientes, 0, 0, 1, 1)

        self.tabWidget.addTab(self.clientes, "")

        self.gridLayout_2.addWidget(self.tabWidget, 0, 0, 1, 1)


        self.verticalLayout.addWidget(self.widget_2)

        self.widget_3 = QWidget(Dialog)
        self.widget_3.setObjectName(u"widget_3")
        sizePolicy6 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Minimum)
        sizePolicy6.setHorizontalStretch(0)
        sizePolicy6.setVerticalStretch(0)
        sizePolicy6.setHeightForWidth(self.widget_3.sizePolicy().hasHeightForWidth())
        self.widget_3.setSizePolicy(sizePolicy6)
        self.horizontalLayout = QHBoxLayout(self.widget_3)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(217, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(self.widget_3)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.pushButton = QPushButton(self.widget_3)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)


        self.verticalLayout.addWidget(self.widget_3)

        QWidget.setTabOrder(self.pushButton_3, self.tbl_versoes)
        QWidget.setTabOrder(self.tbl_versoes, self.btn_editar_bd)
        QWidget.setTabOrder(self.btn_editar_bd, self.pushButton_4)
        QWidget.setTabOrder(self.pushButton_4, self.bd_nome)
        QWidget.setTabOrder(self.bd_nome, self.bd_usuario)
        QWidget.setTabOrder(self.bd_usuario, self.bd_senha)
        QWidget.setTabOrder(self.bd_senha, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.reject)
        self.pushButton_2.released.connect(Dialog.cadastro)
        self.pushButton_3.released.connect(Dialog.versao_nova)
        self.tbl_versoes.cellDoubleClicked.connect(Dialog.versao_cadastro)
        self.btn_editar_bd.released.connect(Dialog.editar_bd)
        self.pushButton_4.released.connect(Dialog.resetar_bd)
        self.tbl_clientes.cellDoubleClicked.connect(Dialog.cliente)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Aplicativo detalhe", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"C\u00f3digo:", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
        self.lb_codigo.setText(QCoreApplication.translate("Dialog", u"TextLabel", None))
        self.lb_nome.setText(QCoreApplication.translate("Dialog", u"TextLabel", None))
        self.pushButton_3.setText(QCoreApplication.translate("Dialog", u"Nova", None))
        ___qtablewidgetitem = self.tbl_versoes.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Dialog", u"Nome", None));
        ___qtablewidgetitem1 = self.tbl_versoes.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Dialog", u"Implementa\u00e7\u00e3o", None));
        ___qtablewidgetitem2 = self.tbl_versoes.verticalHeaderItem(0)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Dialog", u"1", None));
        ___qtablewidgetitem3 = self.tbl_versoes.verticalHeaderItem(1)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Dialog", u"2", None));

        __sortingEnabled = self.tbl_versoes.isSortingEnabled()
        self.tbl_versoes.setSortingEnabled(False)
        ___qtablewidgetitem4 = self.tbl_versoes.item(0, 0)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Dialog", u"Teste 1", None));
        ___qtablewidgetitem5 = self.tbl_versoes.item(0, 1)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("Dialog", u"Teste 1", None));
        ___qtablewidgetitem6 = self.tbl_versoes.item(1, 0)
        ___qtablewidgetitem6.setText(QCoreApplication.translate("Dialog", u"Teste 2", None));
        ___qtablewidgetitem7 = self.tbl_versoes.item(1, 1)
        ___qtablewidgetitem7.setText(QCoreApplication.translate("Dialog", u"Teste 2", None));
        self.tbl_versoes.setSortingEnabled(__sortingEnabled)

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("Dialog", u"Vers\u00f5es", None))
        self.btn_editar_bd.setText(QCoreApplication.translate("Dialog", u"Editar", None))
        self.pushButton_4.setText(QCoreApplication.translate("Dialog", u"Resetar", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Bando de dados:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Usu\u00e1rio:", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Senha:", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("Dialog", u"PostgreSQL", None))
        ___qtablewidgetitem8 = self.tbl_clientes.horizontalHeaderItem(0)
        ___qtablewidgetitem8.setText(QCoreApplication.translate("Dialog", u"ID", None));
        ___qtablewidgetitem9 = self.tbl_clientes.horizontalHeaderItem(1)
        ___qtablewidgetitem9.setText(QCoreApplication.translate("Dialog", u"Autorizado", None));
        ___qtablewidgetitem10 = self.tbl_clientes.horizontalHeaderItem(2)
        ___qtablewidgetitem10.setText(QCoreApplication.translate("Dialog", u"Descri\u00e7\u00e3o", None));
        ___qtablewidgetitem11 = self.tbl_clientes.verticalHeaderItem(0)
        ___qtablewidgetitem11.setText(QCoreApplication.translate("Dialog", u"1", None));
        ___qtablewidgetitem12 = self.tbl_clientes.verticalHeaderItem(1)
        ___qtablewidgetitem12.setText(QCoreApplication.translate("Dialog", u"2", None));

        __sortingEnabled1 = self.tbl_clientes.isSortingEnabled()
        self.tbl_clientes.setSortingEnabled(False)
        ___qtablewidgetitem13 = self.tbl_clientes.item(0, 0)
        ___qtablewidgetitem13.setText(QCoreApplication.translate("Dialog", u"E0C84-12C75-155CD-0CA90-F5B8D-1EBF7", None));
        ___qtablewidgetitem14 = self.tbl_clientes.item(0, 1)
        ___qtablewidgetitem14.setText(QCoreApplication.translate("Dialog", u"Sim", None));
        ___qtablewidgetitem15 = self.tbl_clientes.item(0, 2)
        ___qtablewidgetitem15.setText(QCoreApplication.translate("Dialog", u"RH-01 / Computador do Jo\u00e3o", None));
        ___qtablewidgetitem16 = self.tbl_clientes.item(1, 0)
        ___qtablewidgetitem16.setText(QCoreApplication.translate("Dialog", u"A", None));
        ___qtablewidgetitem17 = self.tbl_clientes.item(1, 1)
        ___qtablewidgetitem17.setText(QCoreApplication.translate("Dialog", u"N\u00e3o", None));
        ___qtablewidgetitem18 = self.tbl_clientes.item(1, 2)
        ___qtablewidgetitem18.setText(QCoreApplication.translate("Dialog", u"RH-02", None));
        self.tbl_clientes.setSortingEnabled(__sortingEnabled1)

        self.tabWidget.setTabText(self.tabWidget.indexOf(self.clientes), QCoreApplication.translate("Dialog", u"Clientes", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Cadastro", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
    # retranslateUi

