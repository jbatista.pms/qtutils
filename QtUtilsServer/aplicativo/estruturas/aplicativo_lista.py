# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'aplicativo_lista.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(544, 261)
        Dialog.setMinimumSize(QSize(544, 261))
        Dialog.setMaximumSize(QSize(544, 261))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.aplicativos = QTableWidget(Dialog)
        if (self.aplicativos.columnCount() < 1):
            self.aplicativos.setColumnCount(1)
        __qtablewidgetitem = QTableWidgetItem()
        self.aplicativos.setHorizontalHeaderItem(0, __qtablewidgetitem)
        self.aplicativos.setObjectName(u"aplicativos")
        self.aplicativos.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.aplicativos.setAlternatingRowColors(True)
        self.aplicativos.setSelectionMode(QAbstractItemView.SingleSelection)
        self.aplicativos.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.aplicativos.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.aplicativos.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.aplicativos.horizontalHeader().setVisible(False)
        self.aplicativos.horizontalHeader().setHighlightSections(False)
        self.aplicativos.horizontalHeader().setStretchLastSection(True)
        self.aplicativos.verticalHeader().setVisible(False)
        self.aplicativos.verticalHeader().setHighlightSections(False)

        self.verticalLayout.addWidget(self.aplicativos)

        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        self.horizontalLayout = QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(self.widget)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.pushButton = QPushButton(self.widget)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)


        self.verticalLayout.addWidget(self.widget)

        QWidget.setTabOrder(self.pushButton_2, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton.released.connect(Dialog.reject)
        self.pushButton_2.released.connect(Dialog.adicionar)
        self.aplicativos.cellDoubleClicked.connect(Dialog.abrir_cadastro)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Aplicativos", None))
        ___qtablewidgetitem = self.aplicativos.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Dialog", u"Nome", None));
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Adicionar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
    # retranslateUi

