# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'aplicativo_cadastro_bd.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(257, 275)
        Dialog.setMinimumSize(QSize(257, 275))
        Dialog.setMaximumSize(QSize(257, 275))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        self.formLayout = QFormLayout(self.widget)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setRowWrapPolicy(QFormLayout.WrapLongRows)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.widget)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.bd_servidor = QLineEdit(self.widget)
        self.bd_servidor.setObjectName(u"bd_servidor")

        self.formLayout.setWidget(1, QFormLayout.SpanningRole, self.bd_servidor)

        self.label_3 = QLabel(self.widget)
        self.label_3.setObjectName(u"label_3")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.label_3)

        self.label_5 = QLabel(self.widget)
        self.label_5.setObjectName(u"label_5")

        self.formLayout.setWidget(4, QFormLayout.LabelRole, self.label_5)

        self.bd_nome = QLineEdit(self.widget)
        self.bd_nome.setObjectName(u"bd_nome")

        self.formLayout.setWidget(5, QFormLayout.SpanningRole, self.bd_nome)

        self.label_2 = QLabel(self.widget)
        self.label_2.setObjectName(u"label_2")

        self.formLayout.setWidget(6, QFormLayout.LabelRole, self.label_2)

        self.bd_usuario = QLineEdit(self.widget)
        self.bd_usuario.setObjectName(u"bd_usuario")

        self.formLayout.setWidget(7, QFormLayout.SpanningRole, self.bd_usuario)

        self.label_4 = QLabel(self.widget)
        self.label_4.setObjectName(u"label_4")

        self.formLayout.setWidget(8, QFormLayout.LabelRole, self.label_4)

        self.bd_senha = QLineEdit(self.widget)
        self.bd_senha.setObjectName(u"bd_senha")
        self.bd_senha.setEchoMode(QLineEdit.Normal)

        self.formLayout.setWidget(9, QFormLayout.SpanningRole, self.bd_senha)

        self.bd_porta = QSpinBox(self.widget)
        self.bd_porta.setObjectName(u"bd_porta")
        self.bd_porta.setMaximum(65536)

        self.formLayout.setWidget(3, QFormLayout.SpanningRole, self.bd_porta)


        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy)
        self.horizontalLayout = QHBoxLayout(self.widget_2)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 9, 0, 0)
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton_2 = QPushButton(self.widget_2)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)

        self.pushButton = QPushButton(self.widget_2)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)


        self.verticalLayout.addWidget(self.widget_2)

        QWidget.setTabOrder(self.bd_servidor, self.bd_porta)
        QWidget.setTabOrder(self.bd_porta, self.bd_nome)
        QWidget.setTabOrder(self.bd_nome, self.bd_usuario)
        QWidget.setTabOrder(self.bd_usuario, self.bd_senha)
        QWidget.setTabOrder(self.bd_senha, self.pushButton_2)
        QWidget.setTabOrder(self.pushButton_2, self.pushButton)

        self.retranslateUi(Dialog)
        self.pushButton_2.released.connect(Dialog.reject)
        self.pushButton.released.connect(Dialog.salvar)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Acesso ao banco de dados", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"Servidor:", None))
        self.label_3.setText(QCoreApplication.translate("Dialog", u"Porta:", None))
        self.label_5.setText(QCoreApplication.translate("Dialog", u"Banco de dados:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Usu\u00e1rio:", None))
        self.label_4.setText(QCoreApplication.translate("Dialog", u"Senha:", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

