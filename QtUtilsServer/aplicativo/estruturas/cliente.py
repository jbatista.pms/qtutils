# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'cliente.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(265, 140)
        Dialog.setMinimumSize(QSize(265, 140))
        Dialog.setMaximumSize(QSize(265, 140))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        self.gridLayout = QGridLayout(self.widget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName(u"gridLayout")
        self.label = QLabel(self.widget)
        self.label.setObjectName(u"label")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.id_ = QLineEdit(self.widget)
        self.id_.setObjectName(u"id_")

        self.gridLayout.addWidget(self.id_, 1, 0, 1, 1)

        self.label_2 = QLabel(self.widget)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.nome = QLineEdit(self.widget)
        self.nome.setObjectName(u"nome")

        self.gridLayout.addWidget(self.nome, 3, 0, 1, 1)


        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        self.horizontalLayout = QHBoxLayout(self.widget_2)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 9, 0, 0)
        self.btn_status = QPushButton(self.widget_2)
        self.btn_status.setObjectName(u"btn_status")

        self.horizontalLayout.addWidget(self.btn_status)

        self.btn_fechar = QPushButton(self.widget_2)
        self.btn_fechar.setObjectName(u"btn_fechar")

        self.horizontalLayout.addWidget(self.btn_fechar)


        self.verticalLayout.addWidget(self.widget_2)


        self.retranslateUi(Dialog)
        self.btn_status.released.connect(Dialog.mudar_autorizacao)
        self.btn_fechar.released.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cliente", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"ID:", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Nome:", None))
        self.btn_status.setText(QCoreApplication.translate("Dialog", u"Autorizar", None))
        self.btn_fechar.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
    # retranslateUi

