# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'aplicativo_cadastro.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(294, 102)
        Dialog.setMinimumSize(QSize(294, 102))
        Dialog.setMaximumSize(QSize(294, 102))
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.widget = QWidget(Dialog)
        self.widget.setObjectName(u"widget")
        self.formLayout = QFormLayout(self.widget)
        self.formLayout.setObjectName(u"formLayout")
        self.formLayout.setContentsMargins(0, 0, 0, 9)
        self.label = QLabel(self.widget)
        self.label.setObjectName(u"label")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.label)

        self.codigo = QLineEdit(self.widget)
        self.codigo.setObjectName(u"codigo")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.codigo)

        self.label_2 = QLabel(self.widget)
        self.label_2.setObjectName(u"label_2")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.label_2)

        self.nome = QLineEdit(self.widget)
        self.nome.setObjectName(u"nome")

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.nome)


        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(Dialog)
        self.widget_2.setObjectName(u"widget_2")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget_2.sizePolicy().hasHeightForWidth())
        self.widget_2.setSizePolicy(sizePolicy)
        self.horizontalLayout = QHBoxLayout(self.widget_2)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalSpacer = QSpacerItem(30, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pushButton = QPushButton(self.widget_2)
        self.pushButton.setObjectName(u"pushButton")

        self.horizontalLayout.addWidget(self.pushButton)

        self.btn_excluir = QPushButton(self.widget_2)
        self.btn_excluir.setObjectName(u"btn_excluir")

        self.horizontalLayout.addWidget(self.btn_excluir)

        self.pushButton_2 = QPushButton(self.widget_2)
        self.pushButton_2.setObjectName(u"pushButton_2")

        self.horizontalLayout.addWidget(self.pushButton_2)


        self.verticalLayout.addWidget(self.widget_2)

        QWidget.setTabOrder(self.codigo, self.nome)
        QWidget.setTabOrder(self.nome, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.btn_excluir)
        QWidget.setTabOrder(self.btn_excluir, self.pushButton_2)

        self.retranslateUi(Dialog)
        self.pushButton.pressed.connect(Dialog.reject)
        self.pushButton_2.pressed.connect(Dialog.salvar)
        self.btn_excluir.released.connect(Dialog.excluir)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Cadastro Aplicativo", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"C\u00f3digo", None))
        self.label_2.setText(QCoreApplication.translate("Dialog", u"Nome", None))
        self.pushButton.setText(QCoreApplication.translate("Dialog", u"Fechar", None))
        self.btn_excluir.setText(QCoreApplication.translate("Dialog", u"Excluir", None))
        self.pushButton_2.setText(QCoreApplication.translate("Dialog", u"Salvar", None))
    # retranslateUi

