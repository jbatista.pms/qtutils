# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'janela_principal.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from  . import icones_rc

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(732, 445)
        self.actionAplicativos = QAction(MainWindow)
        self.actionAplicativos.setObjectName(u"actionAplicativos")
        icon = QIcon()
        icon.addFile(u":/icones/icones/apps.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionAplicativos.setIcon(icon)
        self.actionEncerrar = QAction(MainWindow)
        self.actionEncerrar.setObjectName(u"actionEncerrar")
        icon1 = QIcon()
        icon1.addFile(u":/icones/icones/logout.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionEncerrar.setIcon(icon1)
        self.actionConfiguracoes = QAction(MainWindow)
        self.actionConfiguracoes.setObjectName(u"actionConfiguracoes")
        icon2 = QIcon()
        icon2.addFile(u":/icones/icones/settings.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionConfiguracoes.setIcon(icon2)
        self.actionBackups = QAction(MainWindow)
        self.actionBackups.setObjectName(u"actionBackups")
        icon3 = QIcon()
        icon3.addFile(u":/icones/icones/backup_bd.png", QSize(), QIcon.Normal, QIcon.Off)
        self.actionBackups.setIcon(icon3)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.verticalLayout_2 = QVBoxLayout(self.tab)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.groupBox = QGroupBox(self.tab)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.estado_api = QLabel(self.groupBox)
        self.estado_api.setObjectName(u"estado_api")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.estado_api.sizePolicy().hasHeightForWidth())
        self.estado_api.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.estado_api, 0, 1, 1, 1)

        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label_3, 2, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 3, 0, 1, 1)

        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)

        self.estado_relatorio = QLabel(self.groupBox)
        self.estado_relatorio.setObjectName(u"estado_relatorio")

        self.gridLayout.addWidget(self.estado_relatorio, 2, 1, 1, 1)


        self.verticalLayout_2.addWidget(self.groupBox)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.verticalLayout_3 = QVBoxLayout(self.tab_2)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.tl_registros = QTextEdit(self.tab_2)
        self.tl_registros.setObjectName(u"tl_registros")
        self.tl_registros.setLineWrapMode(QTextEdit.NoWrap)
        self.tl_registros.setReadOnly(True)

        self.verticalLayout_3.addWidget(self.tl_registros)

        self.tabWidget.addTab(self.tab_2, "")

        self.verticalLayout.addWidget(self.tabWidget)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 732, 21))
        MainWindow.setMenuBar(self.menubar)
        self.toolBar = QToolBar(MainWindow)
        self.toolBar.setObjectName(u"toolBar")
        self.toolBar.setMovable(False)
        self.toolBar.setToolButtonStyle(Qt.ToolButtonTextBesideIcon)
        self.toolBar.setFloatable(False)
        MainWindow.addToolBar(Qt.TopToolBarArea, self.toolBar)

        self.toolBar.addAction(self.actionAplicativos)
        self.toolBar.addAction(self.actionBackups)
        self.toolBar.addAction(self.actionConfiguracoes)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionEncerrar)

        self.retranslateUi(MainWindow)
        self.actionEncerrar.triggered.connect(MainWindow.encerrar)
        self.actionAplicativos.triggered.connect(MainWindow.aplicativo_lista)
        self.actionConfiguracoes.triggered.connect(MainWindow.configuracao)
        self.actionBackups.triggered.connect(MainWindow.backups)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"QtUtilsServer", None))
        self.actionAplicativos.setText(QCoreApplication.translate("MainWindow", u"Aplicativos", None))
        self.actionEncerrar.setText(QCoreApplication.translate("MainWindow", u"Encerrar", None))
        self.actionConfiguracoes.setText(QCoreApplication.translate("MainWindow", u"Configura\u00e7\u00f5es", None))
        self.actionBackups.setText(QCoreApplication.translate("MainWindow", u"Backups", None))
#if QT_CONFIG(tooltip)
        self.actionBackups.setToolTip(QCoreApplication.translate("MainWindow", u"Backups", None))
#endif // QT_CONFIG(tooltip)
        self.groupBox.setTitle(QCoreApplication.translate("MainWindow", u"Servi\u00e7os", None))
        self.estado_api.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" color:#ff5500;\">Estado indispon\u00edvel.</span></p></body></html>", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" font-weight:600;\">Relat\u00f3rios:</span></p></body></html>", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" font-weight:600;\">Api:</span></p></body></html>", None))
        self.estado_relatorio.setText(QCoreApplication.translate("MainWindow", u"<html><head/><body><p><span style=\" color:#ff5500;\">Estado indispon\u00edvel.</span></p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("MainWindow", u"Estado", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("MainWindow", u"Registros", None))
        self.toolBar.setWindowTitle(QCoreApplication.translate("MainWindow", u"toolBar", None))
    # retranslateUi

