from typing import Optional

from loguru import logger
from peewee import *
from PySide2 import QtWidgets

from QtUtils import QtUtils
from QtUtils.colecoes.erros import ErrosFormularioDialogo
from QtUtils.db import ModelBase
from QtUtils.sinais import Sinal


class ModeloBase(ModelBase):
    list_subclass = {}

    def __init_subclass__(cls, *args, **kwargs) -> None:
        super().__init_subclass__(*args, **kwargs)
        cls.list_subclass.update({cls.__name__: cls})
        cls.sinal_salvar = Sinal(f'{cls.__name__}.sinal_salvar')
        cls.sinal_excluir = Sinal(f'{cls.__name__}.sinal_excluir')

    @staticmethod
    def mostrar_erros_validacao(erros: dict, texto: str):
        if QtWidgets.QApplication.instance():
            msg = ErrosFormularioDialogo(erros=erros)
            msg.setText(texto)
            msg.exec_()
        else:
            raise Exception("{texto}: {erros}")
        return False
    
    def antes_de_salvar(self) -> None:
        pass
    
    def apos_excluir(self) -> None:
        pass
    
    def apos_salvar(self) -> None:
        pass
    
    def atualizar(self, **kwargs):
        for k,v in kwargs.items():
            setattr(self, k, v)
    
    @classmethod
    def create(cls, **kwargs):
        instancia = cls(**kwargs)
        return instancia.salvar()
    
    def excluir(self, msg: Optional[str]=None, questionar: bool=True):
        if questionar:
            if QtWidgets.QApplication.instance():
                dlg = QtWidgets.QMessageBox(parent=QtUtils.instancia_janela)
                dlg.addButton("Não", QtWidgets.QMessageBox.NoRole)
                dlg.addButton("Sim", QtWidgets.QMessageBox.YesRole)
                dlg.setIcon(QtWidgets.QMessageBox.Question)
                dlg.setText(msg or "Exclusão permanentemente o registro?")
                dlg.setWindowTitle("Confirmação")
                if dlg.exec_():
                    delete_obj = self.delete_instance()
                    self.apos_excluir()
                    self.sinal_excluir.emitir(obj=self)
                    return delete_obj
            else:
                if input("\nDigite 'sim' para confirmar a exlusão do registro: ") == 'sim':
                    delete_obj = self.delete_instance()
                    self.apos_excluir()
                    self.sinal_excluir.emitir(obj=self)
                    return delete_obj
        else:
            delete_obj = self.delete_instance()
            self.apos_excluir()
            self.sinal_excluir.emitir(obj=self)
            return delete_obj
    
    @classmethod
    def selecione(cls, *args, **kwargs):
        return cls.filter(*args, **kwargs)
    
    def salvar(self):
        # TODO: Atualizar nome de diretório para arquivos
        if self.validar():
            self.antes_de_salvar()
            self.save()
            self.apos_salvar()
            self.sinal_salvar.emitir(obj=self)
            return self

    def validar(self) -> bool:
        raise NotImplementedError(
            f"Validação não implementada para modelo {self.__class__.__name__}"
        )