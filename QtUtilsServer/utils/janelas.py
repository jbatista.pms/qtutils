from loguru import logger
from peewee import *
from PySide2 import QtWidgets
from typing_extensions import Final

from QtUtils import QtUtils


class DialogoInstanciaBase(QtWidgets.QDialog):
    classe_janela = None # Classe de janela
    instancia = None # Instância do registro
    modelo = None # Classe de modelo da instância

    def __init__(self, instancia=None, parent=None, *args, **kwargs) -> None:
        self.instancia = instancia
        if self.modelo:
            self.modelo.sinal_excluir.conectar(recebedor=self.__sinal_excluir)
            self.modelo.sinal_salvar.conectar(recebedor=self.__sinal_salvar)
        super(DialogoInstanciaBase, self).__init__(parent=parent or QtUtils.instancia_janela)
        self.ui = self.classe_janela()
        self.ui.setupUi(self)
        self.inicializar()
    
    def __sinal_excluir(self, obj, sinal) -> None:
        logger.debug(
            f"Sinal '{sinal}' '{obj.__class__.__name__}' recebido em "
            f"'{self.__class__.__name__}' id {id(self)}."
        )
        if obj == obj and self.instancia and self.instancia.id == obj.id:
            self.instancia = None
        self.sinal_excluir(obj, sinal)
    
    def __sinal_salvar(self, obj, sinal) -> None:
        logger.debug(
            f"Sinal '{sinal.id}' '{obj.__class__.__name__}' recebido em "
            f"'{self.__class__.__name__}' id {id(self)}."
        )
        self.sinal_salvar(obj=obj, sinal=sinal)
    
    def accept(self) -> None:
        self.encerrando()
        return super().accept()
    
    def encerrando(self) -> None:
        if self.modelo:
            self.modelo.sinal_excluir.desconectar(recebedor=self.__sinal_excluir)
            self.modelo.sinal_salvar.desconectar(recebedor=self.__sinal_salvar)
    
    @property
    def e_instancia(self)  -> bool:
        return not self.instancia is None
    
    def reject(self) -> None:
        self.encerrando()
        return super().reject()
    
    def sinal_excluir(self, obj, sinal) -> None:
        pass

    def sinal_salvar(self, obj, sinal) -> None:
        pass