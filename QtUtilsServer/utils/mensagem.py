LARANJA = 'ff5500'
VERDE = '005500'
VERMELHO = 'ff0000'

html = (
    '<html><head/><body><p><span style=" color:#{cor};">'
    '{mensagem}</span></p></body></html>'
)

def construir_mensagem(cor: str, mensagem: str) -> str:
    return html.format(cor=cor, mensagem=mensagem)