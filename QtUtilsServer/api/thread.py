from loguru import logger
from PySide2 import QtCore
from waitress import serve

from QtUtils import QtUtils
from QtUtilsServer.utils.mensagem import construir_mensagem, VERDE, VERMELHO
from . import ControladorAPI
from .flask import criar_instancia_flask


class APIThread(QtCore.QThread):    
    def run(self):
        logger.info(f'Iniciado serviço na porta {QtUtils.config.porta_api}.')
        ControladorAPI.sinal_estado.emitir(
            construir_mensagem(
                cor=VERDE,
                mensagem=f'Ativo (0.0.0.0:{QtUtils.config.porta_api}).',
            )
        )
        try:
            serve(app=criar_instancia_flask(), host='0.0.0.0', port=QtUtils.config.porta_api)
        except:
            logger.exception(f'Erro ao inicial a api:')
            ControladorAPI.sinal_estado.emitir(
                construir_mensagem(cor=VERMELHO, mensagem='Inativo.')
            )