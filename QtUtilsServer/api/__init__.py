from loguru import logger

from QtUtils import QtUtils
from QtUtils.sinais import Sinal
from QtUtils.controladores import ControladorBase
from QtUtilsServer.utils.mensagem import construir_mensagem, LARANJA


class ControladorAPI(ControladorBase):
    __th = None # Instância de APIThread
    sinal_estado = Sinal('API.estado')

    def configurar(self) -> None:
        if self.__th is None:
            from .thread import APIThread
            self.__th = APIThread()

    def encerrar(self) -> None:
        logger.debug("Encerrando api.")
        if self.__th and self.__th.isRunning():
            self.__th.terminate()
    
    def estado(self) -> bool:
        if self.__th is None:
            return False
        return self.__th.isRunning()
    
    def executar(self) -> None:
        if self.__th and not self.__th.isRunning():
            self.sinal_estado.emitir(construir_mensagem(cor=LARANJA, mensagem='Inicializando.'))
            self.__th.start()