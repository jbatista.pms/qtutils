from flask import Flask
from flask_restful import Resource, Api
from PySide2.QtCore import QThread

from QtUtils import QtUtils
from QtUtilsServer.relatorio.base import Office
from ..aplicativo.api import bp as bp_cliente
from ..relatorio.api import bp as bp_relatorio
from ..versao.api import bp as bp_versao
from . import ControladorAPI


class EstadoAPI(Resource):
    def get(self):
        return {
            'api': ControladorAPI().estado(),
            'relatorio': not bool(Office.validar(localizacao=QtUtils.config.office)),
        }


def criar_instancia_flask():
    app = Flask(__name__)
    app.register_blueprint(bp_cliente)
    app.register_blueprint(bp_relatorio)
    app.register_blueprint(bp_versao)
    api = Api(app)
    api.add_resource(EstadoAPI, '/')
    return app