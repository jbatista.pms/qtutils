from QtUtils import QtUtils
from PySide2 import QtGui, QtWidgets

from loguru import logger

from QtUtils.backup.backup import BackupDialogo
from QtUtilsServer.aplicativo.janelas import AplicativoLista
from QtUtilsServer.configuracao.janelas import ConfiguracaoJanela
from QtUtilsServer.api import ControladorAPI
from QtUtilsServer.relatorio.base import Office
from .estruturas import janela_principal


class JanelaPrincipal(QtWidgets.QMainWindow):
    def __init__(self, parent=None):
        super(JanelaPrincipal, self).__init__(parent)
        self.ui = janela_principal.Ui_MainWindow()
        self.ui.setupUi(self)
        ControladorAPI.sinal_estado.conectar(self.estado_api)
        Office.estado.conectar(self.estado_relatorio)
        logger.add(self.escrever_registro, catch=False, format="{message}", level="INFO")
        logger.info("Inicializando sistema.")
    
    def aplicativo_lista(self):
        AplicativoLista(parent=self).exec_()
    
    def backups(self):
        BackupDialogo(parent=self).exec_()
    
    def configuracao(self):
        ConfiguracaoJanela(parent=self).exec_()
    
    def closeEvent(self, event: QtGui.QCloseEvent) -> None:
        if QtUtils.config.comportamento.na_bandeja:
            event.ignore()
            self.hide()
        else:
            return super().closeEvent(event)
    
    def encerrar(self):
        self.close()
    
    def escrever_registro(self, registro: str) -> None:
        if registro.endswith('\n'):
            registro = registro[:-1]
        self.ui.tl_registros.append(registro)
    
    def estado_api(self, obj: str, *args, **kwargs):
        self.ui.estado_api.setText(obj)
    
    def estado_relatorio(self, obj: str, *args, **kwargs):
        self.ui.estado_relatorio.setText(obj)
    
    def show(self, forcar=False):
        if not forcar:
            if QtUtils.config.comportamento.na_bandeja:
                return None
        return super().show()