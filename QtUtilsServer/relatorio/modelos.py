import os
import shutil
import uuid
from typing import Optional

from loguru import logger
from peewee import ForeignKeyField, IntegerField, UUIDField
from pikepdf import Pdf

from QtUtils import QtUtils
from QtUtilsServer.aplicativo.modelos import Cliente
from QtUtilsServer.relatorio.base import Office
from QtUtilsServer.utils.modelos import ModeloBase


class RelatorioArquivo(ModeloBase):
    id_ = UUIDField(default=uuid.uuid4, unique=True)
    cliente = ForeignKeyField(Cliente)

    def adicionar_parte(self, dados: bytes, seq) -> bool:
        parte = RelatorioArquivoParte(arquivo=self, sequencia=seq)
        parte.salvar()
        return bool(parte.salvar_arquivo(dados=dados))

    @property
    def caminho(self):
        return os.path.join(self.diretorio, "arquivo.odt")
    
    @property
    def caminho_pdf(self):
        return os.path.join(self.diretorio, "arquivo.pdf")
    
    def converter(self) -> bool:
        # Converter partes
        office = Office(localizacao=QtUtils.config.office)
        partes = self.partes()
        if partes:
            pdf = Pdf.new()
            for p in partes:
                logger.debug(f"Convertendo parte id '{p.id}'")
                office.converter_para_pdf(arquivo=p.caminho_odt())
                pdf.pages.extend(Pdf.open(p.caminho_pdf()).pages)
            try:
                pdf.save(self.caminho_pdf)
            except RecursionError as erro:
                logger.error(
                    f"Erro ao gravar RelatorioArquivo "
                    f"'{self.id_}': {erro}"
                )
                return False
        return self.convertido

    @property
    def convertido(self):
        return os.path.exists(self.caminho_pdf)
    
    @property
    def diretorio(self):
        diretorio = os.path.join(RelatorioArquivo.diretorio_padrao(), f"{self.id_}")
        if not os.path.exists(diretorio):
            os.mkdir(diretorio)
        return diretorio
    
    @staticmethod
    def diretorio_padrao() -> str:
        return os.path.join(QtUtils.definicoes.diretorio_base, 'relatorios')
    
    def excluir(self):
        super().excluir(questionar=False)
        if os.path.exists(self.diretorio):
            shutil.rmtree(self.diretorio)
    
    def partes(self):
        return RelatorioArquivoParte.selecione(
            arquivo=self
        ).order_by(RelatorioArquivoParte.sequencia.asc())
    
    def validar(self) -> bool:
        return True


class RelatorioArquivoParte(ModeloBase):
    arquivo = ForeignKeyField(
        RelatorioArquivo,
        backref='partes_select',
        on_delete='CASCADE',
    )
    sequencia = IntegerField()

    def caminho_odt(self) -> str:
        return os.path.join(self.arquivo.diretorio, f"{self.id}.odt")
    
    def caminho_pdf(self) -> str:
        return os.path.join(self.arquivo.diretorio, f"{self.id}.pdf")
    
    def salvar_arquivo(self, dados: bytes) -> Optional[str]:
        try:
            with open(self.caminho_odt(), 'wb') as arq:
                arq.write(dados)
        except Exception as error:
            logger.error(f"Erro ao adicionar arquivo: {error}")
            self.excluir(questionar=False)
            return None
        else:
            return str(self.caminho_odt())
    
    def validar(self) -> bool:
        return True