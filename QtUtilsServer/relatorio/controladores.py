import os, shutil
from pathlib import WindowsPath

from QtUtils import QtUtils
from QtUtils.controladores import ControladorBase
from .base import Office
from .modelos import RelatorioArquivo, RelatorioArquivoParte


class ControladorRelatorio(ControladorBase):
    def configurar(self) -> None:
        diretorio_padrao = RelatorioArquivo.diretorio_padrao()
        if os.path.exists(diretorio_padrao):
            shutil.rmtree(diretorio_padrao, ignore_errors=True)
        os.mkdir(diretorio_padrao)
    
    def executar(self) -> None:
        RelatorioArquivoParte.drop_table()
        RelatorioArquivo.drop_table()
        RelatorioArquivo.create_table()
        RelatorioArquivoParte.create_table()
        if Office.validar(localizacao=QtUtils.config.office):
            Office.emitir_sinal_inativo()
        else:
            Office.emitir_sinal_ativo()