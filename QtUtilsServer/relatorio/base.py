import os
import configparser
import subprocess
from pathlib import PureWindowsPath
from typing import Optional
from typing_extensions import Final

from loguru import logger

from QtUtils.sinais import Sinal
from QtUtilsServer.utils.mensagem import construir_mensagem, VERDE, VERMELHO


class Office(object):
    estado = Sinal('Office.estado')

    def __init__(self, localizacao: str) -> None:
        erro = Office.validar(localizacao)
        if erro:
            logger.error(f"A instalação do Office em {localizacao} possui erro: {erro}.")
            executavel = None
            instalacao = None
            versao = None
        else:
            executavel = PureWindowsPath(localizacao)
            instalacao = self.__obter_nome_instalacao(dir=executavel.parent)
            versao = self.__obter_versao(dir=executavel.parent)
        self.executavel: Final = executavel
        self.instalacao: Final = instalacao
        self.versao: Final = versao
    
    def __str__(self) -> str:
        return f"Office(localizacao={self.executavel})"
    
    @staticmethod
    def __obter_arquivo_instalacao(dir: str) -> str:
        return os.path.join(dir, 'bootstrap.ini')
    
    @staticmethod
    def __obter_arquivo_versao(dir: str) -> str:
        return os.path.join(dir, 'version.ini')
    
    @classmethod
    def __obter_nome_instalacao(cls, dir: str) -> str:
        cfg = configparser.ConfigParser()
        cfg.read(cls.__obter_arquivo_instalacao(dir=dir))
        return cfg.get('Bootstrap', 'ProductKey')
    
    @classmethod
    def __obter_versao(cls, dir: str) -> str:
        cfg = configparser.ConfigParser()
        cfg.read(cls.__obter_arquivo_versao(dir=dir))
        return cfg.get('Version', 'MsiProductVersion')
    
    def converter_para_pdf(self, arquivo: str) -> bool:
        logger.debug(f"Converter arquivo '{arquivo}' em PDF.")
        endereco = PureWindowsPath(arquivo)
        try:
            result = subprocess.run(
                [
                    f'{self.executavel}', '--headless', '--convert-to','pdf',
                    '--outdir', f'{endereco.parent}', str(endereco)
                ],
                capture_output=True,
            )
        except Exception as erro:
            logger.error(f"Erro ao executar conversão de relatório: {str(erro)}")
        else:
            if result.returncode != 0:
                logger.error(
                    "Erro ao executar conversão de relatório: "
                    f"{result.stderr}"
                )
            logger.debug(f"Sucesso em converter o arquivo {arquivo}")
            return True
        return False
    
    @classmethod
    def emitir_sinal_ativo(cls):
        cls.estado.emitir(construir_mensagem(cor=VERDE, mensagem='Ativo.'))
    
    @classmethod
    def emitir_sinal_inativo(cls):
        cls.estado.emitir(construir_mensagem(cor=VERMELHO, mensagem='Inativo.'))

    @classmethod
    def validar(cls, localizacao: str) -> Optional[str]:
        # Verificar existência do arquivo
        if not os.path.exists(localizacao):
            return 'Executável não existe'
        loc = PureWindowsPath(localizacao)
        # Verificar arquivo da instalação
        loc_instalacao = cls.__obter_arquivo_instalacao(dir=loc.parent)
        if not os.path.exists(loc_instalacao):
            return 'Não foi possível encontrar o arquivo de dados da instalação'
        # Verificar arquivo de versão
        loc_versao = cls.__obter_arquivo_versao(dir=loc.parent)
        if not os.path.exists(loc_versao):
            return 'Não foi possível encontrar o arquivo de dados de versão'
        # Obter informações de instalação
        try:
            nome = cls.__obter_nome_instalacao(dir=loc.parent)
            if not nome.startswith("LibreOffice 7"):
                return f"{nome} é uma versão incompatível. Espera-se Libreoffice 7.x."
        except:
            return 'Não foi possível extrair informações da instalação'
        # Obter dados de versão
        try:
            cls.__obter_versao(dir=loc.parent)
        except:
            return 'Não foi possível extrair dados de versão'