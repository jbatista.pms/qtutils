import base64

from flask import Blueprint, request, send_file
from flask_restful import Api, Resource
from loguru import logger

from QtUtils import QtUtils
from QtUtils.crypto import ControladorCrypto
from QtUtilsServer.aplicativo.modelos import Cliente

from .base import Office
from .modelos import RelatorioArquivo

bp = Blueprint('relatorio', __name__, url_prefix='/relatorio/')
api = Api(bp)


class RelatorioAPI(Resource):
    def __init__(self) -> None:
        super().__init__()
        self.ctrl_crypto: ControladorCrypto = ControladorCrypto()
        
    def post(self):
        dados = self.ctrl_crypto.private_key.decrypt_dict(request.json)

        cliente_id = dados.get('cli_id', None)
        if cliente_id is None:
            return {'mensagem': 'Faltam argumentos.'}, 404
        cliente = Cliente.get_or_none(id_=cliente_id)
        if cliente is None or not cliente.autorizado:
            return {'mensagem': 'Cliente não autorizado.'}, 401
        
        rel_arquivo = RelatorioArquivo(cliente=cliente)
        rel_arquivo.salvar()
        for seq, arq in dados['documentos'].items():
            arq = base64.urlsafe_b64decode(s=arq)
            if not rel_arquivo.adicionar_parte(dados=arq, seq=seq):
                return {'mensagem': 'Erro no servidor.'}, 404
        return cliente.public_key().encrypt_dict({'id_': str(rel_arquivo.id_)})


class RelatorioObterAPI(Resource):
    def __init__(self) -> None:
        super().__init__()
        self.ctrl_crypto: ControladorCrypto = ControladorCrypto()
    
    def enviar(self, rel: RelatorioArquivo):
        arq_bytes = open(rel.caminho_pdf, 'rb').read()
        arq_dict = {'doc': base64.urlsafe_b64encode(arq_bytes).decode()}
        return rel.cliente.public_key().encrypt_dict(arq_dict)

    def get(self, id_):
        # Obter instância do arquivo
        try:
            rel_arquivo = RelatorioArquivo.get(id_=id_)
        except RelatorioArquivo.DoesNotExist:
            return {'mensagem': 'Não encontrado.'}, 404
        
        if rel_arquivo.convertido:
            return self.enviar(rel=rel_arquivo)
        
        # Verificar estado do serviço
        erro = Office.validar(localizacao=QtUtils.config.office)
        if erro:
            logger.error(f"Erro ao validar Office: {erro}.")
            Office.emitir_sinal_inativo()
            rel_arquivo.excluir()
            return {'mensagem': 'Serviço indisponível.'}, 503
        
        # Conversão e envio do relatório
        office = Office(localizacao=QtUtils.config.office)
        if rel_arquivo.converter():
            return self.enviar(rel=rel_arquivo)
        return {'mensagem': 'Serviço indisponível.'}, 503


api.add_resource(RelatorioAPI, '/')
api.add_resource(RelatorioObterAPI, '/<id_>/')