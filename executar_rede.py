from QtUtils import Definicoes, QtUtils

definicoes_cliente_rede = {
    'abreviatura_nome': 'QtUtils',
    'classe_janela_principal': 'QtUtils.app.JanelaPrincipal',
    'inicializar_cef': True,
    'nome_aplicacao': 'QtUtils',
    'modo_execucao': Definicoes.MODO_REDE,
    'verificar_atualizacao': True,
}

if __name__ == '__main__':
    if QtUtils.configurar(definicoes_cliente_rede):
        QtUtils.executar()