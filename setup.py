from setuptools import setup, find_packages

setup(
    name='QtUtils',
    version='23.12.05',
    packages=find_packages(),
    install_requires=[
        'cefpython3>66,<67',
        'cryptography==41.0.5',
        'loguru>0.5,<0.6',
        'passlib>=1.7,<1.8',
        'peewee>3.14,<3.18',
        'psycopg2>2.8,<2.10',
        'PySide2>5.15,<5.16',
        'pytz>=2021.1',
        'python-dotenv>0.17,<0.18',
        'requests>2.25,<2.26',
        'pikepdf>=5.1,<5.2',
        'py3o.template==0.10.0',
    ],
    include_package_data=True,
    scripts=['./QtUtils/scripts/converter.py'],
)
