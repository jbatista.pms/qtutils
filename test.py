import unittest
from pathlib import WindowsPath
from tempfile import mkdtemp

from executar_local import definicoes_cliente_local
from QtUtils import QtUtils
QtUtils.configurar(definicoes_cliente_local)

from QtUtils import crypto


class TestCrypto(unittest.TestCase):
    def setUp(self) -> None:
        self.path_dir = WindowsPath(mkdtemp())

    def test_crypto(self) -> None:
        path_pri = self.path_dir.joinpath('id_rsa')
        path_pub = self.path_dir.joinpath('id_rsa.pub')

        private_key = crypto.RSAPrivateKey.create(path=path_pri)
        public_key = crypto.RSAPublicKey.create(
            private_key=private_key,
            path=path_pub
        )

        private_key = crypto.RSAPrivateKey.load(path=path_pri)
        public_key = crypto.RSAPublicKey.load(path=path_pub)
        data = '123456'

        signature = private_key.sign(data=data)
        self.assertTrue(public_key.verify(data=data, signature=signature))

        pem = public_key.to_string()
        public_key = crypto.RSAPublicKey.from_string(s=pem)
        self.assertTrue(public_key.verify(data=data, signature=signature))

        ciphertext = public_key.encrypt(data=data)
        self.assertEqual(private_key.decrypt(ciphertext), data)

        data1 = {'1': '1', '2': {'3': '3'}}
        data2 = public_key.encrypt_dict(d=data1)
        self.assertDictEqual(data1, private_key.decrypt_dict(d=data2))

        self.assertTrue(isinstance(public_key.id(), str))

        cert_loc = self.path_dir.joinpath('cert.pem')
        cert = crypto.Certificate.create(
            path=cert_loc,
            pri_key=private_key,
            pub_key=public_key,
        )
        cert2 = crypto.Certificate.load(path=cert_loc)
        cert_str = cert.to_string()
        self.assertEqual(cert2.to_string(), cert_str)

        cert3 = crypto.Certificate.from_string(s=cert_str)
        self.assertEqual(cert.to_string(), cert_str)
        self.assertEqual(cert3.to_string(), cert_str)


if __name__ == '__main__':
    unittest.main()