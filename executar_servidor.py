from QtUtils import Definicoes, QtUtils

definicoes = {
    'abreviatura_nome': 'QtUtilsServer2',
    'classe_janela_principal': 'QtUtilsServer.janela_principal.JanelaPrincipal',
    'controladores': [
        'QtUtilsServer.api.ControladorAPI',
        'QtUtilsServer.bandeja.ControladorBandejaSistema',
        'QtUtilsServer.relatorio.controladores.ControladorRelatorio',
    ],
    'inicializar_cef': False,
    'nome_aplicacao': 'QtUtilsServer 2',
    'modelo_configuracao': 'QtUtilsServer.configuracao.modelos.ConfigQtUtilsServer',
    'modelos':[
        'QtUtilsServer.aplicativo.modelos.Cliente',
        'QtUtilsServer.aplicativo.modelos.Aplicativo',
        'QtUtilsServer.relatorio.modelos.RelatorioArquivo',
        'QtUtilsServer.relatorio.modelos.RelatorioArquivoParte',
        'QtUtilsServer.versao.modelos.Versao',
    ],
    'modo_execucao': Definicoes.MODO_LOCAL,
    'verificar_atualizacao': True,
}

if __name__ == '__main__':
    if QtUtils.configurar(definicoes=definicoes):
        QtUtils.executar()