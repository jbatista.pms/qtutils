"""
This is an example of using PyInstaller packager to build
executable from one of CEF Python's examples (wxpython.py).

See README-pyinstaller.md for installing required packages.

To package example type: `python pyinstaller.py`.
"""
import git
import os
import platform
import shutil
import sys
from subprocess import Popen

from QtUtils import QtUtils
from QtUtils.modulos import ImportarClasse
from executar_servidor import definicoes
QtUtils.configurar(definicoes)

DIR_BASE = os.path.join(os.getcwd(), "dev")
BUILD_DIR = os.path.join(DIR_BASE, "build")
EXE_EXT = ".exe"
DIST_DIR = os.path.join(DIR_BASE, "dist")

version = git.Repo(os.getcwd()).tags[-1].tag.tag

def assinar_executavel(exe, env=None):
    sub = Popen(
        [
            "C:\\Program Files (x86)\\Windows Kits\\10\\bin\\10.0.22621.0\\x64\\signtool.exe", 
            "sign", "/a", "/fd", "SHA256", "/f",
            os.path.join(DIR_BASE, "key.pfx"),
            exe,
        ], 
        env=env
    )
    sub.communicate()
    rcode = sub.returncode
    if rcode != 0:
        print("Error: Assinatura do executável, code=%s" % rcode)
        # Delete distribution directory if created
        if os.path.exists(DIST_DIR):
            shutil.rmtree(DIST_DIR)
        sys.exit(1)

def main():
    # Platforms supported
    if platform.system() != "Windows":
        raise SystemExit("Error: Only Windows platform is currently "
                         "supported. See Issue #135 for details.")

    # Make sure nothing is cached from previous build.
    # Delete the build/ and dist/ directories.
    if not os.path.exists(DIR_BASE):
        os.mkdir(DIR_BASE)
    if os.path.exists(BUILD_DIR):
        shutil.rmtree(BUILD_DIR)
    if os.path.exists(DIST_DIR):
        shutil.rmtree(DIST_DIR)

    # Execute pyinstaller.
    # Note: the "--clean" flag passed to PyInstaller will delete
    #       global global cache and temporary files from previous
    #       runs. For example on Windows this will delete the
    #       "%appdata%/roaming/pyinstaller/bincache00_py27_32bit"
    #       directory.
    env = os.environ    
    env['APP_NAME'] = QtUtils.definicoes.abreviatura_nome
    # Acrescentar módulos para importação
    env["QTUTILS_MODULOS"] = ','.join(ImportarClasse.modulos_registrados())
    if "--debug" in sys.argv:
        env["CEFPYTHON_PYINSTALLER_DEBUG"] = "1"
    sub = Popen(
        [
            "pyinstaller", 
            "--clean", 
            f"--distpath={DIST_DIR}", 
            f"--workpath={BUILD_DIR}", 
            os.path.join(os.getcwd(), 'pyinstaller_qtutilsserver.spec'),
            f"--specpath={DIR_BASE}"
        ], 
        env=env
    )
    sub.communicate()
    rcode = sub.returncode
    if rcode != 0:
        print("Error: PyInstaller failed, code=%s" % rcode)
        # Delete distribution directory if created
        if os.path.exists(DIST_DIR):
            shutil.rmtree(DIST_DIR)
        sys.exit(1)

    # Make sure everything went fine
    curdir = os.getcwd()
    app_dir = os.path.join(curdir, "dev", "dist", "pyinstaller")
    executable = os.path.join(app_dir, QtUtils.definicoes.abreviatura_nome + EXE_EXT)
    if not os.path.exists(executable):
        print("Error: PyInstaller failed, main executable is missing: %s"
              % executable)
        sys.exit(1)
    
    # Criar aquivo com dados da instalação
    from QtUtils.atualizacao.controles import DadosInstalacao
    DadosInstalacao(os.path.join(app_dir, "instalacao.ver")).gravar_arquivo(versao=version)

    # Assinando executável
    assinar_executavel(executable, env)
    
    # On Windows open folder in explorer or when --debug is passed
    # run the result binary using "cmd.exe /k cefapp.exe", so that
    # console window doesn't close.
    if platform.system() == "Windows":
        if "--debug" in sys.argv:
            os.system("start cmd /k \"%s\"" % executable)
        else:
            print("OK. Created dist/ directory.")
            build = Popen(
                [
                    "iscc",
                    "/DMyAppVersion=%s" % version,
                    "/DMyAppName=%s" % QtUtils.definicoes.abreviatura_nome,
                    "/DMyWork=%s" % os.getcwd(),
                    "/DMyAppExeName=%s" % QtUtils.definicoes.abreviatura_nome,
                    "/DMyAppPublisher=%s" % QtUtils.definicoes.abreviatura_nome,
                    os.path.join(os.getcwd(), 'instalador_qtutilsserver.iss'),
                ],
                env=env,
            )
            build.communicate()
            rcode = build.returncode
            if rcode != 0:
                print("Error: Inno setup compile failed, code=%s" % rcode)
                # Delete distribution directory if created
                if os.path.exists("dev/dist/"):
                    shutil.rmtree("dev/dist/")
                sys.exit(1)
            
            app_setup = os.path.join(curdir, "dev", "setups")
            executable = os.path.join(app_setup, "%s %s%s" % (QtUtils.definicoes.abreviatura_nome, version, EXE_EXT))
            if not os.path.exists(executable):
                print("Error: Inno setup compile failed, main executable is missing: %s"
                    % executable)
                sys.exit(1)

            assinar_executavel(executable)

            # SYSTEMROOT = C:/Windows
            os.system("%s/explorer.exe /n,/e,%s" % (
                os.environ["SYSTEMROOT"], app_setup))


if __name__ == "__main__":
    main()
